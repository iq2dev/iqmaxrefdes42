.. _iolinkappl:

IO-Link Application
===================

iqMAXREFDES42 communicates with an IO-Link Master on COM3 transmission rate (230,4kbit/s).

Process Data In
---------------

::

  Bits    23  22  21  20  19  18  17  16  15  14  13  12  11  10   9   8       7   6   5   4   3   2   1   0
         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+   +---+---+---+---+---+---+---+---+
    MSB  | x |       ambient temperature process value (15bits)          |   | x | x | x | x | x | x | x | F |  LSB
         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+   +---+---+---+---+---+---+---+---+
                                                                                                           |
                                               temperature process value switch-point flag (FALSE/TRUE) ---+

Process Data Out
----------------

Be absent.

Commands
--------

Can be run by IO-Link Index 0x0002:

================  ============
Value             Description
================  ============
128 (0x80)        Device Reset
160 (0xA0)        Tech-in ambient temperature switch-point level (see IO-Link index 0x0100 below)
================  ============

Parameters
----------

================  ============  ==============================================  =========================================
IO-Link index     Mode          Description                                     Default value
================  ============  ==============================================  =========================================
16 (0x0010)       read-only     Vendor Name                                     Maxim Integrated
17 (0x0011)       read-only     Vendor Text                                     The world leader in analog integration
18 (0x0012)       read-only     Product Name                                    Mojave (MAXREFDES42#)
19 (0x0013)       read-only     Product ID                                      MAXREFDES42
20 (0x0014)       read-only     Product Text                                    IO-Link RTD Temp Sensor
23 (0x0017)       read-only     Firmware Revision                               v |release|
256 (0x0100)      read/write    Ambient temperature switch-point level          9148 (0x23BC)
257 (0x0101)      read/write    Ambient temperature switch-point hysteresis     32 (0x20)
258 (0x0102)      read/write    Display's temperature scale (0 - °F, 1 - °C)    1 (0x01)
259 (0x0103)      read/write    N-wire type connection (2, 3, 4)                2
260 (0x0104)      read/write    MAX31865ATP+ Configuration register             0xCO (2-wire or 4-wire auto conversion mode)
261 (0x0105)      read-only     MAX31865ATP+ RTD (2 bytes)                      0
262 (0x0106)      read/write    MAX31865ATP+ High Fault Threshold (2 bytes)     0xFFFF
263 (0x0107)      read/write    MAX31865ATP+ Low Fault Threshold (2 bytes)      0
264 (0x0108)      read-only     MAX31865ATP+ Fault Status register              0x00
265 (0x0109)      read/write    Reference resistance (3 bytes)                  400_000mΩ (PT100)
266 (0x010A)      read/write    Nominal resistance at 0°C (3 bytes)             100_000mΩ (PT100)
267 (0x010B)      read-only     Ambient temperature (in °F)                     -412.3768°F
268 (0x010C)      read-only     Ambient temperature (in °C)                     -246.876°C
269 (0x010D)      read-only     Ambient temperature SP level (in °F)            86.0°F
270 (0x010E)      read-only     Ambient temperature SP level (in °C)            30.0°C
271 (0x010F)      read-only     Ambient temperature SP hysteresis (in °F)       1.8°F
272 (0x0110)      read-only     Ambient temperature SP hysteresis (in °C)       1.0°C
================  ============  ==============================================  =========================================

Events
------

================  ============
Event code        Description
================  ============
0x4210            Device temperature over-run
0x5000            Device hardware fault
0x5111            Primary supply voltage under-run
0x6000            Device software fault
0x7710            Short circuit
================  ============
