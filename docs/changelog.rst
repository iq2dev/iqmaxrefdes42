.. _changelog:

Changelog
=========

v1.3.3 (2015-03-03)
-------------------

  * Improvements:

    * (`#16 <http://bitbucket.org/iq2dev/iqmaxrefdes42/issue/16>`_) Introduce startup delay (100-300 msec)

v1.3.2 (2014-11-18)
-------------------

  * Improvements:

    * (`#15 <http://bitbucket.org/iq2dev/iqmaxrefdes42/issue/15>`_) Restarting the ADC after a fault RTD-to-Digital Converter (MAX31865) detection cycle


v1.3.1 (2014-09-30)
-------------------

  * Improvements:

    * (`#14 <http://bitbucket.org/iq2dev/iqmaxrefdes42/issue/14>`_) Please don't make display go "----" when fault is detected

v1.3 (2014-08-24)
-----------------

  * Improvements:

    * (`#12 <http://bitbucket.org/iq2dev/iqmaxrefdes42/issue/12>`_) Temperature readback value in oC does not match display after calibrating Ro
    * (`#13 <http://bitbucket.org/iq2dev/iqmaxrefdes42/issue/13>`_) Use '0.xxxx' format for temperature readback values

v1.2 (2014-08-08)
-----------------

  * Improvements:

    * (`#9 <http://bitbucket.org/iq2dev/iqmaxrefdes42/issue/9>`_) PT100 & PT1000 & Custom Ro

v1.1.1 (2014-07-28)
-----------------

  * Improvements:

    * (`#4 <http://bitbucket.org/iq2dev/iqmaxrefdes42/issue/4>`_) Callendar-Van Dusen equation

v1.1 (2014-06-07)
-----------------

  * **Breaking changes:**

    * (`#3 <http://bitbucket.org/iq2dev/iqmaxrefdes42/issue/3/>`_) MAX31865ATP+ RTD-to-Digital Converter registers are available as IO-Link parameters
    * (`#6 <http://bitbucket.org/iq2dev/iqmaxrefdes42/issue/6>`_) Switch N-wire connection type via an IO-Link parameter

  * Improvements:

    * (`#5 <http://bitbucket.org/iq2dev/iqmaxrefdes42/issue/5>`_) IODD picture and icon was added

v1.0 (2014-05-17)
-----------------

  * New features:

    * Hardware setups:

      * Renesas RL78/G1A (R5F10E8EALA) microcontroller (with Applilet3 support, see 'src\\applilet3_src')
      * MAX14821EWA+ IO-Link Device Transceiver (SPI/UART, see 'src\\hw_drivers\\c_max1482x.*')
      * MAX31865ATP+ RTD-to-Digital Converter (SPI, see 'src\\hw_drivers\\c_max31865.*')
      * SCE574x 4-Character 5x7 Dot Matrix Serial Input Dot Addressable Display (SPI, see 'src\\hw_drivers\\c_sce574x.*')

    * Porting IO-Link iqStack(R) Device and set up the IO-Link communication (see 'src\\applilet3_src\\r_main.c')
    * Write a simple IO-Link :term:`resistor temperature detector` Application (with appropriate IODDs, see 'src\\sensor_appl.c')
