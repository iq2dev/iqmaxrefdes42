.. _glossary:

Glossary
========

.. glossary::

    RTD
        Short for :term:`resistor temperature detector`.

    PV
        Short for :term:`process value`.

    resistor temperature detector
        Temperature sensor that operates on the measurement principle
        that a material’s electrical resistance changes with temperature.

    process value
        Internal unsigned numeric number, must be converted in order
        to display a required physical value.
