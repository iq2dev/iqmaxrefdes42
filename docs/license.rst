.. _license:

License
=======

All source files with the correspondent header notes are the subject
to the IQ² Development GmbH terms and conditions defined in file
:download:`Allgemeine Vertragsbedingungen.pdf <../Allgemeine Vertragsbedingungen.pdf>`.

For any other details please `contact with us <http://www.iq2-development.de/en/contact/>`_.
