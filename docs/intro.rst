.. _intro:

Introduction
============

.. image:: ./static/logos.png
   :align: center

Maxim's Mojave (MAXREFDES42#) reference design features a resistance
to digital temperature sensor which can also detect multiple faults.
The IO-Link communication protocol enables quick sensor configuration
and reduces cabling while featuring a robust, medium speed, communication
protocol with power, enabling higher powered sensors than a 4-20mA loop.

Board Specification
-------------------

+---------------------------+------------------------------------------+
| Item                      | Specification                            |
+===========================+==========================================+
| Oscillator Frequency      | 18.432MHz                                |
+---------------------------+------------------------------------------+
| Microcontroller           | Renesas RL78/G1A (R5F10E8EALA)           |
+---------------------------+------------------------------------------+
| DC Power                  | 24V via M12 Connector                    |
+---------------------------+------------------------------------------+
| LEDs                      | - Power indicator: Green x1              |
|                           | - IO-Link indicator: Amber x1            |
|                           | - Digital output indicator: Red x1       |
+---------------------------+------------------------------------------+
| IO-link Connector         | Male M12 4-pole A-coded                  |
+---------------------------+------------------------------------------+
| IO-Link PHY               | Maxim MAX14821EWA+                       |
+---------------------------+------------------------------------------+
| RTD-to-Digital Converter  | MAX31865ATP+                             |
+---------------------------+------------------------------------------+
| SPI 3.3V/5V Level Shifter | MAX1841EUB                               |
+---------------------------+------------------------------------------+
| Dot Addressable Display   | SCE5744 Z                                |
+---------------------------+------------------------------------------+

Project Goal
------------

.. image:: ./static/schema.png
   :align: center

The purpose of this project is by giving the user a strong basis to start
develop production-ready IO-Link® application based on Maxim Integrated IO-Link
device transceiver (`MAX14821 <http://www.maximintegrated.com/datasheet/index.mvp/id/7416>`_),
a Renesas ultra-low-power, 16-bit microcontroller (`RL78 <http://am.renesas.com/products/mpumcu/rl78/rl78g1x/rl78g1a/>`_)
and `iqStack® IO-Link Device Stack <http://www.iq2-development.de/iqstack-device-und-master>`_.