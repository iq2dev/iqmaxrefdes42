.. _troubleshooting:

Troubleshooting
===============

If during power-on self-test (POST) hardware fails were occurred,
then the main sensor application will not started and instead a first
appeared hardware fail number was blinked on digital output red-indicator
channel continuously and also was displayed on LCD.

============  ================
Blinks Count  Fail Description
============  ================
1             Renesas RL78 Illegal memory access (a firmware error, contact with developers)
2             Renesas RL78 RAM parity check (a firmware error, contact with developers)
3             Renesas RL78 Illegal instruction (a firmware error, contact with developers)
4             Maxim MAX14821EWA+ selfcheck (the chip is possibly corrupt, try to restart the device)
5             Maxim MAX31865ATP+ selfcheck (the chip is possibly corrupt, try to restart the device)
============  ================
