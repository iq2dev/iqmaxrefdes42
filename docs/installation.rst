.. _installation:

Installation Details
====================

There are two ways to experiment with MAXREFDES42#:

* Upload Firmware HEX-file, connect a device to IO-Link Master and play around.
* Clone the source code from Bitbucket repository, go throw and try to debug.

Firmware
--------

Firmware HEX-file, IODDs data and this document in PDF format can be downloaded
from the `Bitbucket repository <https://bitbucket.org/iq2dev/iqmaxrefdes42>`_
absolutely free without registration.

To upload HEX-file into MAXREFDES23 have to get
`Renesas E1 emulator <http://www.renesas.com/products/tools/emulation_debugging/onchip_debuggers/e1>`_
and also `Renesas Flash Programmer <http://am.renesas.com/products/tools/flash_prom_programming/rfp/>`_.

Source Code
-----------

The project source code can be cloned from the `Bitbucket repository
<https://bitbucket.org/iq2dev/iqmaxrefdes42>`_ freely without registration.

Download a trial version of `IAR RL78 Embedded Workbench IDE
<http://www.iar.com/Products/IAR-Embedded-Workbench/Renesas-RL78/>`_
in order to compile it and also take a look for
`Renesas Application Leading Tool (Applilet) <http://am.renesas.com/products/tools/coding_tools/coding_assistance/applilet/>`_.


Also the `Renesas E1 emulator <http://www.renesas.com/products/tools/emulation_debugging/onchip_debuggers/e1>`_
and `Renesas Flash Programmer <http://am.renesas.com/products/tools/flash_prom_programming/rfp/>`_
needed to start debug session with a device.
