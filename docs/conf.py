# -*- coding: utf-8 -*-

import os
import sys

sys.path.append(os.path.abspath('_ext'))

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.todo',
]

templates_path = ['templates']

source_suffix = '.rst'

master_doc = 'index'

project = u'iqMAXREFDES42'
copyright = u'2014, IQ² Development GmbH'

# The short X.Y version
version = '1.3'
# The full version, including alpha/beta/rc tags
release = '1.3.3'

exclude_patterns = ['_build']

default_role = 'obj'
pygments_style = 'sphinx'

# on_rtd is whether we are on readthedocs.org, this line of code grabbed from docs.readthedocs.org
on_rtd = os.environ.get('READTHEDOCS', None) == 'True'

if not on_rtd:  # only import and set the theme if we're building docs locally
    import sphinx_rtd_theme
    extensions.append('rst2pdf.pdfbuilder')
    html_theme = 'sphinx_rtd_theme'
    html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]

# otherwise, readthedocs.org uses their theme by default, so no need to specify ithtml_theme = "sphinx_rtd_theme"

html_static_path = ['static']

latex_documents = [
  ('index', 'iqmaxrefdes42.tex', u'iqMAXREFDES42 Documentation',
   u'IQ2 Development GmbH', 'manual'),
]

pdf_documents = [
    ('index', 'iqMAXREFDES42', u'iqMAXREFDES42 Documentation',
     u'IQ² Development GmbH'),
]

pdf_stylesheets = ['sphinx', 'a4']

pdf_language = "en_US"

man_pages = [
    ('index', 'iqMAXREFDES42', u'iqMAXREFDES42 Documentation',
     [u'IQ² Development GmbH'], 1)
]

texinfo_documents = [
  ('index', 'iqMAXREFDES42', u'iqMAXREFDES42 Documentation',
   u'IQ² Development GmbH', 'iqMAXREFDES42', 'One line description of project.',
   'Miscellaneous'),
]

language = 'en'