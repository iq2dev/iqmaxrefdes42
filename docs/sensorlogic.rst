.. _sensorlogic:

Sensor Logic
============

In iqMAXREFDES42 have been implemented simple switch-point hysteresis logic and
additionally can be configured as normally open (NO) or normally closed (NC).

.. image:: ./static/sp.png
   :align: center

By default ambient temperature switch-point settings (32767 is the maximal value):

=====================  ==========================  ======================  ================
What                   SP (Level)                  Hysteresis              Mode
=====================  ==========================  ======================  ================
Ambient temperature    9148 (0x23BC, 30°C/86°F)    32 (0x20, 1°C/1.8°F)    Normally Open (NO)
=====================  ==========================  ======================  ================

.. note::
    Ambient temperature switch-point flag always indicated on digital output channel (red LED).

.. note::
    Ambient temperature switch-point level can be calculated via IO-Link Tech-in command (160, 0xA0) automatically.
