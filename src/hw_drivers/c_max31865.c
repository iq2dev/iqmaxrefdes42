/**
 * \brief Maxim MAX31865 RTD-to-Digital Converter
 * \copyright Copyright (C) 2014 by IQ2 Development GmbH, Neckartenzlingen. All rights reserved.
 * \see http://www.maximintegrated.com/datasheet/index.mvp/id/7900
 * 
 * This file is subject to the terms and conditions
 * defined in file 'Allgemeine Vertragsbedingungen.pdf',
 * which is part of this source code package.
 */

#include "c_max31865.h"

static volatile uint8_t spi_is_complete = FALSE;
static volatile int16_t spi_err = ERR_NONE;

static void spi_complete_handler(int16_t err)
{
  spi_is_complete = TRUE;
  spi_err = err;
}

int16_t max31865_init(void)
{
  uint8_t buf1[4], buf2[] = { 0xFFU, 0x12U, 0x00U, 0x34U };
  
  CHK(max31865_read_registers(MAX31865_REG_03_RTDHIGH_MSB, buf1, 4));
  
  CHK(max31865_write_registers(MAX31865_REG_03_RTDHIGH_MSB, buf2, 4));
  
  return max31865_write_registers(MAX31865_REG_03_RTDHIGH_MSB, buf1, 4);
}

int16_t max31865_read_rtd(void)
{
  uint8_t buf[2];
  uint16_t val;
  
  CHK(max31865_read_registers(MAX31865_REG_01_RTD_MSB, buf, 2));
  
  val = GET_BIGEND_UINT16(buf);
  
  if (val & MAX31865_RTD_01_FAULT)
    return MAX31865_ERR_8003_VOLTAGE;
  
  return val >> 1;
}

#pragma inline
static float calc_iteration(float rnorm, float t) {
  return rnorm / (0.39083 - 0.00005775 * t);
}

float max31865_precalc(uint32_t rref,
                       uint32_t r0)
{
  return ((float) rref) / ((float) r0) / 327.68;
}

int16_t max31865_calc(uint16_t rtd,
                      float precalc,
                      float *val,
                      uint8_t unit)
{
  float rnorm;
  
  if (rtd & 0x8000U)
    return MAX31865_ERR_8001_ARGUMENT;

  rnorm = ((float) rtd) * precalc - 100.0;
  
  *val = calc_iteration(rnorm, ((float) rtd) / 32 - 256);
  
  if (rtd > 13000)
  {
    *val = rtd > 21000 ?
      calc_iteration(rnorm, calc_iteration(rnorm, *val)) :
      calc_iteration(rnorm, *val);
  }
  
  if (unit == MAX31865_UNIT_00_FAHRENHEIT)
  {
      *val = *val * 1.8 + 32;
  }
 
  return ERR_NONE;
}

int16_t max31865_scale_rtd(uint16_t rtd,
                           float precalc,
                           int16_t *val,
                           uint8_t unit)
{ 
  float t;
 
  CHK(max31865_calc(rtd, precalc, &t, unit));

  *val = (int16_t) (t + (t >= 0 ? 0.5 : -0.5));

  return ERR_NONE;
}

int16_t max31865_write_registers(uint8_t reg,
                                 uint8_t const *data,
                                 uint8_t num)
{
  static uint8_t const WMASKS[] =
  {
    0xD1U,                                                                      /* MAX31865_REG_00_CONF */
    0x00U,                                                                      /* MAX31865_REG_01_RTD_MSB */
    0x00U,                                                                      /* MAX31865_REG_02_RTD_LSB */
    0xFFU,                                                                      /* MAX31865_REG_03_RTDHIGH_MSB */
    0xFEU,                                                                      /* MAX31865_REG_04_RTDHIGH_LSB */
    0xFFU,                                                                      /* MAX31865_REG_05_RTDLOW_MSB */
    0xFEU,                                                                      /* MAX31865_REG_06_RTDLOW_LSB */
    0x00U                                                                       /* MAX31865_REG_07_STATUS */
  };
  
  int8_t i;  
  uint8_t buf[4];

  if (num > 4 || (reg + num) > MAX31865_REG_08_END)
    return MAX31865_ERR_8001_ARGUMENT;

  spi_err = ERR_NONE;
  
  _max31865_spi_start();

  _max31865_spi_select_chip();
  
  do
  {
    *buf = 0x80U | reg;
    _max31865_spi_transfer_async(buf, buf + 1, 1, spi_complete_handler);
    while (!spi_is_complete) { __asm("NOP"); }
    spi_is_complete = FALSE;
    CHK_BREAK(spi_err);
    
    _max31865_spi_transfer_async(data, buf, num, spi_complete_handler);
    while (!spi_is_complete) { __asm("NOP"); }
    spi_is_complete = FALSE;
    CHK_BREAK(spi_err);
    
  } while (0U);
  
  _max31865_spi_deselect_chip();

  _max31865_spi_stop();
  
  if (IS_ERR(spi_err))
    return spi_err;
  
  CHK(max31865_read_registers(reg, buf, num));
  
  for (i = 0; i < num; i++)
  {
    if ((data[i] & WMASKS[reg + i]) != (buf[i] & WMASKS[reg + i]))
      return MAX31865_ERR_8000_SELFTEST;
  }
  
  return ERR_NONE;
}

int16_t max31865_read_registers(uint8_t reg,
                                uint8_t *ptr,
                                uint8_t num)
{
  uint8_t buf[8];

  if (num > 8 || (reg + num) > MAX31865_REG_08_END)
    return MAX31865_ERR_8001_ARGUMENT;
  
  spi_err = ERR_NONE;
  
  _max31865_spi_start();
  
  _max31865_spi_select_chip();
  
  do
  {
    _max31865_spi_transfer_async(&reg, buf, 1, spi_complete_handler);
    while (!spi_is_complete) { __asm("NOP"); }
    spi_is_complete = FALSE;
    CHK_BREAK(spi_err);

    _max31865_spi_transfer_async(buf, ptr, num, spi_complete_handler);
    while (!spi_is_complete) { __asm("NOP"); }
    spi_is_complete = FALSE;
    CHK_BREAK(spi_err);
    
  } while (0U);
  
  _max31865_spi_deselect_chip();

  _max31865_spi_stop();
  
  return spi_err;
}
