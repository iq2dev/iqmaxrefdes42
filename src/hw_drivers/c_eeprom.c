/**
 * \brief Flash EEPROM emulation functions
 * \copyright Copyright (C) 2014 by IQ2 Development GmbH, Neckartenzlingen. All rights reserved.
 * 
 * This file is subject to the terms and conditions
 * defined in file 'Allgemeine Vertragsbedingungen.pdf',
 * which is part of this source code package.
 */

#include "c_eeprom.h"

#include "fdl.h"
#include "fdl_descriptor.h"
#include "eel.h"
#include "eel_descriptor.h"

extern __far const eel_u08 eel_descriptor[EEL_VAR_NO+1][4];

uint8_t eeprom_is_formated = FALSE;

int16_t eeprom_format(void)
{  
  eel_request_t eel_request;

  // Format EEL pool flash access
  eel_request.command_enu = EEL_CMD_FORMAT;
  eel_request.timeout_u08 = 0xFF;
  
  EEL_Execute(&eel_request);
  
  if (eel_request.status_enu != EEL_OK) 
    return EEPROM_ERR_8000_INTERNAL;
  
  // Startup EEL pool flash access
  eel_request.command_enu = EEL_CMD_STARTUP;
  eel_request.timeout_u08 = 0xFF;
  
  EEL_Execute(&eel_request);
  
  if (eel_request.status_enu != EEL_OK) 
    return EEPROM_ERR_8000_INTERNAL;
  
  eeprom_is_formated = TRUE;
  
  return ERR_NONE;
}

int16_t eeprom_init(void)
{ 
  eel_request_t eel_request;

  eel_status_t err;  
  
  // Init FAL, Flash Access Library
  if (FAL_Init(&fal_descriptor_str) != FAL_OK)
    return EEPROM_ERR_8000_INTERNAL;
  
  err = EEL_Init();
  
  // Init EEL, EEPROM Emulation library
  if (err != EEL_OK)
    return EEPROM_ERR_8000_INTERNAL;
  
  // Open EEL pool flash access
  EEL_Open();
  
  // Startup EEL pool flash access
  // executes EEL command in enforce mode
  eel_request.command_enu = EEL_CMD_STARTUP;
  eel_request.timeout_u08 = 0xFF;
  
  EEL_Execute(&eel_request);
  
  switch (eel_request.status_enu)
  {
    case EEL_OK:
      return ERR_NONE;
      
    case EEL_ERR_POOL_INCONSISTENT:
      
      if (eeprom_format())
        return EEPROM_ERR_8000_INTERNAL;

      return ERR_NONE;
    
    default:
      return EEPROM_ERR_8000_INTERNAL;
  }
  
}

int16_t eeprom_write(uint8_t parNumber,
                     uint8_t *value)
{
  eel_request_t eel_request;
  
  eel_request.address_pu08 = (eel_u08*)value;
  eel_request.identifier_u08 = parNumber;
  eel_request.timeout_u08 = 0xFF;
  eel_request.command_enu = EEL_CMD_WRITE;
  
  EEL_Execute(&eel_request);
  
  if (eel_request.status_enu == EEL_OK)
    return ERR_NONE;
  
  if (eel_request.status_enu == EEL_ERR_PARAMETER)
    return EEPROM_ERR_8001_WRONG_PAR;
  
  if (eel_request.status_enu == EEL_ERR_REJECTED
      || eel_request.status_enu == EEL_BUSY)
    return EEPROM_ERR_8004_BUSY;

  return EEPROM_ERR_8000_INTERNAL;
}

int16_t eeprom_read(uint8_t parNumber,
                    uint8_t *value)
{
  eel_request_t eel_request;

  eel_request.address_pu08 = (eel_u08*)value;
  eel_request.identifier_u08 = parNumber;
  eel_request.timeout_u08 = 0xFF;
  eel_request.command_enu = EEL_CMD_READ;
  
  EEL_Execute(&eel_request);
  
  if (eel_request.status_enu == EEL_OK)
    return ERR_NONE;
  
  if (eel_request.status_enu == EEL_ERR_NO_INSTANCE)
    return EEPROM_ERR_8002_PAR_NOT_INIT;
  
  if (eel_request.status_enu == EEL_ERR_PARAMETER)
    return EEPROM_ERR_8001_WRONG_PAR;
  
  if (eel_request.status_enu == EEL_ERR_REJECTED
      || eel_request.status_enu == EEL_BUSY)
    return EEPROM_ERR_8004_BUSY;

  return EEPROM_ERR_8000_INTERNAL;
}

void eeprom_handle(void)
{
  eel_driver_status_t driverStatus;
  
  EEL_Handler(0);
  EEL_GetDriverStatus(&driverStatus);
}