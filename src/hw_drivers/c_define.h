/**
 * \brief General commons defines for firmware development
 * \copyright Copyright (C) 2014 by IQ2 Development GmbH, Neckartenzlingen. All rights reserved.
 * 
 * This file is subject to the terms and conditions
 * defined in file 'Allgemeine Vertragsbedingungen.pdf',
 * which is part of this source code package.
 */

#ifndef __C_DEFINE_H
#define __C_DEFINE_H

#include <stdlib.h>
#include <string.h>

enum
{
  OFF       = 0,
  ON        = 1,
  
  FALSE     = 0x00U,
  TRUE      = 0xFFU,
  
  ERR_NONE  = (short)0x0000,
  ERR_BASE  = (short)0x8000,
};

typedef union
{
  struct
  {
    unsigned char n0 : 1;
    unsigned char n1 : 1;
    unsigned char n2 : 1;
    unsigned char n3 : 1;
    unsigned char n4 : 1;
    unsigned char n5 : 1;
    unsigned char n6 : 1;
    unsigned char n7 : 1;
  } bits;
  unsigned char val;
} byte_t;
#ifndef __TYPEDEF__
typedef signed char     int8_t;
typedef unsigned char   uint8_t;
typedef signed short    int16_t;
typedef unsigned short  uint16_t;
typedef signed long     int32_t;
typedef unsigned long   uint32_t;
#endif

#define MIN_INT8    (-0x7F - 1)
#define MIN_INT16   (-0x7FFF - 1)
#define MIN_INT32   (-0x7FFFFFFF - 1)

#define MAX_INT8    0x7F
#define MAX_INT16   0x7FFF
#define MAX_INT32   0x7FFFFFFF

#define MAX_UINT8   0xFFU
#define MAX_UINT16  0xFFFFU
#define MAX_UINT32  0xFFFFFFFFU

#define STR(x) #x
#define TOSTR(x) STR(x)

#define ABS(x) (((x) < 0) ? -(x) : (x))

#define MIN(x,y) (((x)<=(y))?(x):(y))
#define MAX(x,y) (((x)>=(y))?(x):(y))
  
#define SWAP(var1, var2) \
  { var1 ^= var2; var2 = (var1) ^ (var2); var1 ^= var2; }

#define GET_BIT(byte, pos) (((byte) >> (pos)) & 1U)
#define SET_BIT(byte, pos, flag) \
  (byte = (flag) ? ((byte) | (1U << (pos))) : ((byte) & ~(1U << (pos))))

#define GET_BIGEND_INT16(ptr) \
  (int16_t)(((((uint8_t *)(ptr))[0]) << 8) \
          + ((((uint8_t *)(ptr))[1])))

#define GET_BIGEND_UINT16(ptr) \
  (uint16_t)(((((uint8_t *)(ptr))[0]) << 8) \
           + ((((uint8_t *)(ptr))[1])))

#define GET_BIGEND_LONG32(ptr) \
  (int32_t)(((((uint8_t *)(ptr))[0]) << 24) \
          + ((((uint8_t *)(ptr))[1]) << 16) \
          + ((((uint8_t *)(ptr))[2]) <<  8) \
          + ((((uint8_t *)(ptr))[3])))

#define GET_BIGEND_ULONG32(ptr) \
            (((uint32_t)(((uint8_t *)(ptr))[0]) << 24) \
           + ((uint32_t)(((uint8_t *)(ptr))[1]) << 16) \
           + ((uint32_t)(((uint8_t *)(ptr))[2]) <<  8) \
           + ((uint32_t)(((uint8_t *)(ptr))[3])))

#define SET_BIGEND_INT16(ptr, val) \
  do { \
    ((uint8_t *)(ptr))[0] = (uint8_t)(((val) >> 8) & 0xFF); \
    ((uint8_t *)(ptr))[1] = (uint8_t)(((val)     ) & 0xFF); \
  } while(0)

#define SET_BIGEND_UINT16(ptr, val) \
  SET_BIGEND_INT16(ptr, val)

#define SET_BIGEND_LONG32(ptr, val) \
  do { \
    ((uint8_t *)(ptr))[0] = (uint8_t)(((val) >> 24) & 0xFF); \
    ((uint8_t *)(ptr))[1] = (uint8_t)(((val) >> 16) & 0xFF); \
    ((uint8_t *)(ptr))[2] = (uint8_t)(((val) >>  8) & 0xFF); \
    ((uint8_t *)(ptr))[3] = (uint8_t)(((val)      ) & 0xFF); \
  } while(0)

#define SET_BIGEND_ULONG32(ptr, val) \
  SET_BIGEND_LONG32(ptr, val)

#define SET_BIGEND_FLOAT(ptr, val) \
  do { \
    uint8_t *__ptr = (uint8_t *)&(val); \
    ((uint8_t *)(ptr))[0] = __ptr[3]; \
    ((uint8_t *)(ptr))[1] = __ptr[2]; \
    ((uint8_t *)(ptr))[2] = __ptr[1]; \
    ((uint8_t *)(ptr))[3] = __ptr[0]; \
  } while(0)

#define IS_ERR(fn) ((fn) < -1)      
      
#define CHK_CUSTOM(exp, action, exit) \
  do { \
    int16_t __res = (int16_t)(exp); \
    if (IS_ERR(__res)) { action; exit; } \
  } while(0)

#define CHK(exp) \
  CHK_CUSTOM(exp,, return __res)
    
#define CHK2(exp, action) \
  CHK_CUSTOM(exp, action, return __res)

#define CHK_VOID2(exp, action) \
  CHK_CUSTOM(exp, action,)

#define CHK_CONTINUE(exp) \
  CHK_CUSTOM(exp,, continue)

#define CHK_CONTINUE2(exp, action) \
  CHK_CUSTOM(exp, action, continue)

#define CHK_BREAK(exp) \
  if (IS_ERR((int16_t)(exp))) { break; }

#define CHK_BREAK2(exp, action) \
  CHK_CUSTOM(exp, action, break)
    
#define CALL0(fn) \
  if (fn) fn()
    
#define CALL1(fn, arg1) \
  if (fn) fn(arg1)
    
#define CALL2(fn, arg1, arg2) \
  if (fn) fn(arg1, arg2)

#endif
