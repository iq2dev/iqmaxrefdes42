/**
 * \brief Maxim MAX1482[0|1] IO-Link Device Transceiver Chip Driver
 * \copyright Copyright (C) 2014 by IQ2 Development GmbH, Neckartenzlingen. All rights reserved.
 * \see http://www.maximintegrated.com/datasheet/index.mvp/id/7416
 * 
 * This file is subject to the terms and conditions
 * defined in file 'Allgemeine Vertragsbedingungen.pdf',
 * which is part of this source code package.
 */

#include "c_max1482x.h"

static volatile uint8_t is_irq = FALSE;
static volatile uint8_t tx_enable = FALSE;

static volatile uint8_t spi_is_complete = FALSE;
static volatile int16_t spi_err = ERR_NONE;

#ifdef MAX1482X_UART_COMPLETE
volatile uint8_t max1482x_uart_complete = FALSE;
#endif

static uint8_t regs_mirror[MAX1482X_REG_04_END];

static void (*event_appear_callback)(uint8_t event);
static void (*event_disappear_callback)(uint8_t event);

static void (*weakup_callback)(void);

static void (*byte_recv_callback)(uint8_t byte,
                                  int16_t err);

static void irq_handler(void)
{
  is_irq = TRUE;
}

static void weakup_handler(void)
{
  _max1482x_set_txenable(OFF);
  _max1482x_disable_wakeup();
  CALL0(weakup_callback);
}

static void spi_complete_handler(int16_t err)
{
  spi_is_complete = TRUE;
  spi_err = err;
}

static void uart_recv_handler(uint8_t byte,
                              int16_t err)
{
  if (tx_enable)
    return;
  
  CALL2(byte_recv_callback, byte, err);
}

static void uart_complete_handler(void)
{
  tx_enable = FALSE;
  _max1482x_set_txenable(OFF);
  
#ifdef MAX1482X_UART_COMPLETE
  max1482x_uart_complete = TRUE;
#endif
}

int16_t max1482x_init(void)
{
  max1482x_spi_start();
  
  do
  {
    CHK_BREAK(max1482x_spi_read_register(
        MAX1482X_REG_01_CQCONFIG, regs_mirror + MAX1482X_REG_01_CQCONFIG));
    CHK_BREAK(max1482x_spi_read_register(
        MAX1482X_REG_02_DIOCONFIG, regs_mirror + MAX1482X_REG_02_DIOCONFIG));
    CHK_BREAK(max1482x_spi_read_register(
        MAX1482X_REG_03_MODE, regs_mirror + MAX1482X_REG_03_MODE));

    CHK_BREAK(max1482x_spi_write_register(MAX1482X_REG_02_DIOCONFIG,
        MAX1482X_DIOCONFIG_08_DOEN | MAX1482X_DIOCONFIG_80_DOINV));             /* just try to enable DO Driver and DO Output Polarity */
  
    max1482x_spi_write_register(
      MAX1482X_REG_02_DIOCONFIG, regs_mirror[MAX1482X_REG_02_DIOCONFIG]);
    
  } while (0U);

  max1482x_spi_stop();
  
  return spi_err;
}

void max1482x_watch_irq_events(void)
{
  static uint8_t conv = NULL;
  
  uint8_t byte;

#if defined MAX1482X_IRQ_POLLING
  
  if (!is_irq && !_max1482x_get_irq())
  {
    irq_handler();
  }
  
#endif
  
  if (!is_irq)
    return;
  
  max1482x_spi_start();

  do
  {
    CHK_BREAK(max1482x_spi_read_register(MAX1482X_REG_03_MODE, &byte));
    
    if ((byte & MAX1482X_MODE_02_UV33EN) ^ (conv & 0x02U))
    {
      if (byte & MAX1482X_MODE_02_UV33EN)
      {
        SET_BIT(conv, 1, ON);
        event_appear_callback(MAX1482X_EVENT_02_UV3_3);
      }
      else
      {
        SET_BIT(conv, 1, OFF);
        event_disappear_callback(MAX1482X_EVENT_02_UV3_3);
      }
    }
    
    if ((byte & MAX1482X_MODE_04_OTEMP) ^ (conv & 0x04U))
    {
      if (byte & MAX1482X_MODE_04_OTEMP)
      {
        SET_BIT(conv, 2, ON);
        event_appear_callback(MAX1482X_EVENT_00_OTEMP);
      }
      else
      {
        SET_BIT(conv, 2, OFF);
        event_disappear_callback(MAX1482X_EVENT_00_OTEMP);
      }
    }
    
    if ((byte & MAX1482X_MODE_08_UV24) ^ (conv & 0x08U))
    {
      if (byte & MAX1482X_MODE_08_UV24)
      {
        SET_BIT(conv, 3, ON);
        event_appear_callback(MAX1482X_EVENT_01_UV24);
      }
      else
      {
        SET_BIT(conv, 3, OFF);
        event_disappear_callback(MAX1482X_EVENT_01_UV24);
      }
    }
    
    if ((byte & MAX1482X_MODE_10_CQFAULT) ^ (conv & 0x10U))                     /* ~1.3/100 during reinitialisation may be null */
    {
      if (byte & MAX1482X_MODE_10_CQFAULT)
      {
        SET_BIT(conv, 4, ON);
        SET_BIT(conv, 5, OFF);
        event_appear_callback(MAX1482X_EVENT_03_CQFAULT);
      }
      else if (conv & 0x20U)
      {
        SET_BIT(conv, 4, OFF);
        SET_BIT(conv, 5, OFF);
        event_disappear_callback(MAX1482X_EVENT_03_CQFAULT);
      }
      else
      {
        SET_BIT(conv, 5, ON);
      }
    }
    else if (byte & MAX1482X_MODE_10_CQFAULT)
    {
      SET_BIT(conv, 5, OFF);
    }
    
    if ((byte & MAX1482X_MODE_20_DOFAULT) ^ ((conv & 0x40U) >> 1))              /* ~1.7/100 during reinitialisation may be null */
    {
      if (byte & MAX1482X_MODE_20_DOFAULT)
      {
        SET_BIT(conv, 6, ON);
        SET_BIT(conv, 7, OFF);
        event_appear_callback(MAX1482X_EVENT_04_DOFAULT);
      }
      else if (conv & 0x80U)
      {
        SET_BIT(conv, 6, OFF);
        SET_BIT(conv, 7, OFF);
        event_disappear_callback(MAX1482X_EVENT_04_DOFAULT);
      }
      else
      {
        SET_BIT(conv, 7, ON);
      }
    }
    else if (byte & MAX1482X_MODE_20_DOFAULT)
    {
      SET_BIT(conv, 7, OFF);
    }
  
    if (conv)
      break;
    
    is_irq = FALSE;

    CHK_BREAK(max1482x_spi_read_register(MAX1482X_REG_00_STATUS, &byte));

    CHK_BREAK(max1482x_spi_write_register(
        MAX1482X_REG_01_CQCONFIG, regs_mirror[MAX1482X_REG_01_CQCONFIG]));
    CHK_BREAK(max1482x_spi_write_register(
        MAX1482X_REG_02_DIOCONFIG, regs_mirror[MAX1482X_REG_02_DIOCONFIG]));
    CHK_BREAK(max1482x_spi_write_register(
        MAX1482X_REG_03_MODE, regs_mirror[MAX1482X_REG_03_MODE]));
    
  } while (0U);
  
  max1482x_spi_stop();
  
  if (IS_ERR(spi_err))
  {
    event_appear_callback(MAX1482X_EVENT_80_SPICOMM);
  }
}

void max1482x_listen_events(void (*on_appear)(uint8_t event),
                            void (*on_disappear)(uint8_t event))
{
  event_appear_callback = on_appear;
  event_disappear_callback = on_disappear;

#if !(defined MAX1482X_IRQ_POLLING)
  max1482x_INTREnable(process_irq);
#endif
}

int16_t max1482x_spi_write_register(uint8_t reg,
                                    uint8_t val)
{
  uint8_t byte;

  spi_err = ERR_NONE;
  
  _max1482x_spi_select_chip();
  
  do
  {
    _max1482x_spi_byte_cycle_async(reg, &byte, spi_complete_handler);
    while (!spi_is_complete) { __asm("NOP"); }
    spi_is_complete = FALSE;
    CHK_BREAK(spi_err);
    
    _max1482x_spi_byte_cycle_async(val, &byte, spi_complete_handler);
    while (!spi_is_complete) { __asm("NOP"); }
    spi_is_complete = FALSE;
    CHK_BREAK(spi_err);
    
  } while (0U);
  
  _max1482x_spi_deselect_chip();
  
  if (IS_ERR(spi_err))
    return spi_err;
  
  regs_mirror[reg] = val;
  
  if (reg == MAX1482X_REG_03_MODE)
    return ERR_NONE;
  
  CHK(max1482x_spi_read_register(reg, &byte));
  
  return byte == val ? ERR_NONE : MAX1482X_ERR_8000_SELFTEST;
}

int16_t max1482x_spi_read_register(uint8_t reg,
                                   uint8_t *ptr)
{
  uint8_t byte;

  spi_err = ERR_NONE;
  
  _max1482x_spi_select_chip();
  
  do
  {
    _max1482x_spi_byte_cycle_async(0x80U | reg, &byte, spi_complete_handler);
    while (!spi_is_complete) { __asm("NOP"); }
    spi_is_complete = FALSE;
    CHK_BREAK(spi_err);

    _max1482x_spi_byte_cycle_async(byte = 0, ptr, spi_complete_handler);
    while (!spi_is_complete) { __asm("NOP"); }
    spi_is_complete = FALSE;
    CHK_BREAK(spi_err);
    
  } while (0U);
  
  _max1482x_spi_deselect_chip();
  
  return spi_err;
}

int16_t max1482x_wait_wakeup(void (*on_wakeup)(void))
{
  uint8_t byte;

  max1482x_uart_stop();
  max1482x_spi_start();
  
  CHK2(max1482x_spi_read_register(MAX1482X_REG_01_CQCONFIG, &byte),
    max1482x_spi_stop());
  SET_BIT(byte, 4, OFF), SET_BIT(byte, 5, OFF);                                 /* switch C/Q to PNP mode */
  CHK2(max1482x_spi_write_register(MAX1482X_REG_01_CQCONFIG, byte),
    max1482x_spi_stop());
  
  max1482x_spi_stop();
  
  weakup_callback = on_wakeup;
  _max1482x_set_txenable(ON);
  _max1482x_enable_wakeup(weakup_handler);
  
  return ERR_NONE;
}

int16_t max1482x_uart_start(void (*on_byte_recv)(uint8_t byte,
                                                 int16_t err))
{
  uint8_t byte;
  
  max1482x_spi_start();
  
  CHK2(max1482x_spi_read_register(MAX1482X_REG_01_CQCONFIG, &byte),
    max1482x_spi_stop());
  SET_BIT(byte, 4, ON);                                                         /* switch C/Q from PNP to SDCI mode */
  CHK2(max1482x_spi_write_register(MAX1482X_REG_01_CQCONFIG, byte),
    max1482x_spi_stop());
  
  max1482x_spi_stop();

  byte_recv_callback = on_byte_recv;
  _max1482x_set_txenable(OFF);
  _max1482x_uart_start(uart_recv_handler);
  
  return ERR_NONE;
}

void max1482x_uart_send_async(uint8_t const *data,
                              uint8_t len)
{
  tx_enable = TRUE;
  _max1482x_set_txenable(ON);
  _max1482x_uart_send_async(data, len, uart_complete_handler);
}
