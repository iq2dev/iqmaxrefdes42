/**
 * \brief OSRAM SCE574x 4-Character 5x7 Dot Matrix Serial Input Dot Addressable Display
 * \copyright Copyright (C) 2014 by IQ2 Development GmbH, Neckartenzlingen. All rights reserved.
 * 
 * This file is subject to the terms and conditions
 * defined in file 'Allgemeine Vertragsbedingungen.pdf',
 * which is part of this source code package.
 */

#include "c_sce574x.h"

#include "c_assert.h"

enum
{
  OPTCODE_00_LCD  = 0x00U,                                                      /* Load Column Data */
  OPTCODE_A0_LDA  = 0xA0U,                                                      /* Load Digit Address */
  OPTCODE_C0_SCL  = 0xC0U,                                                      /* Software CLear */
  OPTCODE_E0_LCWD = 0xE0U,                                                      /* Load Control World Data */
};

static uint8_t const DIGIT_ZERO[7] =
{
  0x0EU,
  0x11U,
  0x13U,
  0x15U,
  0x19U,
  0x11U,
  0x0EU,
};

static uint8_t const DIGIT_ONE[7] =
{
  0x04U,
  0x0CU,
  0x14U,
  0x04U,
  0x04,
  0x04U,
  0x1FU,
};

static uint8_t const DIGIT_TWO[7] =
{
  0x0EU,
  0x11U,
  0x01U,
  0x02U,
  0x04U,
  0x08U,
  0x1FU,
};

static uint8_t const DIGIT_THREE[7] =
{
  0x0EU,
  0x11U,
  0x01U,
  0x06U,
  0x01U,
  0x11U,
  0x0EU,
};

static uint8_t const DIGIT_FOUR[7] =
{
  0x02U,
  0x06U,
  0x0AU,
  0x12U,
  0x1FU,
  0x02U,
  0x02U,
};

static uint8_t const DIGIT_FIVE[7] =
{
  0x1FU,
  0x10U,
  0x1EU,
  0x01U,
  0x01U,
  0x11U,
  0x0EU,
};

static uint8_t const DIGIT_SIX[7] =
{
  0x0EU,
  0x11U,
  0x10U,
  0x1EU,
  0x11U,
  0x11U,
  0x0EU,
};

static uint8_t const DIGIT_SEVEN[7] =
{
  0x1FU,
  0x01U,
  0x02U,
  0x04U,
  0x08U,
  0x08U,
  0x08U,
};

static uint8_t const DIGIT_EIGHT[7] =
{
  0x0EU,
  0x11U,
  0x11U,
  0x0EU,
  0x11U,
  0x11U,
  0x0EU,
};

static uint8_t const DIGIT_NINE[7] =
{
  0x0EU,
  0x11U,
  0x11U,
  0x0FU,
  0x01U,
  0x11U,
  0x0EU,
};

static uint8_t const DIGIT_MINUS[7] =
{
  0x00U,
  0x00U,
  0x00U,
  0x1FU,
  0x00U,
  0x00U,
  0x00U,
};

static uint8_t const DIGIT_PLUS[7] =
{
  0x00U,
  0x04U,
  0x04U,
  0x1FU,
  0x04U,
  0x04U,
  0x00U,
};

static uint8_t const DIGIT_FAHRENHEIT[7] =
{
  0x18U,
  0x18U,
  0x07U,
  0x04U,
  0x06U,
  0x04U,
  0x04U,
};

static uint8_t const DIGIT_CELSIUS[7] =
{
  0x18U,
  0x18U,
  0x03U,
  0x04U,
  0x04U,
  0x04U,
  0x03U,
};

static uint8_t const DIGIT_BLANK[7] =
{
  0x00U,
  0x00U,
  0x00U,
  0x00U,
  0x00U,
  0x00U,
  0x00U,
};

static uint8_t const * const DIGMAP[SCE574X_DIGIT_0E_BLANK + 1] =
{
  DIGIT_ZERO,                                                                   /* SCE574X_DIGIT_00_ZERO */
  DIGIT_ONE,                                                                    /* SCE574X_DIGIT_01_ONE */
  DIGIT_TWO,                                                                    /* SCE574X_DIGIT_02_TWO */
  DIGIT_THREE,                                                                  /* SCE574X_DIGIT_03_THREE */
  DIGIT_FOUR,                                                                   /* SCE574X_DIGIT_04_FOUR */
  DIGIT_FIVE,                                                                   /* SCE574X_DIGIT_05_FIVE */
  DIGIT_SIX,                                                                    /* SCE574X_DIGIT_06_SIX */
  DIGIT_SEVEN,                                                                  /* SCE574X_DIGIT_07_SEVEN */
  DIGIT_EIGHT,                                                                  /* SCE574X_DIGIT_08_EIGHT */
  DIGIT_NINE,                                                                   /* SCE574X_DIGIT_09_NINE */
  DIGIT_MINUS,                                                                  /* SCE574X_DIGIT_0A_MINUS */
  DIGIT_PLUS,                                                                   /* SCE574X_DIGIT_0B_PLUS */
  DIGIT_FAHRENHEIT,                                                             /* SCE574X_DIGIT_0C_FAHRENHEIT */
  DIGIT_CELSIUS,                                                                /* SCE574X_DIGIT_0D_CELSIUS */
  DIGIT_BLANK,                                                                  /* SCE574X_DIGIT_0E_BLANK */
};

static volatile uint8_t spi_is_complete = FALSE;
static volatile int16_t spi_err = ERR_NONE;

static uint16_t bcd(uint16_t val)
{
  uint16_t i, j = 0;
  
  for (val = MIN(val, 9999), i = 0; val; val /= 10, i++)
  {
    j = (j >> 4) | ((val % 10) << 12);
  }
  
  return j >> ((4 - i) << 2);
}

#ifdef UNITTESTS

static void test_bcd(void)
{
  ASSERT_EQL(bcd(NULL), NULL);
  ASSERT_EQL(bcd(1), 0x0001U);
  ASSERT_EQL(bcd(12), 0x0012U);
  ASSERT_EQL(bcd(345), 0x0345U);
  ASSERT_EQL(bcd(6789), 0x6789U);
  ASSERT_EQL(bcd(0xFFFFU/*65535*/), 0x9999);
}

#endif

static void spi_complete_handler(int16_t err)
{
  spi_is_complete = TRUE;
  spi_err = err;
}

static int16_t spi_write(uint8_t const *data,
                         uint8_t num)
{
  uint8_t i, j;
  
  spi_err = ERR_NONE;
  
  _sce574x_spi_start();
  
  for (i = 0; i < num; i++)
  {
    _sce574x_spi_select_chip();
    
    _sce574x_spi_transfer_async(data[i], spi_complete_handler);
    while (!spi_is_complete) { __asm("NOP"); }
    spi_is_complete = FALSE;
    CHK_BREAK(spi_err);
  
    _sce574x_spi_deselect_chip();
    
    for (j = 0; j < 11; j++) { __asm("NOP"); }                                  /* waiting for ~600ns, 18.432MHz */
  }

  _sce574x_spi_stop();
  
  return spi_err;
}

int16_t sce574x_init(void)
{
  uint8_t byte = OPTCODE_C0_SCL;
  
  return spi_write(&byte, 1);
}

int16_t sce574x_set_brightness(uint8_t brightness,
                               uint8_t is_reduced)
{
  uint8_t byte;
  
  if (brightness > SCE574X_BRIGHTNESS_07_POWER_DOWN)
    return SCE574X_ERR_8001_ARGUMENT;
  
  byte = OPTCODE_E0_LCWD | ((is_reduced & 0x01U) << 3) | brightness;
  
  return spi_write(&byte, 1);
}

int16_t sce574x_set_quantity_output(int16_t val,
                                    uint8_t dig4)
{
  uint16_t res;
  uint8_t dig1, dig2, dig3;
  
  res = bcd(ABS(MIN(MAX(val, -99), 999)));
  
  dig1 = val < 0 ? SCE574X_DIGIT_0A_MINUS : (res >> 8) & 0x0FU;
  dig2 = (res >> 4) & 0x0FU;
  dig3 = res & 0x0FU;

  if (dig1 == SCE574X_DIGIT_0A_MINUS
      && dig2 == SCE574X_DIGIT_00_ZERO)
  {
    dig1 = SCE574X_DIGIT_0E_BLANK;
    dig2 = SCE574X_DIGIT_0A_MINUS;
  }
  else if (dig1 == SCE574X_DIGIT_00_ZERO
           && dig2 == SCE574X_DIGIT_00_ZERO)
  {
    dig1 = SCE574X_DIGIT_0E_BLANK;
    dig2 = SCE574X_DIGIT_0E_BLANK;
  }
  
  if (dig1 == SCE574X_DIGIT_00_ZERO
      && dig2 == SCE574X_DIGIT_00_ZERO)
  {
    dig1 = SCE574X_DIGIT_0E_BLANK;
    dig2 = SCE574X_DIGIT_0E_BLANK;
  }
  else if (dig1 == SCE574X_DIGIT_00_ZERO)
  {
    dig1 = SCE574X_DIGIT_0E_BLANK;
  }
  
  return sce574x_set_custom_output(dig1, dig2, dig3, dig4);
}

int16_t sce574x_set_custom_output(uint8_t dig1,
                                  uint8_t dig2,
                                  uint8_t dig3,
                                  uint8_t dig4)
{
  uint8_t byte;
  
  if (dig1 > SCE574X_DIGIT_0E_BLANK
      || dig2 > SCE574X_DIGIT_0E_BLANK
      || dig3 > SCE574X_DIGIT_0E_BLANK
      || dig4 > SCE574X_DIGIT_0E_BLANK)
    return SCE574X_ERR_8001_ARGUMENT;
  
  byte = OPTCODE_A0_LDA | 0x00U;
  CHK(spi_write(&byte, 1)); CHK(spi_write(DIGMAP[dig1], 7));
  
  byte = OPTCODE_A0_LDA | 0x01U;
  CHK(spi_write(&byte, 1)); CHK(spi_write(DIGMAP[dig2], 7));
  
  byte = OPTCODE_A0_LDA | 0x02U;
  CHK(spi_write(&byte, 1)); CHK(spi_write(DIGMAP[dig3], 7));
  
  byte = OPTCODE_A0_LDA | 0x03U;
  CHK(spi_write(&byte, 1)); CHK(spi_write(DIGMAP[dig4], 7));
  
  return ERR_NONE;
}

#ifdef UNITTESTS

void sce574x_test_suite(void)
{
  test_bcd();
}

#endif
