/**
 * \brief Flash EEPROM emulation functions
 * \copyright Copyright (C) 2014 by IQ2 Development GmbH, Neckartenzlingen. All rights reserved.
 * 
 * This file is subject to the terms and conditions
 * defined in file 'Allgemeine Vertragsbedingungen.pdf',
 * which is part of this source code package.
 */

#ifndef __C_EEPROM_H
#define __C_EEPROM_H

#include "c_define.h"

#ifdef __cplusplus
extern "C" {
#endif

enum
{
  EEPROM_ERR_8000_INTERNAL      = ERR_BASE + 0x00,
  EEPROM_ERR_8001_WRONG_PAR     = ERR_BASE + 0x01,
  EEPROM_ERR_8002_PAR_NOT_INIT  = ERR_BASE + 0x02,
  EEPROM_ERR_8003_NOT_INIT      = ERR_BASE + 0x03,
  EEPROM_ERR_8004_BUSY          = ERR_BASE + 0x04,
};

extern uint8_t eeprom_is_formated;

/**
 *  \brief Init FAL (Flash Access Library) and EEL (EEPROM Emulation library)
 */
int16_t eeprom_init(void);

/**
 *  \brief Write parameter to EEPROM to execute EEL command in enforced mode.
 *
 *  \param parNumber    parameter number (EEL variable identifier)
 *  \param value        pointer to value to write to EEPROM
 */
int16_t eeprom_write(uint8_t parNumber, 
                     uint8_t *value);

/**
 *  \brief Read parameter to EEPROM to execute EEL command in enforced mode.
 *
 *  \param parNumber    parameter number (EEL variable identifier)
 *  \param value        pointer to value to read value into it from EEPROM
 */
int16_t eeprom_read(uint8_t parNumber,
                    uint8_t *value);

/**
 *  Should be called in forever loop.
 */
void eeprom_handle(void);

#ifdef __cplusplus
}
#endif

#endif