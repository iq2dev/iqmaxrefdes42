/**
 * \brief Maxim MAX1482[0|1] IO-Link Device Transceiver Chip Driver
 * \copyright Copyright (C) 2014 by IQ2 Development GmbH, Neckartenzlingen. All rights reserved.
 * \see http://www.maximintegrated.com/datasheet/index.mvp/id/7416
 * 
 * This file is subject to the terms and conditions
 * defined in file 'Allgemeine Vertragsbedingungen.pdf',
 * which is part of this source code package.
 */

#ifndef __C_MAX1482X_H
#define __C_MAX1482X_H

#include "c_define.h"

#ifdef __cplusplus
extern "C" {
#endif

enum
{
  MAX1482X_ERR_8000_SELFTEST      = ERR_BASE + 0x00,
  MAX1482X_ERR_8001_SPI_OVERRUN   = ERR_BASE + 0x01,
  MAX1482X_ERR_8002_UART_PARITY   = ERR_BASE + 0x02,
  MAX1482X_ERR_8003_UART_FRAMING  = ERR_BASE + 0x03,
  MAX1482X_ERR_8004_UART_OVERRUN  = ERR_BASE + 0x04,
};

enum
{
  MAX1482X_EVENT_00_OTEMP   = 0x00U,
  MAX1482X_EVENT_01_UV24    = 0x01U,
  MAX1482X_EVENT_02_UV3_3   = 0x02U,
  MAX1482X_EVENT_03_CQFAULT = 0x03U,
  MAX1482X_EVENT_04_DOFAULT = 0x04U,
  MAX1482X_EVENT_80_SPICOMM = 0x80U,
};

enum
{
  MAX1482X_REG_00_STATUS    = 0x00U,
  MAX1482X_REG_01_CQCONFIG  = 0x01U,
  MAX1482X_REG_02_DIOCONFIG = 0x02U,
  MAX1482X_REG_03_MODE      = 0x03U,
  MAX1482X_REG_04_END       = 0x04U,
};

enum
{
  MAX1482X_STATUS_01_OTEMPINT   = 0x01U,
  MAX1482X_STATUS_02_UV24INT    = 0x02U,
  MAX1482X_STATUS_04_UV33INT    = 0x04U,
  MAX1482X_STATUS_08_CQFAULTINT = 0x08U,  
  MAX1482X_STATUS_10_QLVL       = 0x10U,
  MAX1482X_STATUS_20_DILVL      = 0x20U,
  MAX1482X_STATUS_40_DOFAULTINT = 0x40U,
  MAX1482X_STATUS_80_WUINT      = 0x80U,
};

enum
{
  MAX1482X_CQCONFIG_01_CQLOAD   = 0x01U,
  MAX1482X_CQCONFIG_02_RXDIS    = 0x02U,
  MAX1482X_CQCONFIG_04_Q        = 0x04U,
  MAX1482X_CQCONFIG_08_CQDEN    = 0x08U,  
  MAX1482X_CQCONFIG_10_CQ_PP    = 0x10U,
  MAX1482X_CQCONFIG_20_CQ_NP    = 0x20U,
  MAX1482X_CQCONFIG_40_HISLEW   = 0x40U,
  MAX1482X_CQCONFIG_80_RXFILTER = 0x80U,
};

enum
{
  MAX1482X_DIOCONFIG_01_DILOAD  = 0x01U,
  MAX1482X_DIOCONFIG_02_LIDIS   = 0x02U,
  MAX1482X_DIOCONFIG_04_DOBIT   = 0x04U,
  MAX1482X_DIOCONFIG_08_DOEN    = 0x08U,  
  MAX1482X_DIOCONFIG_10_DOPP    = 0x10U,
  MAX1482X_DIOCONFIG_20_DONP    = 0x20U,
  MAX1482X_DIOCONFIG_40_DOAV    = 0x40U,
  MAX1482X_DIOCONFIG_80_DOINV   = 0x80U,
};

enum
{
  MAX1482X_MODE_01_LDO33DIS = 0x01U,
  MAX1482X_MODE_02_UV33EN   = 0x02U,
  MAX1482X_MODE_04_OTEMP    = 0x04U,
  MAX1482X_MODE_08_UV24     = 0x08U,  
  MAX1482X_MODE_10_CQFAULT  = 0x10U,
  MAX1482X_MODE_20_DOFAULT  = 0x20U,
  MAX1482X_MODE_40_WUINTEN  = 0x40U,
  MAX1482X_MODE_80_RST      = 0x80U,
};

#ifdef MAX1482X_UART_COMPLETE

/**
 * \brief An event flag, when the UART transfer is completed
 * \see 'max1482x_uart_send_async'
 */
extern volatile uint8_t max1482x_uart_complete;

#endif

/**
 * \brief Initialize and perform power-on self-test (POST) procedures
 */
int16_t max1482x_init(void);

/**
 * \brief IRQ's events watcher timer callback (optimal every 30 msec)
 */
void max1482x_watch_irq_events(void);

/**
 * \brief Start monitoring MAX1482x events (appear/disappear)
 *
 * param[in]  on_appear       on a new event appear callback (see 'MAX1482X_EVENT_*')
 * param[in]  on_disappear    on an event disappear callback (see 'MAX1482X_EVENT_*')
 *
 * \see #max1482x_watch_irq_events
 */
void max1482x_listen_events(void (*on_appear)(uint8_t event),
                            void (*on_disappear)(uint8_t event));

/**
 * \brief Start SPI protocol communication
 */
extern void max1482x_spi_start(void);

/**
 * \brief Stop SPI protocol communication
 */
extern void max1482x_spi_stop(void);

/**
 * \brief Write a register value by SPI protocol
 *
 * \param[in] reg   a register number (see the enum 'MAX1482X_REG_*' above)
 * \param[in] val   a register value
 */
int16_t max1482x_spi_write_register(uint8_t reg,
                                    uint8_t val);

/**
 * \brief Read a register value by SPI protocol
 *
 * \param[in]   reg   a register number (see the enum 'MAX1482X_REG_*' above)
 * \param[out]  val   a pointer to a byte value buffer
 */
int16_t max1482x_spi_read_register(uint8_t reg,
                                   uint8_t *ptr);

/**
 * \brief Switch MAX1482x to wait _WURQ signal
 *
 * \param[out]  on_wakeup   on wakeup callback
 */
int16_t max1482x_wait_wakeup(void (*on_wakeup)(void));


/**
 * \brief UART protocol communication start
 *
 * \param[out]  on_byte_recv    on receive per byte callback
 */
int16_t max1482x_uart_start(void (*on_byte_recv)(uint8_t byte,
                                                 int16_t err));

/**
 * \brief UART protocol communication stop
 */
extern void max1482x_uart_stop(void);

/**
 * \brief UART protocol transfer bytes async
 *
 * \param[in]   data    a data buffer to transfer
 * \param[in]   len     a data buffer length
 */
void max1482x_uart_send_async(uint8_t const *data,
                              uint8_t len);

/**
 * \brief Set a new state of the logical input of the DO output
 *
 * \param[in] state   a required state "[ON|OFF]"
 */
extern void max1482x_set_output(uint8_t state);

/* =============== integration API ========================================== */

#if !(defined MAX1482X_IRQ_POLLING)

/**
 * \brief Enable interrupts in order to catching it
 *
 * \param[out]  callback    on interrupt coming callback
 */
extern void _max1482x_listen_irq(void (*callback)(void));

#else

/**
 * \brief Get an interrupt state in order to catching it
 */
extern uint8_t _max1482x_get_irq(void);

#endif

/**
 * \brief Enable SPI protocol chip select (CS)
 */
extern void _max1482x_spi_select_chip(void);

/**
 * \brief Disable SPI protocol chip select (CS)
 */
extern void _max1482x_spi_deselect_chip(void);

/**
 * \brief SPI protocol single byte transfer/receive asynchronous cycle
 *
 * \param[in]   tx            a byte to transfer
 * \param[out]  rx            a byte receive buffer
 * \param[in]   on_complete   on receive complete callback
 */
extern void _max1482x_spi_byte_cycle_async(uint8_t tx,
                                           uint8_t *rx,
                                           void (*on_complete)(int16_t err));

/**
 * \brief Set a new state of the transmitter enable (TXEN)
 *
 * \param[in] state   a required state "[ON|OFF]"
 */
extern void _max1482x_set_txenable(uint8_t state);

/**
 * \brief Enable wake-up interrupt in order to single catching
 *
 * \param[in] callback    on interrupt coming callback
 */
extern void _max1482x_enable_wakeup(void (*callback)(void));

/**
 * \brief Disable wake-up interrupt
 */
extern void _max1482x_disable_wakeup(void);

/**
 * \brief UART protocol communication start
 *
 * \param[in] on_byte_recv    on receive per byte callback
 */
extern void _max1482x_uart_start(void (*on_byte_recv)(uint8_t byte,
                                                      int16_t err));

/**
 * \brief UART protocol asynchronous bytes transfer
 *
 * \param[in] data          a data buffer to transfer
 * \param[in] len           a data buffer length
 * \param[in] on_complete   on transfer complete callback
 */
extern void _max1482x_uart_send_async(uint8_t const *data,
                                      uint8_t len,
                                      void (*on_complete)(void));

#ifdef __cplusplus
}
#endif

#endif