/**
 * \brief Simple test framework based on the assert method
 * \copyright Copyright (C) 2014 by IQ2 Development GmbH, Neckartenzlingen. All rights reserved.
 * 
 * This file is subject to the terms and conditions
 * defined in file 'Allgemeine Vertragsbedingungen.pdf',
 * which is part of this source code package.
 */

#include "c_assert.h"

__far_func char const *__assert_itoa(int16_t val,
                                uint8_t base,
                                char const *pre,
                                char const *post)
{
  static char const SMBL[] = "0123456789ABCDEF";
  
  static char overflow_msg[] = "buffer overflow", buf[128], alrm[128];

  uint16_t i, j = base == 10 && val < 0 ? -val : val;
  char *ptr1 = buf, *ptr2 = buf, tmp;
  
  *buf = NULL;
  *alrm = NULL;
  
  if (base < 2 || base > 16)
    return buf;
  
  do
  {
    i = j;
    j /= base;
    *ptr1++ =  SMBL[i - j * base];
  } while (j);
  
  if (base == 10 && val < 0)
  {
    *ptr1++ = '-';
  }
  
  memset(ptr1, '*', strlen(pre));
  ptr1 += strlen(pre);
  *(ptr1--) = NULL;
  
  while(ptr2 < ptr1)
  {
    tmp = *ptr1;
    *ptr1--= *ptr2;
    *ptr2++ = tmp;
  }
  
  memcpy(buf, pre, strlen(pre));
  strcpy(buf + strlen(buf), post);
  
  return *alrm ? overflow_msg : buf;
}
