/**
 * \brief Simple test framework based on the assert method
 * \copyright Copyright (C) 2014 by IQ2 Development GmbH, Neckartenzlingen. All rights reserved.
 * 
 * This file is subject to the terms and conditions
 * defined in file 'Allgemeine Vertragsbedingungen.pdf',
 * which is part of this source code package.
 */

#ifndef __C_ASSERT_H
#define __C_ASSERT_H

#include "c_define.h"

#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef NDEBUG
#define UNITTESTS
#endif

__far_func char const *__assert_itoa(int16_t val,
                                uint8_t base,
                                char const *pre,
                                char const *post);
  
#define ASSERT(clause) assert(clause)

#define ASSERT_MSG(clause, msg) assert(clause && msg)

#define ASSERT_EQL(el1, el2) \
  ASSERT((el1) == (el2))

#define ASSERT_EQL_MSG(el1, el2, msg) \
  ASSERT_MSG((el1) == (el2), msg)

#define ASSERT_EQL_STR(str1, str2) \
  ASSERT(!strcmp(str1, str2));

#define ASSERT_NULL(el) \
  ASSERT_EQL_MSG(el, NULL, "Should be 'NULL'")

#define ASSERT_NOT_NULL(el) \
  ASSERT_MSG((el) != NULL, "Shouldn't be 'NULL'")

#define ASSERT_OFF(exp) \
  ASSERT_EQL_MSG(exp, OFF, "Should be 'OFF'")

#define ASSERT_ON(exp) \
  ASSERT_EQL_MSG(exp, ON, "Should be 'ON'")

#define ASSERT_FALSE(exp) \
  ASSERT_EQL_MSG(exp, FALSE, "Should be 'FALSE'")

#define ASSERT_TRUE(exp) \
  ASSERT_EQL_MSG(exp, TRUE, "Should be 'TRUE'")

#define ASSERT_SUCCESS(fn) \
  do { \
    int16_t __rc = (int16_t)(fn); \
    if (__rc >= -1) break; \
    _CSTD __rl78abi_assert(__assert_itoa(__rc, 16, #fn " && ", " && Should be 'SUCCESS'"), __FILE__, __LINE__), _STEPPOINT; \
  } while(0)

#define ASSERT_FAILED(fn) \
  do { \
    int16_t __rc = (int16_t)(fn); \
    if (__rc < -1) break; \
    _CSTD __rl78abi_assert(__assert_itoa(__rc, 16, #fn " && ", " && Should be 'FAILED'"), __FILE__, __LINE__), _STEPPOINT; \
  } while(0)

#ifdef __cplusplus
}
#endif

#endif
