/**
 * \brief Maxim MAX31865 RTD-to-Digital Converter
 * \copyright Copyright (C) 2014 by IQ2 Development GmbH, Neckartenzlingen. All rights reserved.
 * \see http://www.maximintegrated.com/datasheet/index.mvp/id/7900
 * 
 * This file is subject to the terms and conditions
 * defined in file 'Allgemeine Vertragsbedingungen.pdf',
 * which is part of this source code package.
 */

#ifndef __C_MAX31865_H
#define __C_MAX31865_H

#include "c_define.h"

#ifdef __cplusplus
extern "C" {
#endif

enum
{
  MAX31865_ERR_8000_SELFTEST    = ERR_BASE + 0x00,
  MAX31865_ERR_8001_ARGUMENT    = ERR_BASE + 0x01,
  MAX31865_ERR_8002_SPI_OVERRUN = ERR_BASE + 0x02,
  MAX31865_ERR_8003_VOLTAGE     = ERR_BASE + 0x03,
};

enum
{
  MAX31865_REG_00_CONF        = 0x00U,
  MAX31865_REG_01_RTD_MSB     = 0x01U,
  MAX31865_REG_02_RTD_LSB     = 0x02U,
  MAX31865_REG_03_RTDHIGH_MSB = 0x03U,
  MAX31865_REG_04_RTDHIGH_LSB = 0x04U,
  MAX31865_REG_05_RTDLOW_MSB  = 0x05U,
  MAX31865_REG_06_RTDLOW_LSB  = 0x06U,
  MAX31865_REG_07_STATUS      = 0x07U,
  MAX31865_REG_08_END         = 0x08U,
};

enum
{
  MAX31865_CONF_01_SINCFILTER   = 0x01U,
  MAX31865_CONF_02_CLEARSTATUS  = 0x02U,
  MAX31865_CONF_0D_FAULTDETECT  = 0x0DU,
  MAX31865_CONF_10_3WIRE        = 0x10U,  
  MAX31865_CONF_20_1SHOT        = 0x20U,
  MAX31865_CONF_40_CONVMODE     = 0x40U,
  MAX31865_CONF_80_VBIAS        = 0x80U,
};

enum
{
  MAX31865_RTD_01_FAULT = 0x01U,
};

enum
{
  MAX31865_STATUS_04_VOLTAGE      = 0x04U,
  MAX31865_STATUS_08_RTDIN_FORCE  = 0x08U,  
  MAX31865_STATUS_10_REFIN_FORCE  = 0x10U,
  MAX31865_STATUS_20_REFIN        = 0x20U,
  MAX31865_STATUS_40_RTDLOW       = 0x40U,
  MAX31865_STATUS_80_RTDHIGH      = 0x80U,
};

enum
{
  MAX31865_UNIT_00_FAHRENHEIT = 0x00U,
  MAX31865_UNIT_01_CELSIUS    = 0x01U,
};

/**
 * \brief Initialize and perform power-on self-test (POST) procedures
 */
int16_t max31865_init(void);

/**
 * \brief Read the RTD resistance registers value (15bits)
 */
int16_t max31865_read_rtd(void);

/**
 * \brief Perform common calculation in order to buffer them
 *
 * \param[in] rref    a reference resistance
 * \param[in] r0      a nominal resistance
 *
 * \see #max31865_scale_rtd
 */
float max31865_precalc(uint32_t rref,
                       uint32_t r0);

/**
 * \brief Calculate a plain resistance registers value to physical units
 *
 * \param[in]   rtd       resistance registers value (RTD, 15bits)
 * \param[in]   precalc   a precalculated coefficient
 * \param[out]  val       a physical temperature value (as float!)
 * \param[in]   unit      a physical unit (see 'MAX31865_UNIT_*')
 *
 * \see #max31865_read_rtd
 * \see #max31865_prescale_rtd
 * \see #max31865_scale_rtd
 */
int16_t max31865_calc(uint16_t rtd,
                      float precalc,
                      float *val,
                      uint8_t unit);

/**
 * \brief Scale a plain resistance registers value to physical units
 *
 * \param[in]   rtd       resistance registers value (RTD, 15bits)
 * \param[in]   precalc   a precalculated coefficient
 * \param[out]  val       a physical temperature value (as numeric!)
 * \param[in]   unit      a physical unit (see 'MAX31865_UNIT_*')
 *
 * \see #max31865_read_rtd
 * \see #max31865_prescale_rtd
 * \see #max31865_calc
 */
int16_t max31865_scale_rtd(uint16_t rtd,
                           float precalc,
                           int16_t *val,
                           uint8_t unit);

/**
 * \brief Write registers' values by SPI protocol
 *
 * \param[in] reg     a register number (see the enum 'MAX31865_REG_*' above)
 * \param[in] data    a register values
 * \param[in] num     a required registers number to write (must be <= 4)
 */
int16_t max31865_write_registers(uint8_t reg,
                                 uint8_t const *data,
                                 uint8_t num);

/**
 * \brief Read registers' values by SPI protocol
 *
 * \param[in]   reg   a register number (see the enum 'MAX31865_REG_*' above)
 * \param[out]  ptr   a pointer to a byte value buffer
 * \param[in]   num   a required registers number to read (must be <= 8)
 */
int16_t max31865_read_registers(uint8_t reg,
                                uint8_t *ptr,
                                uint8_t num);

/* =============== integration API ========================================== */

/**
 * \brief Start SPI protocol communication
 */
extern void _max31865_spi_start(void);

/**
 * \brief Stop SPI protocol communication
 */
extern void _max31865_spi_stop(void);

/**
 * \brief Enable SPI protocol chip select (CS)
 */
extern void _max31865_spi_select_chip(void);

/**
 * \brief Disable SPI protocol chip select (CS)
 */
extern void _max31865_spi_deselect_chip(void);

/**
 * \brief SPI protocol several bytes transfer/receive asynchronous cycle
 *
 * \param[in]   tx            a transfer byte array
 * \param[out]  rx            a receive byte array (with equal size as previous)
 * \param[in]   num           a number of bites to transfer and to receive
 * \param[in]   on_complete   on receive complete callback
 */
extern void _max31865_spi_transfer_async(uint8_t const *tx,
                                         uint8_t *rx,
                                         uint8_t num,
                                         void (*on_complete)(int16_t err));

#ifdef __cplusplus
}
#endif

#endif