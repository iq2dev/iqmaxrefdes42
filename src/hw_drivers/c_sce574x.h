/**
 * \brief OSRAM SCE574x 4-Character 5x7 Dot Matrix Serial Input Dot Addressable Display
 * \copyright Copyright (C) 2014 by IQ2 Development GmbH, Neckartenzlingen. All rights reserved.
 * 
 * This file is subject to the terms and conditions
 * defined in file 'Allgemeine Vertragsbedingungen.pdf',
 * which is part of this source code package.
 */

#ifndef __C_SCE574X_H
#define __C_SCE574X_H

#include "c_define.h"

#ifdef __cplusplus
extern "C" {
#endif

enum
{
  SCE574X_ERR_8000_SELFTEST = ERR_BASE + 0x00,
  SCE574X_ERR_8001_ARGUMENT = ERR_BASE + 0x01,
};

enum
{
  SCE574X_BRIGHTNESS_00_100PERCENT  = 0x00U,
  SCE574X_BRIGHTNESS_01_53PERCENT   = 0x01U,
  SCE574X_BRIGHTNESS_02_40PERCENT   = 0x02U,
  SCE574X_BRIGHTNESS_03_27PERCENT   = 0x03U,
  SCE574X_BRIGHTNESS_04_20PERCENT   = 0x04U,
  SCE574X_BRIGHTNESS_05_13PERCENT   = 0x05U,
  SCE574X_BRIGHTNESS_06_6_6PERCENT  = 0x06U,
  SCE574X_BRIGHTNESS_07_POWER_DOWN  = 0x07U,
};

enum
{
  SCE574X_DIGIT_00_ZERO       = 0x00U,
  SCE574X_DIGIT_01_ONE        = 0x01U,
  SCE574X_DIGIT_02_TWO        = 0x02U,
  SCE574X_DIGIT_03_THREE      = 0x03U,
  SCE574X_DIGIT_04_FOUR       = 0x04U,
  SCE574X_DIGIT_05_FIVE       = 0x05U,
  SCE574X_DIGIT_06_SIX        = 0x06U,
  SCE574X_DIGIT_07_SEVEN      = 0x07U,
  SCE574X_DIGIT_08_EIGHT      = 0x08U,
  SCE574X_DIGIT_09_NINE       = 0x09U,
  SCE574X_DIGIT_0A_MINUS      = 0x0AU,
  SCE574X_DIGIT_0B_PLUS       = 0x0BU,
  SCE574X_DIGIT_0C_FAHRENHEIT = 0x0CU,
  SCE574X_DIGIT_0D_CELSIUS    = 0x0DU,
  SCE574X_DIGIT_0E_BLANK      = 0x0EU,
};

/**
 * \brief Initialize and perform power-on self-test (POST) procedures
 */
int16_t sce574x_init(void);

/**
 * \brief Set a display brightness
 *
 * \param[in] brightness    a brightness (see 'SCE574X_BRIGHTNESS_*')
 * \param[in] is_reduced    is 'Peak Current' reduced (from 100% to to 12.5%)
 */
int16_t sce574x_set_brightness(uint8_t brightness,
                               uint8_t is_reduced);

/**
 * \brief Set a numeric quantity display output
 *
 * \param[in] val     an numeric value ([-99..999])
 * \param[in] dig4    a first digit index (see 'SCE574X_DIGIT_*')
 */
int16_t sce574x_set_quantity_output(int16_t val,
                                    uint8_t dig4);

/**
 * \brief Set a custom display output
 *
 * \param[in] dig1    a first digit index (see 'SCE574X_DIGIT_*')
 * \param[in] dig2    a first digit index (see 'SCE574X_DIGIT_*')
 * \param[in] dig3    a first digit index (see 'SCE574X_DIGIT_*')
 * \param[in] dig4    a first digit index (see 'SCE574X_DIGIT_*')
 */
int16_t sce574x_set_custom_output(uint8_t dig1,
                                  uint8_t dig2,
                                  uint8_t dig3,
                                  uint8_t dig4);

/* =============== unittests API ============================================ */

/**
 * \brief An aggregate collection of module's test cases.
 */
void sce574x_test_suite(void);

/* =============== integration API ========================================== */

/**
 * \brief Start SPI protocol communication
 */
extern void _sce574x_spi_start(void);

/**
 * \brief Stop SPI protocol communication
 */
extern void _sce574x_spi_stop(void);

/**
 * \brief Enable SPI protocol chip select (CS)
 */
extern void _sce574x_spi_select_chip(void);

/**
 * \brief Disable SPI protocol chip select (CS)
 */
extern void _sce574x_spi_deselect_chip(void);

/**
 * \brief SPI protocol single byte transfer asynchronous cycle
 *
 * \param[in]   byte          a transfer byte array
 * \param[in]   on_complete   on transfer complete callback
 */
extern void _sce574x_spi_transfer_async(uint8_t byte,
                                        void (*on_complete)(int16_t err));

#ifdef __cplusplus
}
#endif

#endif