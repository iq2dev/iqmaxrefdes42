/**
 * \brief Simple sensor domain application logic
 * \copyright Copyright (C) 2014 by IQ2 Development GmbH, Neckartenzlingen. All rights reserved.
 * 
 * This file is subject to the terms and conditions
 * defined in file 'Allgemeine Vertragsbedingungen.pdf',
 * which is part of this source code package.
 */

#include "sensor_logic.h"

#include "c_assert.h"

#ifdef UNITTESTS

static void test_init(void)
{
  sensor_logic_switch_point_t sp;
  
  ASSERT_FAILED(
    sensor_logic_init(&sp, SENSOR_LOGIC_SP_LOGIC_0_NO, 100, 100));
  
  ASSERT_SUCCESS(
    sensor_logic_init(&sp, SENSOR_LOGIC_SP_LOGIC_0_NO, 100, 10));
  
  ASSERT_FALSE(sp.flag);
  ASSERT_FALSE(sp.flag_changed);
}

#endif

int16_t sensor_logic_init(sensor_logic_switch_point_t *sp,
                          uint8_t logic,
                          uint16_t level,
                          uint16_t hyst)
{
  if (!sp)
    return SENSOR_LOGIC_ERR_8000_NULL_ARGUMENT;
  
  if (hyst >= level)
    return SENSOR_LOGIC_ERR_8001_INCONSISTENT;
  
  sp->flag = logic ? TRUE : FALSE;
  sp->flag_changed = FALSE;

  sp->logic = logic;
  
  sp->level = level;
  sp->hyst = hyst;
  
  sp->__prev_value = -1;
  
  return ERR_NONE;
}

#ifdef UNITTESTS

static void test_teachin(void)
{
  sensor_logic_switch_point_t sp;
  
  ASSERT_SUCCESS(
    sensor_logic_init(&sp, SENSOR_LOGIC_SP_LOGIC_0_NO, 100, 10));
  
  ASSERT_FAILED(sensor_logic_teachin(&sp, 100, 100));
  
  ASSERT_SUCCESS(sensor_logic_teachin(&sp, 150, 30));
  
  ASSERT_EQL(sp.level, 150);
  ASSERT_EQL(sp.hyst, 30);
}

#endif

int16_t sensor_logic_teachin(sensor_logic_switch_point_t *sp,
                             uint16_t level,
                             uint16_t hyst)
{
  if (!sp)
    return SENSOR_LOGIC_ERR_8000_NULL_ARGUMENT;
  
  if (hyst >= level)
    return SENSOR_LOGIC_ERR_8001_INCONSISTENT;
  
  sp->level = level;
  sp->hyst = hyst;
  
  return sensor_logic_process(sp, sp->__prev_value);
}

#ifdef UNITTESTS

static void test_normally_open(void)
{
  sensor_logic_switch_point_t sp;
  
  ASSERT_SUCCESS(
    sensor_logic_init(&sp, SENSOR_LOGIC_SP_LOGIC_0_NO, 100, 30));
  
  ASSERT_SUCCESS(sensor_logic_process(&sp, 110));
  
  ASSERT_TRUE(sp.flag);
  ASSERT_FALSE(sp.flag_changed);
  
  ASSERT_SUCCESS(sensor_logic_process(&sp, 80));
  
  ASSERT_TRUE(sp.flag);
  ASSERT_FALSE(sp.flag_changed);
  
  ASSERT_SUCCESS(sensor_logic_process(&sp, 70));
  
  ASSERT_FALSE(sp.flag);
  ASSERT_TRUE(sp.flag_changed);
  
  ASSERT_SUCCESS(sensor_logic_process(&sp, 90));
  
  ASSERT_FALSE(sp.flag);
  ASSERT_FALSE(sp.flag_changed);
  
  ASSERT_SUCCESS(sensor_logic_process(&sp, 100));
  
  ASSERT_TRUE(sp.flag);
  ASSERT_TRUE(sp.flag_changed);
}

static void test_normally_closed(void)
{
  sensor_logic_switch_point_t sp;
  
  ASSERT_SUCCESS(
    sensor_logic_init(&sp, SENSOR_LOGIC_SP_LOGIC_1_NC, 100, 30));
  
  ASSERT_SUCCESS(sensor_logic_process(&sp, 110));
  
  ASSERT_FALSE(sp.flag);
  ASSERT_FALSE(sp.flag_changed);
  
  ASSERT_SUCCESS(sensor_logic_process(&sp, 80));
  
  ASSERT_FALSE(sp.flag);
  ASSERT_FALSE(sp.flag_changed);
  
  ASSERT_SUCCESS(sensor_logic_process(&sp, 70));
  
  ASSERT_TRUE(sp.flag);
  ASSERT_TRUE(sp.flag_changed);
  
  ASSERT_SUCCESS(sensor_logic_process(&sp, 90));
  
  ASSERT_TRUE(sp.flag);
  ASSERT_FALSE(sp.flag_changed);
  
  ASSERT_SUCCESS(sensor_logic_process(&sp, 100));
  
  ASSERT_FALSE(sp.flag);
  ASSERT_TRUE(sp.flag_changed);
}

#endif

int16_t sensor_logic_process(sensor_logic_switch_point_t *sp,
                             uint16_t val)
{
  uint8_t prev_flag = sp->flag;
  
  if (!sp)
    return SENSOR_LOGIC_ERR_8000_NULL_ARGUMENT;
  
  if ((sp->flag & 1U) ^ (sp->logic & 1U))
  {
    sp->flag =
      (val > sp->level - sp->hyst ? TRUE : FALSE);
  }
  else
  {
    sp->flag =
      (val >= sp->level ? TRUE : FALSE);
  }
  
  if (sp->logic)
  {
    sp->flag = sp->flag ? FALSE : TRUE;
  }
  
  sp->flag_changed = prev_flag != sp->flag
    && (sp->__prev_value != -1 || prev_flag != (sp->logic ? TRUE : FALSE))
      ? TRUE : FALSE;
  
  sp->__prev_value = val;
  
  return ERR_NONE;
}

#ifdef UNITTESTS

void sensor_logic_test_suite(void)
{
  test_init();
  
  test_teachin();
  
  test_normally_open();
  test_normally_closed();
}

#endif
