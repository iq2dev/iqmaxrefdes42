/**
 * \brief Simple sensor domain application logic
 * \copyright Copyright (C) 2014 by IQ2 Development GmbH, Neckartenzlingen. All rights reserved.
 * 
 * This file is subject to the terms and conditions
 * defined in file 'Allgemeine Vertragsbedingungen.pdf',
 * which is part of this source code package.
 */

#ifndef __SENSOR_LOGIC_H
#define __SENSOR_LOGIC_H

#include "c_define.h"

enum
{
  SENSOR_LOGIC_ERR_8000_NULL_ARGUMENT = ERR_BASE + 0x00,
  SENSOR_LOGIC_ERR_8001_INCONSISTENT  = ERR_BASE + 0x01,
};

enum
{
  SENSOR_LOGIC_SP_LOGIC_0_NO = 0,
  SENSOR_LOGIC_SP_LOGIC_1_NC = 1,
};

typedef struct
{  
  uint8_t flag;
  uint8_t flag_changed;

  uint8_t logic;
  
  uint16_t level;
  uint16_t hyst;
  
  uint16_t __prev_value;
  
} sensor_logic_switch_point_t;

/**
 * \brief Init a sensor application module data structure
 *
 * \param[out]  sp      a simple switch point data structure
 * \param[in]   logic   a switch point logic (NO = 0/NC = 1)
 * \param[in]   level   a switch point level (SP1)
 * \param[in]   hyst    a switch point hysteresis (HY)
 */
int16_t sensor_logic_init(sensor_logic_switch_point_t *sp,
                          uint8_t logic,
                          uint16_t level,
                          uint16_t hyst);

/**
 * \brief Change switch point value (SP/HY)
 *
 * \param[in,out] sp      a simple switch point data structure
 * \param[in]     level   a new switch point level (SP1)
 * \param[in]     hyst    a new switch point hysteresis (HY)
 */
int16_t sensor_logic_teachin(sensor_logic_switch_point_t *sp,
                             uint16_t level,
                             uint16_t hyst);

/**
 * \brief Process a current value through a simple switch point domain logic
 *
 * \param[in,out] sp      a simple switch point data structure
 * \param[in]     value   a current value
 */
int16_t sensor_logic_process(sensor_logic_switch_point_t *sp,
                             uint16_t val);

/* =============== unittests API ============================================ */

/**
 * \brief An aggregate collection of module's test cases.
 */
void sensor_logic_test_suite(void);

#endif
