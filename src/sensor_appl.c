/**
 * \brief Maxim MAXREFDES42 IO-Link Light Sensors Application
 * \copyright Copyright (C) 2014 by IQ2 Development GmbH, Neckartenzlingen. All rights reserved.
 * 
 * This file is subject to the terms and conditions
 * defined in file 'Allgemeine Vertragsbedingungen.pdf',
 * which is part of this source code package.
 */

#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "r_cg_wdt.h"

#include "c_max1482x.h"
#include "c_max31865.h"
#include "c_sce574x.h"
#include "c_eeprom.h"

#include "iqstack_dev-20140113-v1.1.2.6_precompiled_settings.h"
#include "iqstack_dev.h"

#include "sensor_logic.h"

#define VERSION_MAJOR 1
#define VERSION_MINOR 3
#define VERSION_PATCH 3

#define VERSION_STR \
  ("v" TOSTR(VERSION_MAJOR) "." TOSTR(VERSION_MINOR) "." TOSTR(VERSION_PATCH))
   
#define EEPROM_00_FORMAT_ADDR           0x00
#define EEPROM_A0_AMBTEMP_SP_LEVEL      0xA0
#define EEPROM_A1_AMBTEMP_SP_HYSTERESIS 0xA1
#define EEPROM_A2_AMBTEMP_DISPLAY_SCALE 0xA2
#define EEPROM_A3_NWIRE_TYPE_CONNECTION 0xA3
#define EEPROM_A9_REFERENCE_RESISTANCE  0xA9
#define EEPROM_AA_NOMINAL_RESISTANCE    0xAA

enum
{
  IOL_INDEX_0100_AMBTEMP_SP_LEVEL         = 0x0100U,
  IOL_INDEX_0101_AMBTEMP_SP_HYSTERESIS    = 0x0101U,
  IOL_INDEX_0102_AMBTEMP_DISPLAY_SCALE    = 0x0102U,
  IOL_INDEX_0103_NWIRE_TYPE_CONNECTION    = 0x0103U,
  IOL_INDEX_0104_MAX31865_CONF            = 0x0104U,
  IOL_INDEX_0105_MAX31865_RTD             = 0x0105U,
  IOL_INDEX_0106_MAX31865_RTDHIGH         = 0x0106U,
  IOL_INDEX_0107_MAX31865_RTDLOW          = 0x0107U,
  IOL_INDEX_0108_MAX31865_STATUS          = 0x0108U,
  IOL_INDEX_0109_REFERENCE_RESISTANCE     = 0x0109U,
  IOL_INDEX_010A_NOMINAL_RESISTANCE       = 0x010AU,
  IOL_INDEX_010B_AMBTEMP_F                = 0x010BU,
  IOL_INDEX_010C_AMBTEMP_C                = 0x010CU,
  IOL_INDEX_010D_AMBTEMP_SP_LEVEL_F       = 0x010DU,
  IOL_INDEX_010E_AMBTEMP_SP_LEVEL_C       = 0x010EU,
  IOL_INDEX_010F_AMBTEMP_SP_HYSTERESIS_F  = 0x010FU,
  IOL_INDEX_0110_AMBTEMP_SP_HYSTERESIS_C  = 0x0110U,
};

enum
{
  IOL_ISDU_ERR_8000_APP_DEV               = 0x8000U,
  IOL_ISDU_ERR_8011_IDX_NOTAVAIL          = 0x8011U,
  IOL_ISDU_ERR_8012_SUBIDX_NOTAVAIL       = 0x8012U,
  IOL_ISDU_ERR_8020_SERV_NOTAVAIL         = 0x8020U,
  IOL_ISDU_ERR_8021_SERV_NOTAVAIL_LOCCTRL = 0x8021U,
  IOL_ISDU_ERR_8022_SERV_NOTAVAIL_DEVCTRL = 0x8022U,
  IOL_ISDU_ERR_8023_IDX_NOT_WRITEABLE     = 0x8023U,
  IOL_ISDU_ERR_8030_PAR_VALOUTOFRNG       = 0x8030U,
  IOL_ISDU_ERR_8031_PAR_VALGTLIM          = 0x8031U,
  IOL_ISDU_ERR_8032_PAR_VALLTLIM          = 0x8032U,
  IOL_ISDU_ERR_8033_VAL_LENOVRRUN         = 0x8033U,
  IOL_ISDU_ERR_8034_VAL_LENUNDRUN         = 0x8034U,
  IOL_ISDU_ERR_8035_FUNC_NOTAVAIL         = 0x8035U,
  IOL_ISDU_ERR_8036_FUNC_UNAVAILTEMP      = 0x8036U,
  IOL_ISDU_ERR_8040_PAR_SETINVALID        = 0x8040U,
  IOL_ISDU_ERR_8041_PAR_SETINCONSIST      = 0x8041U,
  IOL_ISDU_ERR_8082_APP_DEVNOTDRY         = 0x8082U,
};

static DDL_EH_EventT const IOL_EVENT_4210_TEMPOVERRUN_APPEARS =
{
  DDL_EH_EVENT_INSTANCE_APPL,
  DDL_EH_EVENT_TYPE_WARNING,
  DDL_EH_EVENT_MODE_APPEARS,
  DDL_EH_EVENT_SOURCE_REMOTE,
  0x4210U,
};

static DDL_EH_EventT const IOL_EVENT_4210_TEMPOVERRUN_DISAPPEARS =
{
  DDL_EH_EVENT_INSTANCE_APPL,
  DDL_EH_EVENT_TYPE_WARNING,
  DDL_EH_EVENT_MODE_DISAPPEARS,
  DDL_EH_EVENT_SOURCE_REMOTE,
  0x4210U,
};

static DDL_EH_EventT const IOL_EVENT_5000_ERR_HARDWARE =
{
  DDL_EH_EVENT_INSTANCE_APPL,
  DDL_EH_EVENT_TYPE_ERROR,
  DDL_EH_EVENT_MODE_SINGLE_SHOT,
  DDL_EH_EVENT_SOURCE_REMOTE,
  0x5000U,
};

static DDL_EH_EventT const IOL_EVENT_5111_SUPPLYUNDERRUN_APPEARS =
{
  DDL_EH_EVENT_INSTANCE_APPL,
  DDL_EH_EVENT_TYPE_WARNING,
  DDL_EH_EVENT_MODE_APPEARS,
  DDL_EH_EVENT_SOURCE_REMOTE,
  0x5111U,
};

static DDL_EH_EventT const IOL_EVENT_5111_SUPPLYUNDERRUN_DISAPPEARS =
{
  DDL_EH_EVENT_INSTANCE_APPL,
  DDL_EH_EVENT_TYPE_WARNING,
  DDL_EH_EVENT_MODE_DISAPPEARS,
  DDL_EH_EVENT_SOURCE_REMOTE,
  0x5111U,
};

static DDL_EH_EventT const IOL_EVENT_6000_ERR_SOFTWARE =
{
  DDL_EH_EVENT_INSTANCE_APPL,
  DDL_EH_EVENT_TYPE_ERROR,
  DDL_EH_EVENT_MODE_SINGLE_SHOT,
  DDL_EH_EVENT_SOURCE_REMOTE,
  0x6000U,
};

static DDL_EH_EventT const IOL_EVENT_7710_SHORTCIRCUIT_APPEARS =
{
  DDL_EH_EVENT_INSTANCE_APPL,
  DDL_EH_EVENT_TYPE_ERROR,
  DDL_EH_EVENT_MODE_APPEARS,
  DDL_EH_EVENT_SOURCE_REMOTE,
  0x7710U,
};

static DDL_EH_EventT const IOL_EVENT_7710_SHORTCIRCUIT_DISAPPEARS =
{
  DDL_EH_EVENT_INSTANCE_APPL,
  DDL_EH_EVENT_TYPE_ERROR,
  DDL_EH_EVENT_MODE_DISAPPEARS,
  DDL_EH_EVENT_SOURCE_REMOTE,
  0x7710U,
};

static volatile uint8_t iol_event_ready;

static uint8_t iol_events_number;
static DDL_EH_EventT iol_events[16];

static uint16_t ambtemp_pv;                                                     /* ambient temperature process value */
static sensor_logic_switch_point_t ambtemp_sp;                                  /* ambient temperature switch-point */

static uint8_t display_scale = MAX31865_UNIT_01_CELSIUS;                        /* ambient temperature display scale (�C by default) */

static uint8_t nwire_type = 2;

static uint32_t rref = 400000, r0 = 100000;
static float precalc;

static void add_iol_event(DDL_EH_EventT const *event)
{
  if (iol_events_number == 16)
    return;
  
  iol_events[iol_events_number++] = *event;
}

static void iolchip_event_appear(uint8_t event)
{
  switch (event)
  {      
    case MAX1482X_EVENT_00_OTEMP:
      add_iol_event(&IOL_EVENT_4210_TEMPOVERRUN_APPEARS);
      break;
      
    case MAX1482X_EVENT_01_UV24:
    case MAX1482X_EVENT_02_UV3_3:
      add_iol_event(&IOL_EVENT_5111_SUPPLYUNDERRUN_APPEARS);
      break;      
      
    case MAX1482X_EVENT_03_CQFAULT:
    case MAX1482X_EVENT_04_DOFAULT:
      add_iol_event(&IOL_EVENT_7710_SHORTCIRCUIT_APPEARS);
      break;
      
    case MAX1482X_EVENT_80_SPICOMM:
      add_iol_event(&IOL_EVENT_5000_ERR_HARDWARE);
      break;
      
    default:
      add_iol_event(&IOL_EVENT_6000_ERR_SOFTWARE);
      break;
  }
}

static void iolchip_event_disappear(uint8_t event)
{
  switch (event)
  {
    case MAX1482X_EVENT_00_OTEMP:
      add_iol_event(&IOL_EVENT_4210_TEMPOVERRUN_DISAPPEARS);
      break;
      
    case MAX1482X_EVENT_01_UV24:
    case MAX1482X_EVENT_02_UV3_3:
      add_iol_event(&IOL_EVENT_5111_SUPPLYUNDERRUN_DISAPPEARS);
      break;

    case MAX1482X_EVENT_03_CQFAULT:
    case MAX1482X_EVENT_04_DOFAULT:
      add_iol_event(&IOL_EVENT_7710_SHORTCIRCUIT_DISAPPEARS);
      break;
      
    default:
      add_iol_event(&IOL_EVENT_6000_ERR_SOFTWARE);
      break;
  }
}

static int16_t sensorchip_init(void)
{
  uint8_t byte = MAX31865_CONF_40_CONVMODE | MAX31865_CONF_80_VBIAS;            /* goto into automatic (continuous) conversion mode, ~17msec */  

  return max31865_write_registers(MAX31865_REG_00_CONF, &byte, 1);
}

static int16_t displaychip_init(void)
{
  CHK(sce574x_set_brightness(SCE574X_BRIGHTNESS_03_27PERCENT, FALSE));
  
  return sce574x_set_custom_output(SCE574X_DIGIT_0A_MINUS,
    SCE574X_DIGIT_0A_MINUS, SCE574X_DIGIT_0A_MINUS, SCE574X_DIGIT_0A_MINUS);
}

void DA_Init(void)
{
  uint8_t buf[4] = { 0x00, 0x00, 0x00, 0x00 }, byte;
  
  uint16_t level = 0x23BCU, hyst = 0x20U;
  
  if (eeprom_is_formated)
  {
    do
    {
      SET_BIGEND_UINT16(buf, level);
      CHK_BREAK(eeprom_write(EEPROM_A0_AMBTEMP_SP_LEVEL, buf));
      
      SET_BIGEND_UINT16(buf, hyst);
      CHK_BREAK(eeprom_write(EEPROM_A1_AMBTEMP_SP_HYSTERESIS, buf));
      
      *buf = display_scale;
      CHK_BREAK(eeprom_write(EEPROM_A2_AMBTEMP_DISPLAY_SCALE, buf));
      
      *buf = nwire_type;
      CHK_BREAK(eeprom_write(EEPROM_A3_NWIRE_TYPE_CONNECTION, buf));
      
      SET_BIGEND_ULONG32(buf, rref);
      CHK_BREAK(eeprom_write(EEPROM_A9_REFERENCE_RESISTANCE, buf + 1));

      SET_BIGEND_ULONG32(buf, r0);
      CHK_BREAK(eeprom_write(EEPROM_AA_NOMINAL_RESISTANCE, buf + 1));
      
    } while (0U);
  }
  else
  {
    do
    {
      CHK_BREAK(eeprom_read(EEPROM_A0_AMBTEMP_SP_LEVEL, buf));
      level = GET_BIGEND_UINT16(buf);
      
      CHK_BREAK(eeprom_read(EEPROM_A1_AMBTEMP_SP_HYSTERESIS, buf));
      hyst = GET_BIGEND_UINT16(buf);
      
      CHK_BREAK(eeprom_read(EEPROM_A2_AMBTEMP_DISPLAY_SCALE, &display_scale));
      CHK_BREAK(eeprom_read(EEPROM_A3_NWIRE_TYPE_CONNECTION, &nwire_type));
      
      CHK_BREAK(eeprom_read(EEPROM_A9_REFERENCE_RESISTANCE, buf + 1));
      buf[0] = 0, rref = GET_BIGEND_ULONG32(buf);

      CHK_BREAK(eeprom_read(EEPROM_AA_NOMINAL_RESISTANCE, buf + 1));
      buf[0] = 0, r0 = GET_BIGEND_ULONG32(buf);
      
    } while (0U);
  }
  
  max1482x_listen_events(
    iolchip_event_appear, iolchip_event_disappear);
  
  CHK_VOID2(sensorchip_init(),
    add_iol_event(&IOL_EVENT_5000_ERR_HARDWARE));
  
  CHK_VOID2(displaychip_init(),
    add_iol_event(&IOL_EVENT_5000_ERR_HARDWARE));
  
  CHK_VOID2(
    sensor_logic_init(&ambtemp_sp, SENSOR_LOGIC_SP_LOGIC_0_NO, level, hyst),
    add_iol_event(&IOL_EVENT_6000_ERR_SOFTWARE));
  
  do
  {
    CHK_BREAK(max31865_read_registers(MAX31865_REG_00_CONF, &byte, 1));
    SET_BIT(byte, 4, nwire_type != 2 && nwire_type != 4);
    CHK_BREAK(max31865_write_registers(MAX31865_REG_00_CONF, &byte, 1));
              
  } while (0U);
  
  precalc = max31865_precalc(rref, r0);
  
  DEV_IOL_Init();
  
  DSM_SetDeviceMode(DSM_MODE_IDLE);

  DA_InitDSM();

  DSM_SetDeviceMode(DSM_MODE_SIO);
}

void DA_InitDSM(void)
{
  DSM_DeviceComT *DeviceCom;
  DSM_DeviceIdentT *DeviceIdent;
  
  DeviceCom = DSM_GetDeviceCom();
  
  DeviceCom->supportedSIOMode = DPL_MODE_INACTIVE;
  DeviceCom->supportedBaudrate = DPL_SUPPORTED_BAUDRATE;
  DeviceCom->minCycleTimeBase = DSM_TIME_BASE_01;
  DeviceCom->minCycleTimeMultiplier = 0x1EU                                     /* 3 msec */;
  DeviceCom->msequenceODLengthPreoper = DSM_MSEQ_OD_LENGTH_8;
  DeviceCom->msequenceODLengthOper = DSM_MSEQ_OD_LENGTH_2;
  DeviceCom->isISDUSupported = TRUE;
  DeviceCom->revisionID = 0x11U;
  DeviceCom->isBitLengthPDIn = FALSE;
  DeviceCom->lengthPDIn = 3;                                                    /* 3 octets Process Data */
  DeviceCom->isBitLengthPDOut = FALSE;
  DeviceCom->lengthPDOut = 0;
    
  DeviceIdent = DSM_GetDeviceIdent();
  
  DeviceIdent->vendorID_HI = 0x01U;
  DeviceIdent->vendorID_LO = 0xDEU;

  DeviceIdent->deviceID_HI = 0x00U;
  DeviceIdent->deviceID_MID = 0x00U;
  DeviceIdent->deviceID_LO = 0x05U;

  DeviceIdent->functionID_HI = 0;
  DeviceIdent->functionID_LO = 0;
  
  if (DSM_SetDeviceCom() != SERVICE_RESULT_NO_ERROR
      || DSM_SetDeviceIdent() != SERVICE_RESULT_NO_ERROR)
  {                                                                             /* some error in init communication parameters */
    R_WDT_DeviceRestart();
  }
}

void DA_MainCycle(void)
{
  uint8_t byte;
  int16_t val;
  
  DEV_IOL_MainCycle();
  
  if (iol_event_ready && iol_events_number)
  {
    iol_event_ready = FALSE;      
    
    DAL_Event_req(iol_events_number, iol_events);

    iol_events_number = 0;
  }
  
  if (!(max1482x_uart_complete && R_MAIN_measure_counter >= 15)                 /* take a measure every 30 msec by IO-Link */
      && !(!max1482x_uart_complete && R_MAIN_measure_counter >= 50))            /* take a measure every 100 msec in SIO */
  {
    max1482x_uart_complete = FALSE;
    return;
  }

  max1482x_uart_complete = FALSE;

  R_MAIN_measure_counter = FALSE;
  
  DAL_Control_req(FALSE);                                                       /* process data input valid/invalid handling */
  CHK_VOID2(ambtemp_pv = max31865_read_rtd(),
    DAL_Control_req(TRUE); ambtemp_pv = 0/*-247�C*/);
     
  if (!ambtemp_pv)                                                              /* process data input are invalid */ 
  {
    sce574x_set_custom_output(SCE574X_DIGIT_0A_MINUS,
      SCE574X_DIGIT_0A_MINUS, SCE574X_DIGIT_0A_MINUS, SCE574X_DIGIT_0A_MINUS);
    
    max1482x_set_output(FALSE);
    
    do
    {
                                                                                /* Bias off, ADC off */
      CHK_BREAK(max31865_read_registers(MAX31865_REG_00_CONF, &byte, 1));
      SET_BIT(byte, 7, 0);
      CHK_BREAK(max31865_write_registers(MAX31865_REG_00_CONF, &byte, 1));

      if (DSM_GetDeviceCom()->deviceMode != DSM_MODE_SIO)
      {
        DPL_Mode_req(DPL_MODE_COM3);                                            /* restart UART, PCB constraints */
      }
      
      while (!R_MAIN_tick_2msec) { __asm("NOP"); }                              /* waiting for ~3ms randomally */
      R_MAIN_tick_2msec = FALSE;
      while (!R_MAIN_tick_2msec) { __asm("NOP"); }
      R_WDT_Restart();
      
                                                                                /* Bias on, ADC on */
      SET_BIT(byte, 7, 1);
      CHK_BREAK(max31865_write_registers(MAX31865_REG_00_CONF, &byte, 1));

      if (DSM_GetDeviceCom()->deviceMode != DSM_MODE_SIO)
      {
        DPL_Mode_req(DPL_MODE_COM3);                                            /* restart UART, PCB constraints */
      }
      
      while (!R_MAIN_tick_2msec) { __asm("NOP"); }                              /* waiting for ~3ms randomally */
      R_MAIN_tick_2msec = FALSE;
      while (!R_MAIN_tick_2msec) { __asm("NOP"); }
      R_WDT_Restart();

                                                                                /* Fault Status Clear (D1) */
      CHK_BREAK(max31865_read_registers(MAX31865_REG_00_CONF, &byte, 1));
      SET_BIT(byte, 1, 1);
      SET_BIT(byte, 2, 0);
      SET_BIT(byte, 3, 0);
      SET_BIT(byte, 5, 0);
      CHK_BREAK(max31865_write_registers(MAX31865_REG_00_CONF, &byte, 1));

    } while(0);

    if (DSM_GetDeviceCom()->deviceMode != DSM_MODE_SIO)
    {
      DPL_Mode_req(DPL_MODE_COM3);                                              /* restart UART, PCB constraints */
    }
    
    return;
  }
  
  CHK_VOID2(sensor_logic_process(&ambtemp_sp, ambtemp_pv),
    add_iol_event(&IOL_EVENT_6000_ERR_SOFTWARE));
  
  CHK_VOID2(max31865_scale_rtd(ambtemp_pv, precalc, &val, display_scale),
    add_iol_event(&IOL_EVENT_6000_ERR_SOFTWARE));
  
  CHK_VOID2(sce574x_set_quantity_output(val,
      SCE574X_DIGIT_0C_FAHRENHEIT + display_scale),
    add_iol_event(&IOL_EVENT_6000_ERR_SOFTWARE));
  
  max1482x_set_output(ambtemp_sp.flag);

  if (DSM_GetDeviceCom()->deviceMode != DSM_MODE_SIO)
  {
    DPL_Mode_req(DPL_MODE_COM3);                                                /* restart UART, PCB constraints */
  }
}

void DSM_DeviceMode(DSM_ModeT mode)
{
  iol_event_ready = FALSE;
  
  switch (mode)
  {
    case DSM_MODE_IDLE:
      DA_InitDSM();
      DSM_SetDeviceMode(DSM_MODE_SIO);
      break;

    case DSM_MODE_SIO:
      break;
      
    case DSM_MODE_ESTABCOM:
      break;
      
    case DSM_MODE_COM1:
    case DSM_MODE_COM2:
    case DSM_MODE_COM3:
      break;

    case DSM_MODE_IDENT_STARTUP:
      break;

    case DSM_MODE_IDENT_CHANGE:

      // DeviceCom = DSM_GetDeviceCom();  
      // DeviceIdent = DSM_GetDeviceIdent();

      // This mode indication means, that master has not accepted
      // init device parameters, set in DA_InitDSM().
      // Master could change DeviceCom->revisionID and DeviceIdent->deviceID_XX.
      // They can be checked here and set to the compatible ones.
      // In this case delete call DA_InitDSM() at the end of current case.
      // See: IO-Link spec v1.1.1 Oct 2011, 9.3.3.2, Table 93, p.146,
      //      SM_IdentStartup_4 and SM_IdentCheck_5 states of SM
      // 
      // if change of its configuration is not supported by device,
      // it can be just reinitialised once more by DA_InitDSM(),
      // as made by default in this example at the end of current case
      
      // if (DSM_SetDeviceCom() != SERVICE_RESULT_NO_ERROR)
      //{
      // some error in init communication parameters, see dev_sm.c
      //}
      
      // if (DSM_SetDeviceIdent() != SERVICE_RESULT_NO_ERROR)
      //{
      // some error in init communication parameters, see dev_sm.c
      //}  
      
      DA_InitDSM();
      break;

    case DSM_MODE_PREOPERATE:
      iol_event_ready = TRUE;
      break;
      
    case DSM_MODE_OPERATE:
      iol_event_ready = TRUE;
      break;

    default:
      break;
  }
}

#ifdef DPL_BACK_COMPATIBLE_RevID_10

void DSM_SwitchToRevID10_ind(void)
{
}

#endif

void DAL_Control_ind(BooleanT controlCode)
{
}

void DAL_NewOutput_ind(void)
{
  // Process data output handling, for example, copy to buffer
  // by DAL_GetOutput_req()
  // ...
}

void DAL_PDCycle_ind(void)
{
  uint8_t *pdIn = DAL_SetInput_req();
  SET_BIGEND_UINT16(pdIn, ambtemp_pv);
  pdIn[2] = ambtemp_sp.flag & 1U;
}

void DAL_Read_ind(UInteger16T index, UInteger8T subindex, UInteger8T * data)
{
  static char const VENDOR_NAME[] = "Maxim Integrated";
  static char const VENDOR_TEXT[] = "The world leader in analog integration";
  
  static char const PRODUCT_NAME[] = "Mojave (MAXREFDES42#)";
  static char const PRODUCT_ID[] = "MAXREFDES42";
  static char const PRODUCT_TEXT[] = "IO-Link RTD Temp Sensor";
  
  float val;
  
  switch(index)
  {
    case 0x10:                                                                  /* Vendor Name */
      strcpy((char *)data, VENDOR_NAME);
      DAL_Read_rsp(strlen(VENDOR_NAME));
      break;

    case 0x11:                                                                  /* Vendor Text */
      strcpy((char *)data, VENDOR_TEXT);
      DAL_Read_rsp(strlen(VENDOR_TEXT));
      break;

    case 0x12:                                                                  /* Product Name */
      strcpy((char *)data, PRODUCT_NAME);
      DAL_Read_rsp(strlen(PRODUCT_NAME));
      break;

    case 0x13:                                                                  /* Product ID */
      strcpy((char *)data, PRODUCT_ID);
      DAL_Read_rsp(strlen(PRODUCT_ID));
      break;

    case 0x14:                                                                  /* Product Text */
      strcpy((char *)data, PRODUCT_TEXT);
      DAL_Read_rsp(strlen(PRODUCT_TEXT));
      break;

    case 0x17:                                                                  /* Firmware Revision */
      strcpy((char *)data, VERSION_STR);
      DAL_Read_rsp(strlen(VERSION_STR));
      break;
      
    case IOL_INDEX_0100_AMBTEMP_SP_LEVEL:
      SET_BIGEND_UINT16(data, ambtemp_sp.level);
      DAL_Read_rsp(2);
      break;
      
    case IOL_INDEX_0101_AMBTEMP_SP_HYSTERESIS:
      SET_BIGEND_UINT16(data, ambtemp_sp.hyst);
      DAL_Read_rsp(2);
      break;
      
    case IOL_INDEX_0102_AMBTEMP_DISPLAY_SCALE:
      *data = display_scale;
      DAL_Read_rsp(1);
      break;

    case IOL_INDEX_0103_NWIRE_TYPE_CONNECTION:
      *data = nwire_type;
      DAL_Read_rsp(1);
      break;
      
    case IOL_INDEX_0104_MAX31865_CONF:
      if(IS_ERR(max31865_read_registers(MAX31865_REG_00_CONF, data, 1)))
      {
        DPL_Mode_req(DPL_MODE_COM3);                                            /* restart UART, PCB constraints */
        DAL_Read_err(0x80, IOL_ISDU_ERR_8000_APP_DEV & 0xFFU);
        break;
      }

      DPL_Mode_req(DPL_MODE_COM3);                                              /* restart UART, PCB constraints */
      DAL_Read_rsp(1);
      break;

    case IOL_INDEX_0105_MAX31865_RTD:
      if(IS_ERR(max31865_read_registers(MAX31865_REG_01_RTD_MSB, data, 2)))
      {
        DPL_Mode_req(DPL_MODE_COM3);                                            /* restart UART, PCB constraints */
        DAL_Read_err(0x80, IOL_ISDU_ERR_8000_APP_DEV & 0xFFU);
        break;
      }

      DPL_Mode_req(DPL_MODE_COM3);                                              /* restart UART, PCB constraints */
      DAL_Read_rsp(2);
      break;

    case IOL_INDEX_0106_MAX31865_RTDHIGH:
      if(IS_ERR(max31865_read_registers(MAX31865_REG_03_RTDHIGH_MSB, data, 2)))
      {
        DPL_Mode_req(DPL_MODE_COM3);                                            /* restart UART, PCB constraints */
        DAL_Read_err(0x80, IOL_ISDU_ERR_8000_APP_DEV & 0xFFU);
        break;
      }

      DPL_Mode_req(DPL_MODE_COM3);                                              /* restart UART, PCB constraints */
      DAL_Read_rsp(2);
      break;

    case IOL_INDEX_0107_MAX31865_RTDLOW:
      if(IS_ERR(max31865_read_registers(MAX31865_REG_05_RTDLOW_MSB, data, 2)))
      {
        DPL_Mode_req(DPL_MODE_COM3);                                            /* restart UART, PCB constraints */
        DAL_Read_err(0x80, IOL_ISDU_ERR_8000_APP_DEV & 0xFFU);
        break;
      }

      DPL_Mode_req(DPL_MODE_COM3);                                              /* restart UART, PCB constraints */
      DAL_Read_rsp(2);
      break;

    case IOL_INDEX_0108_MAX31865_STATUS:
      if(IS_ERR(max31865_read_registers(MAX31865_REG_07_STATUS, data, 1)))
      {
        DPL_Mode_req(DPL_MODE_COM3);                                            /* restart UART, PCB constraints */
        DAL_Read_err(0x80, IOL_ISDU_ERR_8000_APP_DEV & 0xFFU);
        break;
      }

      DPL_Mode_req(DPL_MODE_COM3);                                              /* restart UART, PCB constraints */
      DAL_Read_rsp(1);
      break;

    case IOL_INDEX_0109_REFERENCE_RESISTANCE:
      data[0] = (uint8_t)((rref >> 16) & 0xFFU);
      data[1] = (uint8_t)((rref >> 8) & 0xFFU);
      data[2] = (uint8_t)(rref & 0xFFU);
      DAL_Read_rsp(3);
      break;
      
    case IOL_INDEX_010A_NOMINAL_RESISTANCE:
      data[0] = (uint8_t)((r0 >> 16) & 0xFFU);
      data[1] = (uint8_t)((r0 >> 8) & 0xFFU);
      data[2] = (uint8_t)(r0 & 0xFFU);
      DAL_Read_rsp(3);
      break;

    case IOL_INDEX_010B_AMBTEMP_F:
    case IOL_INDEX_010C_AMBTEMP_C:
      if(IS_ERR(max31865_calc(ambtemp_pv, precalc, &val,
          index == IOL_INDEX_010B_AMBTEMP_F ?
            MAX31865_UNIT_00_FAHRENHEIT : MAX31865_UNIT_01_CELSIUS)))
      {
        DAL_Read_err(0x80, IOL_ISDU_ERR_8000_APP_DEV & 0xFFU);
        break;
      }

      SET_BIGEND_FLOAT(data, val);
      DAL_Read_rsp(4);
      break;

    case IOL_INDEX_010D_AMBTEMP_SP_LEVEL_F:
    case IOL_INDEX_010E_AMBTEMP_SP_LEVEL_C:
      if(IS_ERR(max31865_calc(ambtemp_sp.level, precalc, &val,
          index == IOL_INDEX_010D_AMBTEMP_SP_LEVEL_F ?
            MAX31865_UNIT_00_FAHRENHEIT : MAX31865_UNIT_01_CELSIUS)))
      {
        DAL_Read_err(0x80, IOL_ISDU_ERR_8000_APP_DEV & 0xFFU);
        break;
      }

      SET_BIGEND_FLOAT(data, val);
      DAL_Read_rsp(4);
      break;

    case IOL_INDEX_010F_AMBTEMP_SP_HYSTERESIS_F:
    case IOL_INDEX_0110_AMBTEMP_SP_HYSTERESIS_C:
      if(IS_ERR(max31865_calc(
          ambtemp_sp.hyst + (uint16_t)(32768. * r0 / rref + 0.5), precalc, &val,
          index == IOL_INDEX_010F_AMBTEMP_SP_HYSTERESIS_F ?
            MAX31865_UNIT_00_FAHRENHEIT : MAX31865_UNIT_01_CELSIUS)))
      {
        DAL_Read_err(0x80, IOL_ISDU_ERR_8000_APP_DEV & 0xFFU);
        break;
      }

      val = index == IOL_INDEX_010F_AMBTEMP_SP_HYSTERESIS_F ? val - 32 : val;
      SET_BIGEND_FLOAT(data, val);
      DAL_Read_rsp(4);
      break;
      
    default:
      DAL_Read_err(0x80, IOL_ISDU_ERR_8011_IDX_NOTAVAIL & 0xFFU);
      break;
  }
}

void DAL_Write_ind(UInteger16T index, UInteger8T subindex, 
                   UInteger8T dataLength, UInteger8T *data)
{
  uint8_t buf[4] = { 0x00, 0x00, 0x00, 0x00 }, byte;
  
  switch(index)
  {
    case 0x02:                                                                  /* System Command */
      if (*data == 0x80U)                                                       /* Device reset */
      {
        R_WDT_DeviceRestart();
        break;
      }

      if (*data == 0xA0U)                                                       /* Tech-in */
      {
        CHK_BREAK2(sensor_logic_teachin(
            &ambtemp_sp, ambtemp_pv, ambtemp_sp.hyst),
          DAL_Write_err(0x80, IOL_ISDU_ERR_8041_PAR_SETINCONSIST & 0xFFU));

        SET_BIGEND_UINT16(buf, ambtemp_pv);
        CHK_VOID2(eeprom_write(EEPROM_A0_AMBTEMP_SP_LEVEL, buf),
          add_iol_event(&IOL_EVENT_5000_ERR_HARDWARE));

        DAL_Write_rsp();
        break;
      }
        
      DAL_Write_err(0x80, IOL_ISDU_ERR_8035_FUNC_NOTAVAIL & 0xFFU);
      break;
     
    case IOL_INDEX_0100_AMBTEMP_SP_LEVEL:
      CHK_BREAK2(sensor_logic_teachin(
          &ambtemp_sp, GET_BIGEND_UINT16(data), ambtemp_sp.hyst),
        DAL_Write_err(0x80, IOL_ISDU_ERR_8041_PAR_SETINCONSIST & 0xFFU));

      SET_BIGEND_UINT16(buf, ambtemp_sp.level);
      CHK_VOID2(eeprom_write(EEPROM_A0_AMBTEMP_SP_LEVEL, buf),
        add_iol_event(&IOL_EVENT_5000_ERR_HARDWARE));

      DAL_Write_rsp();
      break;
      
    case IOL_INDEX_0101_AMBTEMP_SP_HYSTERESIS:
      CHK_BREAK2(sensor_logic_teachin(
          &ambtemp_sp, ambtemp_sp.level, GET_BIGEND_UINT16(data)),
        DAL_Write_err(0x80, IOL_ISDU_ERR_8041_PAR_SETINCONSIST & 0xFFU));

      SET_BIGEND_UINT16(buf, ambtemp_sp.hyst);
      CHK_VOID2(eeprom_write(EEPROM_A1_AMBTEMP_SP_HYSTERESIS, buf),
        add_iol_event(&IOL_EVENT_5000_ERR_HARDWARE));

      DAL_Write_rsp();
      break;
      
    case IOL_INDEX_0102_AMBTEMP_DISPLAY_SCALE:      
      if (*data != MAX31865_UNIT_00_FAHRENHEIT
          && *data != MAX31865_UNIT_01_CELSIUS)
      {
        DAL_Write_err(0x80, IOL_ISDU_ERR_8030_PAR_VALOUTOFRNG & 0xFFU);
        break;
      }
      
      display_scale = *data;      
      
      CHK_VOID2(eeprom_write(EEPROM_A2_AMBTEMP_DISPLAY_SCALE, &display_scale),
        add_iol_event(&IOL_EVENT_5000_ERR_HARDWARE));
      
      DAL_Write_rsp();
      break;

    case IOL_INDEX_0103_NWIRE_TYPE_CONNECTION:
      if(IS_ERR(max31865_read_registers(MAX31865_REG_00_CONF, &byte, 1)))
      {
        DPL_Mode_req(DPL_MODE_COM3);                                            /* restart UART, PCB constraints */
        DAL_Write_err(0x80, IOL_ISDU_ERR_8000_APP_DEV & 0xFFU);
        break;
      }

      if (*data == 2 || *data == 4)
      {
        SET_BIT(byte, 4, FALSE);
      }
      else if (*data == 3)
      {
        SET_BIT(byte, 4, TRUE);
      }
      else
      {
        DPL_Mode_req(DPL_MODE_COM3);                                            /* restart UART, PCB constraints */
        DAL_Write_err(0x80, IOL_ISDU_ERR_8030_PAR_VALOUTOFRNG & 0xFFU);
        break;
      }
      
      nwire_type = *data;      
      *data = byte;

      CHK_VOID2(eeprom_write(EEPROM_A3_NWIRE_TYPE_CONNECTION, &nwire_type),
        add_iol_event(&IOL_EVENT_5000_ERR_HARDWARE));

    case IOL_INDEX_0104_MAX31865_CONF:
      if(IS_ERR(max31865_write_registers(MAX31865_REG_00_CONF, data, 1)))
      {
        DPL_Mode_req(DPL_MODE_COM3);                                            /* restart UART, PCB constraints */
        DAL_Write_err(0x80, IOL_ISDU_ERR_8000_APP_DEV & 0xFFU);
        break;
      }

      if (GET_BIT(*data, 4))
      {
        nwire_type = 3;
      }
      else if (nwire_type == 3)
      {
        nwire_type = 2;
      }

      DPL_Mode_req(DPL_MODE_COM3);                                              /* restart UART, PCB constraints */
      DAL_Write_rsp();
      break;

    case IOL_INDEX_0106_MAX31865_RTDHIGH:
      if (dataLength != 2)
      {
        DAL_Write_err(0x80, IOL_ISDU_ERR_8030_PAR_VALOUTOFRNG & 0xFFU);
        break;
      }
      
      if(IS_ERR(max31865_write_registers(MAX31865_REG_03_RTDHIGH_MSB, data, 2)))
      {
        DPL_Mode_req(DPL_MODE_COM3);                                            /* restart UART, PCB constraints */
        DAL_Write_err(0x80, IOL_ISDU_ERR_8000_APP_DEV & 0xFFU);
        break;
      }

      DPL_Mode_req(DPL_MODE_COM3);                                              /* restart UART, PCB constraints */
      DAL_Write_rsp();
      break;

    case IOL_INDEX_0107_MAX31865_RTDLOW:
      if (dataLength != 2)
      {
        DAL_Write_err(0x80, IOL_ISDU_ERR_8030_PAR_VALOUTOFRNG & 0xFFU);
        break;
      }

      if(IS_ERR(max31865_write_registers(MAX31865_REG_05_RTDLOW_MSB, data, 2)))
      {
        DPL_Mode_req(DPL_MODE_COM3);                                            /* restart UART, PCB constraints */
        DAL_Write_err(0x80, IOL_ISDU_ERR_8000_APP_DEV & 0xFFU);
        break;
      }

      DPL_Mode_req(DPL_MODE_COM3);                                              /* restart UART, PCB constraints */
      DAL_Write_rsp();
      break;

    case IOL_INDEX_0109_REFERENCE_RESISTANCE:
      rref = ((uint32_t)data[0] << 16)
        + ((uint32_t)data[1] << 8) + (uint32_t)data[2];

      precalc = max31865_precalc(rref, r0);
      
      SET_BIGEND_ULONG32(buf, rref);
      CHK_VOID2(eeprom_write(EEPROM_A9_REFERENCE_RESISTANCE, buf + 1),
        add_iol_event(&IOL_EVENT_5000_ERR_HARDWARE));
      
      DAL_Write_rsp();
      break;
      
    case IOL_INDEX_010A_NOMINAL_RESISTANCE:
      r0 = ((uint32_t)data[0] << 16)
        + ((uint32_t)data[1] << 8) + (uint32_t)data[2];

      precalc = max31865_precalc(rref, r0);
      
      SET_BIGEND_ULONG32(buf, r0);
      CHK_VOID2(eeprom_write(EEPROM_AA_NOMINAL_RESISTANCE, buf + 1),
        add_iol_event(&IOL_EVENT_5000_ERR_HARDWARE));
      
      DAL_Write_rsp();
      break;
    
    default:
      DAL_Write_err(0x80, IOL_ISDU_ERR_8011_IDX_NOTAVAIL & 0xFFU);
      break;
  }
}

void DAL_Abort_ind(void)
{
}

void DAL_Event_cnf(void)
{
  iol_event_ready = TRUE;
}

void DDL_MEH_MHInfo_Commloss(void)
{
}