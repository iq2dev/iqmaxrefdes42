/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only 
* intended for use with Renesas products. No other uses are authorized. This 
* software is owned by Renesas Electronics Corporation and is protected under 
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING 
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT 
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE 
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software 
* and to discontinue the availability of this software.  By using this software, 
* you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2010, 2013 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_cg_userdefine.h
* Version      : Applilet3 for RL78/G1A V2.01.00.01 [26 Jul 2013]
* Device(s)    : R5F10E8E
* Tool-Chain   : IAR Systems iccrl78
* Description  : This file includes user definition.
* Creation Date: 07.05.2014
***********************************************************************************************************************/

#ifndef _USER_DEF_H
#define _USER_DEF_H

/***********************************************************************************************************************
User definitions
***********************************************************************************************************************/

/* Start user code for function. Do not edit comment generated here */
#include "c_define.h"

#define MAX44008_CHIP_IIC_ADDR  MAX44008_IIC_ADDR_82_A0_GND

enum
{
    HW_ERR_8000_LOW_VOLTAGE           = ERR_BASE + 0x00,
    HW_ERR_8001_ILLEGAL_MEMORY_ACCESS = ERR_BASE + 0x01,
    HW_ERR_8002_RAM_PARITY            = ERR_BASE + 0x02,
    HW_ERR_8003_ILLEGAL_INSTRUCTION   = ERR_BASE + 0x03,
    HW_ERR_8004_MAX1482X_SELFTEST     = ERR_BASE + 0x04,
    HW_ERR_8005_MAX31865_SELFTEST     = ERR_BASE + 0x05,
    HW_ERR_8006_SCE574X_SELFTEST      = ERR_BASE + 0x06,
    HW_ERR_8007_WATCHDOG_TIMER        = ERR_BASE + 0x07,
};

extern volatile uint8_t R_MAIN_tick_2msec;

extern int16_t R_MAIN_hw_init_err;
extern uint8_t R_MAIN_measure_counter;
/* End user code. Do not edit comment generated here */
#endif
