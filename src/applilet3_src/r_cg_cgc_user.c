/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only 
* intended for use with Renesas products. No other uses are authorized. This 
* software is owned by Renesas Electronics Corporation and is protected under 
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING 
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT 
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE 
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software 
* and to discontinue the availability of this software.  By using this software, 
* you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2010, 2013 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_cg_cgc_user.c
* Version      : Applilet3 for RL78/G1A V2.01.00.01 [26 Jul 2013]
* Device(s)    : R5F10E8E
* Tool-Chain   : IAR Systems iccrl78
* Description  : This file implements device driver for CGC module.
* Creation Date: 07.05.2014
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_cgc.h"
/* Start user code for include. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
#include "r_cg_userdefine.h"

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
/* Start user code for global. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
* Function Name: R_CGC_Get_ResetSource
* Description  : This function process of Reset.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_CGC_Get_ResetSource(void)
{
    uint8_t reset_flag = RESF;
    /* Start user code. Do not edit comment generated here */
    R_MAIN_hw_init_err = ERR_NONE;
    
    if (reset_flag & 0x01U)
    {
        R_MAIN_hw_init_err = HW_ERR_8000_LOW_VOLTAGE;                           /* Internal reset request by low-voltage detector */
    }
    else if (reset_flag & 0x02U)
    {
        R_MAIN_hw_init_err = HW_ERR_8001_ILLEGAL_MEMORY_ACCESS;                 /* Internal reset request by illegal-memory access */
    }
    else if (reset_flag & 0x04U)
    {
        R_MAIN_hw_init_err = HW_ERR_8002_RAM_PARITY;                            /* Internal reset request by RAM parity */
    }
    else if (reset_flag & 0x10U)
    {
        R_MAIN_hw_init_err = HW_ERR_8007_WATCHDOG_TIMER;                        /* Internal reset request by watchdog timer */
    }
    else if (reset_flag & 0x80U)
    {
        R_MAIN_hw_init_err = HW_ERR_8003_ILLEGAL_INSTRUCTION;                   /* Internal reset request by execution of illegal instruction */
    }
    /* End user code. Do not edit comment generated here */
}

/* Start user code for adding. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
