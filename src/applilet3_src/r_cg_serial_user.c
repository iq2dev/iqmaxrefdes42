/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only 
* intended for use with Renesas products. No other uses are authorized. This 
* software is owned by Renesas Electronics Corporation and is protected under 
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING 
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT 
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE 
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software 
* and to discontinue the availability of this software.  By using this software, 
* you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2010, 2013 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_cg_serial_user.c
* Version      : Applilet3 for RL78/G1A V2.01.00.01 [26 Jul 2013]
* Device(s)    : R5F10E8E
* Tool-Chain   : IAR Systems iccrl78
* Description  : This file implements device driver for Serial module.
* Creation Date: 07.05.2014
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_serial.h"
/* Start user code for include. Do not edit comment generated here */
#include "c_max1482x.h"
#include "c_max31865.h"
/* End user code. Do not edit comment generated here */
#include "r_cg_userdefine.h"

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
extern uint8_t * gp_csi00_rx_address;         /* csi00 receive buffer address */
extern uint16_t  g_csi00_rx_length;           /* csi00 receive data length */
extern uint16_t  g_csi00_rx_count;            /* csi00 receive data count */
extern uint8_t * gp_csi00_tx_address;         /* csi00 send buffer address */
extern uint16_t  g_csi00_send_length;         /* csi00 send data length */
extern uint16_t  g_csi00_tx_count;            /* csi00 send data count */
extern uint8_t * gp_csi11_rx_address;         /* csi11 receive buffer address */
extern uint16_t  g_csi11_rx_length;           /* csi11 receive data length */
extern uint16_t  g_csi11_rx_count;            /* csi11 receive data count */
extern uint8_t * gp_csi11_tx_address;         /* csi11 send buffer address */
extern uint16_t  g_csi11_send_length;         /* csi11 send data length */
extern uint16_t  g_csi11_tx_count;            /* csi11 send data count */
extern uint8_t * gp_uart1_tx_address;         /* uart1 send buffer address */
extern uint16_t  g_uart1_tx_count;            /* uart1 send data number */
extern uint8_t * gp_uart1_rx_address;         /* uart1 receive buffer address */
extern uint16_t  g_uart1_rx_count;            /* uart1 receive data number */
extern uint16_t  g_uart1_rx_length;           /* uart1 receive data length */
/* Start user code for global. Do not edit comment generated here */
static void (*csi00_recv_complete_callback)(int16_t err);
static void (*csi11_send_complete_callback)(int16_t err);
static void (*csi11_recv_complete_callback)(int16_t err);
static void r_uart1_interrupt_receive(void);
static void (*uart1_byte_recv_callback)(uint8_t byte, int16_t err);
static void (*uart1_complete_callback)(void);
static uint8_t uart1_rx_buf = 0;
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
* Function Name: r_csi00_interrupt
* Description  : This function is INTCSI00 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
#pragma vector = INTCSI00_vect
__interrupt static void r_csi00_interrupt(void)
{
    uint8_t err_type;
    uint8_t sio_dummy;
    
    err_type = (uint8_t)(SSR00 & _0001_SAU_OVERRUN_ERROR);
    SIR00 = (uint16_t)err_type;

    if (1U == err_type)
    {
        r_csi00_callback_error(err_type);    /* overrun error occurs */
    }
    else
    {
        if (g_csi00_tx_count > 0U)
        {
            if (0U != gp_csi00_rx_address)
            {
                *gp_csi00_rx_address = SIO00;
                gp_csi00_rx_address++;
            }
            else
            {
                sio_dummy = SIO00;
            }

            if (0U != gp_csi00_tx_address)
            {
                SIO00 = *gp_csi00_tx_address;
                gp_csi00_tx_address++;
            }
            else
            {
                SIO00 = 0xFFU;
            }
         
            g_csi00_tx_count--;
        }
        else 
        {
            if (0U == g_csi00_tx_count)
            {
                if (0U != gp_csi00_rx_address)
                {
                    *gp_csi00_rx_address = SIO00;
                }
                else
                {
                    sio_dummy = SIO00;
                }                
            }

            r_csi00_callback_receiveend();    /* complete receive */
        }
    }
}

/***********************************************************************************************************************
* Function Name: r_csi00_callback_receiveend
* Description  : This function is a callback function when CSI00 finishes reception.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
static void r_csi00_callback_receiveend(void)
{
    /* Start user code. Do not edit comment generated here */
    CALL1(csi00_recv_complete_callback, ERR_NONE);
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: r_csi00_callback_error
* Description  : This function is a callback function when CSI00 reception error occurs.
* Arguments    : err_type -
*                    error type value
* Return Value : None
***********************************************************************************************************************/
static void r_csi00_callback_error(uint8_t err_type)
{
    /* Start user code. Do not edit comment generated here */
    CALL1(csi00_recv_complete_callback, MAX1482X_ERR_8001_SPI_OVERRUN);
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: r_csi11_interrupt
* Description  : This function is INTCSI11 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
#pragma vector = INTCSI11_vect
__interrupt static void r_csi11_interrupt(void)
{
    uint8_t err_type;
    uint8_t sio_dummy;

    if (P2_bit.no0 && P6_bit.no1)
    {
      r_uart1_interrupt_receive();
      return;
    }

    if (!P2_bit.no0)
    {
      err_type = (uint8_t)(SSR03 & _0001_SAU_OVERRUN_ERROR);
      SIR03 = (uint16_t)err_type;

      if (err_type != 1U)
      {
          if (g_csi11_tx_count > 0U)
          {
              SIO11 = *gp_csi11_tx_address;
              gp_csi11_tx_address++;
              g_csi11_tx_count--;
          }
          else
          {
              r_csi11_callback_sendend();    /* complete send */
          }
      }
      
      return;
    }

    err_type = (uint8_t)(SSR03 & _0001_SAU_OVERRUN_ERROR);
    SIR03 = (uint16_t)err_type;

    if (1U == err_type)
    {
        r_csi11_callback_error(err_type);    /* overrun error occurs */
    }
    else
    {
        if (g_csi11_tx_count > 0U)
        {
            if (0U != gp_csi11_rx_address)
            {
                *gp_csi11_rx_address = SIO11;
                gp_csi11_rx_address++;
            }
            else
            {
                sio_dummy = SIO11;
            }

            if (0U != gp_csi11_tx_address)
            {
                SIO11 = *gp_csi11_tx_address;
                gp_csi11_tx_address++;
            }
            else
            {
                SIO11 = 0xFFU;
            }
         
            g_csi11_tx_count--;
        }
        else 
        {
            if (0U == g_csi11_tx_count)
            {
                if (0U != gp_csi11_rx_address)
                {
                    *gp_csi11_rx_address = SIO11;
                }
                else
                {
                    sio_dummy = SIO11;
                }                
            }

            r_csi11_callback_receiveend();    /* complete receive */
        }
    }
}

/***********************************************************************************************************************
* Function Name: r_csi11_callback_sendend
* Description  : This function is a callback function when CSI11 finishes transmission.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
static void r_csi11_callback_sendend(void)
{
    /* Start user code. Do not edit comment generated here */
    CALL1(csi11_send_complete_callback, ERR_NONE);    
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: r_csi11_callback_receiveend
* Description  : This function is a callback function when CSI11 finishes reception.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
static void r_csi11_callback_receiveend(void)
{
    /* Start user code. Do not edit comment generated here */
    CALL1(csi11_recv_complete_callback, ERR_NONE);
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: r_csi11_callback_error
* Description  : This function is a callback function when CSI11 reception error occurs.
* Arguments    : err_type -
*                    error type value
* Return Value : None
***********************************************************************************************************************/
static void r_csi11_callback_error(uint8_t err_type)
{
    /* Start user code. Do not edit comment generated here */
    CALL1(csi11_recv_complete_callback, MAX31865_ERR_8002_SPI_OVERRUN);
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: r_uart1_interrupt_receive
* Description  : This function is INTSR1 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
/*#pragma vector = INTSR1_vect*/
/*__interrupt*/ static void r_uart1_interrupt_receive(void)
{
    uint8_t rx_data;

    rx_data = RXD1;

    if (g_uart1_rx_length > g_uart1_rx_count)
    {
        *gp_uart1_rx_address = rx_data;
        gp_uart1_rx_address++;
        g_uart1_rx_count++;

        if (g_uart1_rx_length == g_uart1_rx_count)
        {
            r_uart1_callback_receiveend();
        }
    }
}

/***********************************************************************************************************************
* Function Name: r_uart1_interrupt_error
* Description  : This function is INTSRE1 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
#pragma vector = INTSRE1_vect
__interrupt static void r_uart1_interrupt_error(void)
{
    uint8_t err_type;

    *gp_uart1_rx_address = RXD1;
    err_type = (uint8_t)(SSR03 & 0x0007U);
    r_uart1_callback_error(err_type);
}

/***********************************************************************************************************************
* Function Name: r_uart1_interrupt_send
* Description  : This function is INTST1 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
#pragma vector = INTST1_vect
__interrupt static void r_uart1_interrupt_send(void)
{
    if (g_uart1_tx_count > 0U)
    {
        TXD1 = *gp_uart1_tx_address;
        gp_uart1_tx_address++;
        g_uart1_tx_count--;
    }
    else
    {
        r_uart1_callback_sendend();
    }
}

/***********************************************************************************************************************
* Function Name: r_uart1_callback_receiveend
* Description  : This function is a callback function when UART1 finishes reception.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
static void r_uart1_callback_receiveend(void)
{
    /* Start user code. Do not edit comment generated here */
    CALL2(uart1_byte_recv_callback, uart1_rx_buf, ERR_NONE);
    R_UART1_Receive(&uart1_rx_buf, 1);
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: r_uart1_callback_sendend
* Description  : This function is a callback function when UART1 finishes transmission.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
static void r_uart1_callback_sendend(void)
{
    /* Start user code. Do not edit comment generated here */
    CALL0(uart1_complete_callback);
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: r_uart1_callback_error
* Description  : This function is a callback function when UART1 reception error occurs.
* Arguments    : err_type -
*                    error type value
* Return Value : None
***********************************************************************************************************************/
static void r_uart1_callback_error(uint8_t err_type)
{
    /* Start user code. Do not edit comment generated here */
    SIR03L |= _0004_SAU_SIRMN_FECTMN | _0002_SAU_SIRMN_PECTMN |
              _0001_SAU_SIRMN_OVCTMN;
    
    if (err_type & _0001_SAU_SIRMN_OVCTMN)
    {
        CALL2(uart1_byte_recv_callback,
            uart1_rx_buf, MAX1482X_ERR_8004_UART_OVERRUN);
    }
    else if (err_type & _0002_SAU_SIRMN_PECTMN)
    {
        CALL2(uart1_byte_recv_callback,
            uart1_rx_buf, MAX1482X_ERR_8003_UART_FRAMING);
    }
    else if (err_type & _0004_SAU_SIRMN_FECTMN)
    {
        CALL2(uart1_byte_recv_callback,
            uart1_rx_buf, MAX1482X_ERR_8002_UART_PARITY);
    }
    /* End user code. Do not edit comment generated here */
}

/* Start user code for adding. Do not edit comment generated here */
void max1482x_spi_start(void)
{
    R_CSI00_Start();
}

void max1482x_spi_stop(void)
{
    R_CSI00_Stop();
}

void _max1482x_spi_byte_cycle_async(uint8_t tx,
                                    uint8_t *rx,
                                    void (*on_complete)(int16_t err))
{
    csi00_recv_complete_callback = on_complete;
    R_CSI00_Send_Receive(&tx, 1, rx);
}

void _max1482x_uart_start(void (*on_byte_recv)(uint8_t byte,
                                               int16_t err))
{
    uart1_byte_recv_callback = on_byte_recv;
    R_UART1_Receive(&uart1_rx_buf, 1);
    R_UART1_Start();
}

void max1482x_uart_stop()
{
    R_UART1_Stop();
}

void _max1482x_uart_send_async(uint8_t const *data,
                               uint8_t len,
                               void (*on_complete)(void))
{
    uart1_complete_callback = on_complete;
    CHK_VOID2(R_UART1_Send((uint8_t *)data, len), on_complete());
}

void _max31865_spi_start(void)
{
    R_CSI11_Start();
}

void _max31865_spi_stop(void)
{
    R_CSI11_Stop();
}

void _max31865_spi_transfer_async(uint8_t const *tx,
                                  uint8_t *rx,
                                  uint8_t num,
                                  void (*on_complete)(int16_t err))
{
    csi11_recv_complete_callback = on_complete;
    R_CSI11_Send_Receive((uint8_t *)tx, num, rx);
}

void _sce574x_spi_start(void)
{
    R_CSI11_2_Start();
}

void _sce574x_spi_stop(void)
{
    R_CSI11_2_Stop();
}

void _sce574x_spi_transfer_async(uint8_t byte,
                                 void (*on_complete)(int16_t err))
{
    csi11_send_complete_callback = on_complete;
    R_CSI11_2_Send(&byte, 1);
}
/* End user code. Do not edit comment generated here */
