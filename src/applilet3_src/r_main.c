/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only 
* intended for use with Renesas products. No other uses are authorized. This 
* software is owned by Renesas Electronics Corporation and is protected under 
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING 
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT 
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE 
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software 
* and to discontinue the availability of this software.  By using this software, 
* you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2010, 2013 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_main.c
* Version      : Applilet3 for RL78/G1A V2.01.00.01 [26 Jul 2013]
* Device(s)    : R5F10E8E
* Tool-Chain   : IAR Systems iccrl78
* Description  : This file implements main function.
* Creation Date: 07.05.2014
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_cgc.h"
#include "r_cg_port.h"
#include "r_cg_intc.h"
#include "r_cg_serial.h"
#include "r_cg_timer.h"
#include "r_cg_wdt.h"
/* Start user code for include. Do not edit comment generated here */
#include "c_assert.h"

#include "c_max1482x.h"
#include "c_max31865.h"
#include "c_sce574x.h"
#include "c_eeprom.h"

#include "iqstack_dev-20140113-v1.1.2.6_precompiled_settings.h"
#include "iqstack_dev.h"

#include "sensor_logic.h"
/* End user code. Do not edit comment generated here */
#include "r_cg_userdefine.h"

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
/* Start user code for global. Do not edit comment generated here */
__no_init volatile uint8_t R_MAIN_tick_2msec                                    @ 0xFFEDF;

__near __no_init int16_t R_MAIN_hw_init_err;

__near __no_init uint8_t R_MAIN_measure_counter;

static uint8_t max14821_irq_events_timer, TDSIO_timer, TFBD_timer;
/* End user code. Do not edit comment generated here */

/* Set option bytes */
#pragma location = "OPTBYTE"
__root const uint8_t opbyte0 = 0x78U;
#pragma location = "OPTBYTE"
__root const uint8_t opbyte1 = 0x57U;
#pragma location = "OPTBYTE"
__root const uint8_t opbyte2 = 0xE8U;
#pragma location = "OPTBYTE"
__root const uint8_t opbyte3 = 0x04U;

/* Set security ID */
#pragma location = "SECUID"
__root const uint8_t secuid[10] = 
    {0x00U, 0x00U, 0x00U, 0x00U, 0x00U, 0x00U, 0x00U, 0x00U, 0x00U, 0x00U};

void R_MAIN_UserInit(void);

/***********************************************************************************************************************
* Function Name: main
* Description  : This function implements main function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void main(void)
{
    R_MAIN_UserInit();
    /* Start user code. Do not edit comment generated here */
#ifdef UNITTESTS
    
    sce574x_test_suite();
    R_WDT_Restart();
    
    sensor_logic_test_suite();    
    R_WDT_Restart();
    
#endif
                                                                                /* initialize variables */
    R_MAIN_measure_counter = FALSE;
    
    if (IS_ERR(eeprom_init()))
    {
      R_WDT_DeviceRestart();
    }
    
    DA_Init();                                                                  /* iqStack's initialization */
    
    while (1U)                                                                  /* firmware forever loop */
    {
        R_WDT_Restart();
        
        DA_MainCycle();                                                         /* iqStack's main cycle */
        
        eeprom_handle();

        if (R_MAIN_tick_2msec)
        {
            R_MAIN_tick_2msec = FALSE;
            
            R_MAIN_measure_counter++;

            if (max14821_irq_events_timer)
            {
              max14821_irq_events_timer--;
            }
            else
            {
              max14821_irq_events_timer = 15;
              
              max1482x_watch_irq_events();
            }
            
            if (TDSIO_timer > 1)
            {
                TDSIO_timer--;
            }
            else if (TDSIO_timer == 1)
            {
                TDSIO_timer--;
                
                DDL_MH_TimerCallback(DPL_TIME_TDSIO_DELAY);
                DDL_MEH_TimerCallback(DPL_TIME_TDSIO_DELAY);
            }

            if (TFBD_timer > 1)
            {
                TFBD_timer--;
            }
            if (TFBD_timer == 1)
            {
                TFBD_timer--;
                
                DDL_MH_TimerCallback(DPL_TIME_TFBD_DELAY);
                DDL_MEH_TimerCallback(DPL_TIME_TFBD_DELAY);
            }
        }
    }
    /* End user code. Do not edit comment generated here */
}


/***********************************************************************************************************************
* Function Name: R_MAIN_UserInit
* Description  : This function adds user code before implementing main function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_MAIN_UserInit(void)
{
    /* Start user code. Do not edit comment generated here */
    uint8_t i = 0, j = 0, counter100msec = 0, counterIdle3sec = 0;
    
    EI();
                                                                                /* initialize variables */
    R_MAIN_tick_2msec = FALSE;
  
    R_TAU0_Channel7_Start();                                                    /* start the main timer (2.5 msec) */
    
    for (counter100msec = 75; counter100msec; counter100msec--)
    {                                                                           /* startup delay (~150 msec) */
        while (!R_MAIN_tick_2msec) { __asm("NOP"); }

        R_MAIN_tick_2msec = FALSE;

        R_WDT_Restart();
    }
    
    do
    {
        CHK_BREAK2(max1482x_init(),
            R_MAIN_hw_init_err = HW_ERR_8004_MAX1482X_SELFTEST);
        
        CHK_BREAK2(max31865_init(),
            R_MAIN_hw_init_err = HW_ERR_8005_MAX31865_SELFTEST);
        
        CHK_BREAK2(sce574x_init(),
            R_MAIN_hw_init_err = HW_ERR_8006_SCE574X_SELFTEST);
        
    } while (0U);

    max1482x_spi_start();
    CHK_VOID2(max1482x_spi_write_register(MAX1482X_REG_01_CQCONFIG,
            MAX1482X_CQCONFIG_40_HISLEW | MAX1482X_CQCONFIG_80_RXFILTER),       /* COM3 - DI Receiver Filter Control and Slew-Rate Control */
        R_WDT_DeviceRestart());
    CHK_VOID2(max1482x_spi_write_register(MAX1482X_REG_02_DIOCONFIG,
            MAX1482X_DIOCONFIG_08_DOEN | MAX1482X_DIOCONFIG_80_DOINV),          /* Enable DO Driver and DO Output Polarity */
        R_WDT_DeviceRestart());
    max1482x_spi_stop();
    
    max1482x_set_output(OFF);
    
    if (R_MAIN_hw_init_err == ERR_NONE
        || R_MAIN_hw_init_err == HW_ERR_8000_LOW_VOLTAGE
        || R_MAIN_hw_init_err == HW_ERR_8007_WATCHDOG_TIMER)
        return;

    while (1U)
    {                                                                           /* indicate forever on DO hardware internal error number */
        R_WDT_Restart();
        
        if (R_MAIN_tick_2msec)
        {
            R_MAIN_tick_2msec = FALSE;
        
            if (counter100msec) counter100msec--;
        }
    
        if (!counter100msec)
        {
            counter100msec = 50;
            
            if (counterIdle3sec)
            {
                counterIdle3sec--;
                max1482x_set_output(ON);
                continue;
            }
            
            if (j)
            {
                j--;
                max1482x_set_output(ON);
                continue;
            }
            
            if (i)
            {
                i--;
                j = 9;
                max1482x_set_output(OFF);
                continue;
            }

            i = (uint8_t)R_MAIN_hw_init_err - (uint8_t)ERR_BASE;
            
            counterIdle3sec = 30;
        }
    }
    /* End user code. Do not edit comment generated here */
}

/* Start user code for adding. Do not edit comment generated here */
static void hwtimer_handler(void)
{
    DDL_MH_TimerCallback(DPL_MASTER_MESSAGE_BYTE_RECEIVE_TIMEOUT);
    DDL_MEH_TimerCallback(DPL_MASTER_MESSAGE_BYTE_RECEIVE_TIMEOUT);
}

static void hwtimer_start(DPL_DelayTypeT delayType,
                          DPL_TargetModeT targetMode)
{
    static uint16_t const TIMEOUTS[] =
    {
        42245 /* COM1 - 11 bits - 2292 usec - 18,432 MHz */,
        5271 /* COM2 - 11 bits - 286 usec - 18,432 MHz */,
        884 /* COM3 - 11 bits - 48 usec - 18,432 MHz */
    };

    if (delayType != DPL_MASTER_MESSAGE_BYTE_RECEIVE_TIMEOUT)
          return;
  
    R_TAU0_Channel6_ChangeTimerCondition(TIMEOUTS[targetMode]);
    
    R_TAU0_Channel6_Start();
}

static void hwtimer_stop(void)
{
    R_TAU0_Channel6_Stop();
}

static void uart_recv_handler(uint8_t byte, int16_t err)
{
    switch (err)
    {
        case ERR_NONE:
            DPL_Transfer_ind(byte, DPL_TRANSFER_STATUS_NO_ERROR);
            break;
          
        case MAX1482X_ERR_8002_UART_PARITY:
            DPL_Transfer_ind(byte, DPL_TRANSFER_STATUS_PARITY_ERROR);
            break;
          
        case MAX1482X_ERR_8003_UART_FRAMING:
            DPL_Transfer_ind(byte, DPL_TRANSFER_STATUS_FRAMING_ERROR);
            break;

        case MAX1482X_ERR_8004_UART_OVERRUN:
            DPL_Transfer_ind(byte, DPL_TRANSFER_STATUS_OVERRUN);
            break;
    }
}

void DPL_Init(void)
{
    MD_SetINTTM06Callback(hwtimer_handler);
}

void DPL_StartTimer(DPL_DelayTypeT delayType)
{
    switch (delayType)
    {
        // Standard IO delay in ms
        // After TDSIO the Device falls back to SIO mode (if supported)
        // See: IO-Link spec v1.1.1 Oct 2011, 7.3.2.2, Table 40, p. 73
        case DPL_TIME_TDSIO_DELAY:
            TDSIO_timer = 30 + 1;                                               /* 60 ms */
            break;
        
        // Device specific master message received bytes timeout in bits
        // Timeout between received bytes of master message.
        // Used for observation of the message transmission time.
        case DPL_MASTER_MESSAGE_BYTE_RECEIVE_TIMEOUT:
            // stop timer
            // start timer 11 bit times: COM3 - 48usec, COM2 - 286usec, COM1 - 2292usec
            hwtimer_stop();
            hwtimer_start(
                DPL_MASTER_MESSAGE_BYTE_RECEIVE_TIMEOUT, DPL_SUPPORTED_BAUDRATE);
            break;
    
#ifdef DPL_USE_DEVICE_RESPONSE_DELAY
        // Device specific response delay in bits
        // Delay between master message request received moment
        // and moment of device response message sending start.
        // #DPL_USE_DEVICE_RESPONSE_DELAY switches on/off this timer
        // If it is off, device responds immediately, after answer is ready,
        // else it responds after end of master message plus this delay
        // if response is ready or later when the response will be ready.
        case DPL_DEVICE_RESPONSE_DELAY:
            // can be not started
            // stop timer
            // start timer 3 - 11 bit times
            break;
#endif
      
      // Device specific maximum cycle time
      // Can be not used.
      // See: IO-Link spec v1.1.1 Oct 2011, 7.3.3.5, p. 87 and 10.2, p. 152
      case DPL_MAX_CYCLE_TIME:
          // can be not started
          // stop timer
          // start timer
          // _CycMonitor_Timer = 25 + 1; // 50 ms
          break;
        
      // Fallback delay in ms
      // After a time TFBD the Device shall be switched to SIO mode.
      // See: IO-Link spec v1.1.1 Oct 2011, 7.3.2.3, Table 41, p. 74
      case DPL_TIME_TFBD_DELAY:
          // software timer tick 10ms
          TFBD_timer = 175 + 1;                                                 /* 350 ms */
          break;
        
      default:
          break;
  }
}

void DPL_StopTimer(DPL_DelayTypeT delayType)
{
    switch (delayType)
    {
        case DPL_TIME_TDSIO_DELAY:
            TDSIO_timer = 0;
            break;
        
        case DPL_MASTER_MESSAGE_BYTE_RECEIVE_TIMEOUT:
            hwtimer_stop();
            break;
    
#ifdef DPL_USE_DEVICE_RESPONSE_DELAY
        case DPL_DEVICE_RESPONSE_DELAY:
            break;
#endif

        case DPL_MAX_CYCLE_TIME:
            // CycMonitor_Timer = 0;
            break;
          
        case DPL_TIME_TFBD_DELAY:
            TFBD_timer = 0;
            break;
          
        default:
            break;
    }
}

void DPL_Mode_req(DPL_TargetModeT targetMode)
{ 
    TDSIO_timer = 0;
    TFBD_timer = 0;
    
    switch (targetMode)
    {
        case DPL_MODE_INACTIVE:
            CHK_VOID2(max1482x_wait_wakeup(DPL_Wakeup_ind),
                R_WDT_DeviceRestart());
            break;
          
        case DPL_MODE_DO:    
        case DPL_MODE_DI:
        case DPL_MODE_COM1:
        case DPL_MODE_COM2:
            break;
          
        case DPL_MODE_COM3:
            CHK_VOID2(max1482x_uart_start(uart_recv_handler),
                R_WDT_DeviceRestart());
            break;
          
        case DPL_MODE_PREOPERATE:
        case DPL_MODE_OPERATE:
            break;
    }
}

void DPL_TransferBuffer_req(UInteger8T length,
                            UInteger8T * buffer)
{
    max1482x_uart_send_async(buffer, length);
}
/* End user code. Do not edit comment generated here */
