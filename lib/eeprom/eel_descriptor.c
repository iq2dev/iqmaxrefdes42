/*================================================================================================*/
/* Project      = EEPROM Emulation Library                                                        */
/* Module       = eel_descriptor.c                                                                */
/* Device:      = RL78                                                                            */
/* Version      = V1.10                                                                           */
/* Date         = 6/9/2011 2:57:42 PM                                                             */
/*================================================================================================*/
/*                                  COPYRIGHT                                                     */
/*================================================================================================*/
/* Copyright (c) 2011 by Renesas Electronics Europe GmbH, a company of the Renesas Electronics    */
/* Corporation. All rights reserved.                                                              */
/*================================================================================================*/
/*                                                                                                */
/* Purpose:     contains the user defined EEL-variable descriptor                                 */
/*                                                                                                */
/*================================================================================================*/
/*                                                                                                */
/* Warranty Disclaimer                                                                            */
/*                                                                                                */
/* Because the Product(s) is licensed free of charge, there is no warranty of any kind whatsoever */
/* and expressly disclaimed and excluded by Renesas, either expressed or implied, including but   */
/* not limited to those for non-infringement of intellectual property, merchantability and/or     */
/* fitness for the particular purpose.                                                            */
/* Renesas shall not have any obligation to maintain, service or provide bug fixes for the        */
/* supplied Product(s) and/or the Application.                                                    */
/*                                                                                                */
/* Each User is solely responsible for determining the appropriateness of using the Product(s)    */
/* and assumes all risks associated with its exercise of rights under this Agreement, including,  */
/* but not limited to the risks and costs of program errors, compliance with applicable laws,     */
/* damage to or loss of data, programs or equipment, and unavailability or interruption of        */
/* operations.                                                                                    */
/*                                                                                                */
/* Limitation of Liability                                                                        */
/*                                                                                                */
/* In no event shall Renesas be liable to the User for any incidental, consequential, indirect,   */
/* or punitive damage (including but not limited to lost profits) regardless of whether such      */
/* liability is based on breach of contract, tort, strict liability, breach of warranties,        */
/* failure of essential purpose or otherwise and even if advised of the possibility of such       */
/* damages. Renesas shall not be liable for any services or products provided by third party      */
/* vendors, developers or consultants identified or referred to the User by Renesas in            */
/* connection with the Product(s) and/or the Application.                                         */
/*                                                                                                */
/*================================================================================================*/
/* Environment: IAR environment for RL78 (version V1.xx)                                          */
/*================================================================================================*/



/*==============================================================================================*/
/* compiler settings                                                                            */
/*==============================================================================================*/
#pragma language = extended

/*==============================================================================================*/
/* include files list                                                                           */
/*==============================================================================================*/
#define  __EEL_DESCRIPTOR_C
  #include  "eel_types.h"
  #include  "eel_descriptor.h"
#undef   __EEL_DESCRIPTOR_C

#if (EEL_STORAGE_TYPE == 'C')
  #include  "fcl_types.h"
#elif (EEL_STORAGE_TYPE=='D')
  #include  "fdl_types.h"
#else
  #error "EEL: wrong storage type defined !"
#endif


/* definition of variable types registered at EEL */
#include  "EEL_user_types.h"




/*==============================================================================================*/
/* import list                                                                                  */
/*==============================================================================================*/
/* empty */


/* data segment definition */
 #pragma dataseg="EEL_UDAT"

/*==============================================================================================*/
/* data definition                                                                              */
/*==============================================================================================*/
#if (EEL_STORAGE_TYPE=='C')
  __no_init eel_u16   EEL_var_ref[EEL_VAR_NO];
#else
  __no_init eel_u16   EEL_var_ref[1];                          /* dummy address for FDL variant */
#endif


/* constant segment definition */
 #pragma location="EEL_CNST"
 #pragma data_alignment=2

/*********************************************************************************************************/
/*******                                                                                           *******/
/*******      B E G I N    O F    C U S T O M I Z A B L E    D E C L A R A T I O N    A R E A      *******/
/*******                                                                                           *******/
/*********************************************************************************************************/

__far const eel_u08 eel_descriptor[EEL_VAR_NO+1][4] =
{
/*  identifier         word-size (1...64)                       byte-size (1..255)              RAM-Ref. */
/*  ---------------------------------------------------------------------------------------------------- */
    // device parameters
             /*(eel_u08)   1,     (eel_u08)((sizeof(type_DevPar)+3)/4),    (eel_u08)sizeof(type_DevPar),   0x01,  \*/
    //IOL_INDEX_0100_AMBTEMP_SP_LEVEL
    (eel_u08)0xA0,     (eel_u08)((sizeof( type_DevWorld)+3)/4), (eel_u08)sizeof( type_DevWorld),  0x01,  \
    //IOL_INDEX_0101_AMBTEMP_SP_HYSTERESIS
    (eel_u08)0xA1,     (eel_u08)((sizeof( type_DevWorld)+3)/4), (eel_u08)sizeof( type_DevWorld),  0x01,  \
    //IOL_INDEX_0102_AMBTEMP_DISPLAY_SCALE
    (eel_u08)0xA2,     (eel_u08)((sizeof(  type_DevByte)+3)/4), (eel_u08)sizeof(  type_DevByte),  0x01,  \
    //IOL_INDEX_0103_NWIRE_TYPE_CONNECTION
    (eel_u08)0xA3,     (eel_u08)((sizeof(  type_DevByte)+3)/4), (eel_u08)sizeof(  type_DevByte),  0x01,  \
    //IOL_INDEX_0109_REFERENCE_RESISTANCE
    (eel_u08)0xA9,     (eel_u08)((sizeof(type_DevDWorld)+3)/4), (eel_u08)sizeof(type_DevDWorld),  0x01,  \
    //IOL_INDEX_010A_NOMINAL_RESISTANCE
    (eel_u08)0xAA,     (eel_u08)((sizeof(type_DevDWorld)+3)/4), (eel_u08)sizeof(type_DevDWorld),  0x01,  \

    (eel_u08)0x00,     (eel_u08)(                        0x00), (eel_u08)(                0x00),  0x00,  \
};


/*********************************************************************************************************/
/*******                                                                                           *******/
/*******      E N D    O F    C U S T O M I Z A B L E    D E C L A R A T I O N    A R E A          *******/
/*******                                                                                           *******/
/*********************************************************************************************************/





/* ----------------------------------------------------------------------------------------------------- */
/* ------                                                                                          ----- */
/* ------     B E G I N    O F    U N T O U C H A B L E     D E C L A R A T I O N    A R E A       ----- */
/* ------                                                                                          ----- */
/* ----------------------------------------------------------------------------------------------------- */
 #pragma location="EEL_CNST"
__far const eel_u08   eel_refresh_bth_u08     = (eel_u08)EEL_REFRESH_BLOCK_THRESHOLD;

 #pragma location="EEL_CNST"
__far const eel_u08   eel_storage_type_u08    = (eel_u08)EEL_STORAGE_TYPE;

 #pragma location="EEL_CNST"
__far const eel_u08   eel_var_number_u08      = (eel_u08)EEL_VAR_NO;
/* ----------------------------------------------------------------------------------------------------- */
/* ------                                                                                          ----- */
/* ------     E N D    O F    U N T O U C H A B L E     D E C L A R A T I O N    A R E A           ----- */
/* ------                                                                                          ----- */
/* ----------------------------------------------------------------------------------------------------- */
