//******************************************************************************
//
//    Copyright (C) 2011-2014 by IQ2 Development GmbH, Neckartenzlingen 
//    All rights reserved
// 
//    This file is subject to the terms and conditions
//    defined in file 'Allgemeine Vertragsbedingungen.pdf',
//    which is part of this source code package.
//
//******************************************************************************

#ifndef _IQSTACK_DEV_PRECOMP_SETTINGS_H
#define _IQSTACK_DEV_PRECOMP_SETTINGS_H

/** 
* \brief hardware supported baudrate
* 
* can be:
*   #DPL_MODE_COM1
*   #DPL_MODE_COM2
*   #DPL_MODE_COM3
* 
*/
#define DPL_SUPPORTED_BAUDRATE DPL_MODE_COM3

/** 
 *  \brief Support of IO-Link v1.0
 *  
 *  If it is defined and 
 *  device is connected to old master with the previous IO-Link RevisionID 1.0, 
 *  stack will automatically switch to RevisionID 1.0 and
 *  device application will receive callback from stack.
 *
 */
#define DPL_BACK_COMPATIBLE_RevID_10

/** \brief Receive message buffer maximum length */
#define DPL_MESSAGE_BUFFER_MAX_LENGTH 70

/** \brief ISDU buffer maximum length */
#define DPL_ISDU_BUFFER_MAX_LENGTH 256

/** 
 *  \brief Calculate checksum of send/receive message using const array 
 *  It takes additional 256 byte of ROM, but speeds up checksum calculation.
 */
#define DPL_PACK_CKS_FAST

/** \brief if defined, master message checksum checking will be skipped */
//#define DPL_SKIP_CHECKSUM_CALC_MST_MESSAGE

/** \brief if defined, device message checksum calculation will be skipped */
//#define DPL_SKIP_CHECKSUM_CALC_DEV_MESSAGE

/** \brief Switch on/off DPL_DEVICE_RESPONSE_DELAY */
//#define DPL_USE_DEVICE_RESPONSE_DELAY

/**
 *  \defgroup DPL_Memory_Arrange_Management
 *
 *  It can be necessary for some hardware to arrange
 *  data memory segment manually.
 *  These macros are for this puporse.
 *  They surround defined in io-link code variables  
 *  to place them in special data segment.
 *  \{
 */

/** \brief Start placing defined variables in special data segment */
#define DPL_IOL_DATA_SEG_START _Pragma("dataseg=NEAR_Z")
/** \brief End placing defined variables in special data segment */
#define DPL_IOL_DATA_SEG_END _Pragma("dataseg=default")
/** \brief Defined variable prefix for special data segment */
#define DPL_IOL_DATA_SEG_PREFIX __no_init
/** \brief Defined variable postfix for special data segment */
#define DPL_IOL_DATA_SEG_POSTFIX

/** \} */ // end of DPL_Memory_Arrange_Management

#endif