//******************************************************************************
//
//    Copyright (C) 2011-2014 by IQ2 Development GmbH, Neckartenzlingen 
//    All rights reserved
// 
//    This file is subject to the terms and conditions
//    defined in file 'Allgemeine Vertragsbedingungen.pdf',
//    which is part of this source code package.
//
//******************************************************************************

#ifndef _IQSTACK_DEV_H
#define _IQSTACK_DEV_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \mainpage iqStack device integration manual
 *
 * \section desc_sec Introduction
 *
 * This project is dedicated to the implementation of IO-Link protocol (SDCI)
 * and based on IO-Link Interface and System specification v1.1.1 from Oct 2011:
 * http://io-link.com/en/Download/Download.php. 
 * Please get to know this specification before using this code.
 * 
 * The project consists of device IO-Link protocol 
 * and hardware porting interface. 
 * Integration of this code for device consists of
 * two parts: 
 * 1. porting this code to specific hardware, used to launch it
 * 2. creation of necessary application using the interface of this protocol realization
 * 
 * The project is cross-platform and created using ANSI C programming language. 
 * All code is commented with references to IO-Link specification. 
 * To integrate the stack, please, read these comments to get more information about porting.
 *
 * This code can be used free only for getting to know. 
 * For any commercial purposes, please, contact the developer 
 * http://www.iq2-development.de/kontakt.
 * 
 * \section porting_sec Porting device physical layer
 *
 * Some specific features should be defined 
 * for correct and optimized functioning of this code. 
 * They depend on hardware platform,
 * which will be used to launch this code.
 * 
 * Physical layer:
 * 1. data type definitions
 * 2. Support of connection with IO-Link master 1.0,
 *    using define DPL_BACK_COMPATIBLE_RevID_10
 * 3. SDCI message maximum byte length
 * 4. ISDU maximum byte length
 * 5. if DPL_USE_DEVICE_RESPONSE_DELAY is switched off,
 *    device responses to the master immediately after processing its request
 *    else with delay, which can be defined in DPL_StartTimer() 
 *    for case DPL_DEVICE_RESPONSE_DELAY (dev_porting.c).
 * 6. supported by device SDCI baudrate: COM1, COM2 or COM3
 * 7. data segment definitions, if it is necessary to place all variables
 *    of this project in some special device data segment.
 * 8. DPL_Wakeup_ind() should be called 
 *    if master wake-up impulse request has been received.
 * 9. DPL_Transfer_ind() should be called if a byte has been received from master.

 * Application layer:
 * 1. hardware and software timer definition (DPL_StartTimer, DPL_SWTimerCallback).
 *    Software timers may be not used (replaced by hardware). 
 *    They are used for long delays and implemented based on some hardware tick.
 *    - TDSIO and TFBD delay
 *    - master message interbyte timeout
 *    - device response delay
 *    - master cycle timeout can be used 
 *      to observe keeping by master some cycle time (communication integrity)
 * 2. this io-link implementation responses to master with full byte message (not bytewise),
 *    using DPL_TransferBuffer_req()
 * 3. DPL_Mode_req() is used for setting SDCI physical line mode,
 *    actions should defined for switching to:
 *    - supported SIO modes (inactive, digital input/output)
 *    - supported communication modes (COM1, COM2 or COM3)
 *    - preoperate and operate (optional, if necessary)
 * 
 *
 * \section appl_integration_sec Specific device application integration manual
 *
 * Device application consists of:
 * 1. io-link initialization should be done before using io-link protocol (call (DA_Init())):
 *    - setting io-link communication properties
 *    - setting device identification parameters
 *    - start SIO communication
 * 2. definition of actions within device main cycle iteration (forever loop DA_MainCycle()),
 *    this actions can be interrupted by hardware
 *    - process data handling: 
 *      setting input by DAL_SetInput_req() and its validity DAL_Control_req(), 
 *      getting output by DAL_GetOutput_req() and its validity by callback DAL_Control_ind()
 *    - sending events by DAL_Event_req() 
 *      and waiting conformation by callback DAL_Event_cnf()
 * 3. definition of io-link protocol callback actions:
 *    - DSM_DeviceMode() indicates protocol mode change (startup, preoperate, operate)
 *    - DSM_SwitchToRevID10_ind() should be used for some actions in application,
 *      related to switching to legacy version of io-link
 *    - DAL_Read_ind() indicates master ISDU read request, 
 *      should be confirm after processing (may be later) 
 *      by DAL_Read_rsp() if result is successful or
 *      by DAL_Read_err() with an error.
 *    - DAL_Write_ind() indicates master ISDU write request, 
 *      should be confirm after processing (may be later)
 *      by DAL_Write_rsp() if result is successful or
 *      by DAL_Write_err() with an error.
 *    - DAL_Abort_ind() indicates that master has aborted its current ISDU request
 *    - DAL_PDCycle_ind() indicates a new cycle of io-link communication
 *    - DAL_NewOutput_ind() indicates a new output data from master (similar to DAL_PDCycle_ind())
 *    - DDL_MEH_MHInfo_Commloss() indicates a communication loss, 
 *      can be used by actuators for some standard actions
 */

/**
 *  \defgroup porting_bool_values
 *
 *  BooleanT data type value
 *  See: IO-Link spec v1.1.1 Oct 2011, annex E.2.2, p. 238
 *  \{
 */

/** \brief bool value true  */
#define TRUE  0xFF
/** \brief bool value false */
#define FALSE 0

/** \} */ // end of porting_bool_values

/**
 *  \defgroup porting_basic_data_types
 *  
 *  Basic data types
 *  See: IO-Link spec v1.1.1 Oct 2011, annex E.2, p. 238
 *  \{
 */

/** \brief logical boolean type */
typedef unsigned char      BooleanT;
/** \brief unsigned number 8 bits */
typedef unsigned char      UInteger8T;
/** \brief unsigned number 16 bits */
typedef unsigned int       UInteger16T;
/** \brief unsigned number 32 bits */
typedef unsigned long      UInteger32T;

//typedef unsigned long      UInteger64T;

/** \brief signed number 8 bits */
typedef char               Integer8T;
/** \brief signed number 16 bits */
typedef int                Integer16T;
/** \brief signed number 32 bits */
typedef long               Integer32T;

//typedef long               Integer64T;

//typedef float              Float32T;

/** 
 *  \brief string contains maximum 232 symbols 
 *  encoded in US-ASCII (7 bit) or UTF-8 [7]. UTF-8 
 */
#define StringT(var)       Integer8T var[232]
/** \brief string contains maximum 232 bytes */
#define OctetStringT(var)  Integer8T var[232]

//typedef UInteger64T        TimeT;

/** \} */ // end of porting_basic_data_types

/**
 *  \brief Common Services Results
 *  
 *  Result type for services result: ok or error.
 *  
 */
typedef enum
{
  /** \brief ok result */
  SERVICE_RESULT_NO_ERROR           = 0,
  /** \brief some error */
  SERVICE_RESULT_ERROR              = 1,
  /** \brief state conflict */
  SERVICE_RESULT_STATE_CONFLICT     = 2,
  /** \brief no communication */
  SERVICE_RESULT_NO_COMM            = 3,
  /** \brief wrong parameter */
  SERVICE_RESULT_PARAMETER_CONFLICT = 4,
  /** \brief timing error */
  SERVICE_RESULT_TIMING_CONFLICT    = 5,
  /** \brief service busy error */
  SERVICE_RESULT_BUSY               = 6

} ResultT;

/** \brief State type for state machines. */
typedef  UInteger8T StateT;

/** \brief Enum type for services arguments values. */
typedef  UInteger8T EnumT;

/**
 *  \brief DPL_tranfer_status
 *  
 *  PL Transfer status values of DPL_Transfer_ind()
 *  See: IO-Link spec v1.1.1 Oct 2011, 5.2.2.3, p. 42
 *  
 */

typedef enum
{
  /** \brief no error */
  DPL_TRANSFER_STATUS_NO_ERROR = 0,
  /** \brief parity error */
  DPL_TRANSFER_STATUS_PARITY_ERROR = 1,
  /** \brief framing error */
  DPL_TRANSFER_STATUS_FRAMING_ERROR = 2,
  /** \brief status overrun */
  DPL_TRANSFER_STATUS_OVERRUN = 3
  
} DPL_TransferStatusT;

/**
 *  \brief DPL_TargetMode
 *
 *  Physical layer modes, switched by DPL_Mode_req()
 *  See: IO-Link spec v1.1.1 Oct 2011, 5.2.2.1, p. 41
 *  
 */

typedef enum
{
  /** \brief inactive mode */
  DPL_MODE_INACTIVE = 3,
  /** \brief sdci communication with baudrate COM1 4,8   kbit/s */
  DPL_MODE_COM1 = 0,
  /** \brief sdci communication with baudrate COM2 38,4  kbit/s */
  DPL_MODE_COM2 = 1,
  /** \brief sdci communication with baudrate COM3 230,4 kbit/s */
  DPL_MODE_COM3 = 2,
  /** \brief digital output */
  DPL_MODE_DO = 4,
  /** \brief digital input */
  DPL_MODE_DI = 5,
  
  /** \brief preoperate */
  DPL_MODE_PREOPERATE = 6,
  /** \brief operate */
  DPL_MODE_OPERATE = 7

} DPL_TargetModeT;

/**
 *  \brief DPL_Delay_types
 *
 *  Delay types used in protocol
 *  See: IO-Link spec v1.1.1 Oct 2011, 7.3.2.2, Table 40, p. 73
 *  
 */
typedef enum
{
  /** \brief Standard IO delay */
  DPL_TIME_TDSIO_DELAY,
  /** \brief Fallback delay */
  DPL_TIME_TFBD_DELAY,
  /** \brief Device specific master message received bytes timeout */
  DPL_MASTER_MESSAGE_BYTE_RECEIVE_TIMEOUT,
  /** \brief Device specific response delay */
  DPL_DEVICE_RESPONSE_DELAY,
  /** \brief Device specific maximum cycle time */
  DPL_MAX_CYCLE_TIME

} DPL_DelayTypeT;

/**
 *  \brief System manager states
 *
 *  System manager states
 *  See: IO-Link spec v1.1.1 Oct 2011, 9.3.3.2, p. 145-146
 *  
 */
typedef enum
{  
  /** \brief System manager idle state
   *
   *  In DSM_Idle the SM is waiting for configuration by the Device application and to be set
   *  to SIO mode. The state is left on receiving a DSM_SetDeviceMode(SIO) request from
   *  the Device application. The following sequence of services shall be executed
   *  between Device application and SM.
   *  - DSM_SetDeviceCom(initial parameter list)
   *  - DSM_SetDeviceIdent(VID, initial DID, FID)
   *
   */
  DSM_STATE_IDLE_0          = 0,
  
  /** \brief System manager SIO state
   *
   *  In DSM_SIO the SM Line Handler is remaining in the default SIO mode. The Physical
   *  Layer is set to the SIO mode characteristics defined by the Device application via the
   *  SetDeviceMode service. The state is left on receiving a DDL_Mode(STARTUP)
   *  indication.
   *
   */
  DSM_STATE_SIO_1           = 1,
  
  /** \brief System manager communication establishment state
   *
   *  In DSM_ComEstablish the SM is waiting for the communication to be established in the
   *  Data Link Layer. The state is left on receiving a DDL_Mode(INACTIVE) or a
   *  DDL_Mode(COMx) indication, where COMx may be any of COM1, COM2 or COM3.
   *
   */
  DSM_STATE_COM_ESTABLISH_2 = 2,
  
  /** \brief System manager communication startup state
   *
   *  In DSM_ComStartup the communication parameter (DP 02h to 06h) are read by the
   *  Master SM via DDL_Read requests. The state is left on receiving a
   *  DDL_Mode(INACTIVE), a DDL_Mode(OPERATE) indication (Master V1.0 only) or a
   *  DDL_Write(MASTERIDENT) request (Master >V1.0).
   *
   */
  DSM_STATE_COM_STARTUP_3   = 3,
  
  /** \brief System manager identification startup state
   *
   *  In DSM_IdentStartup the identification data (VID, DID, FID) are read and verified by the
   *  Master. In case of incompatibilities the Master SM writes the supported SDCI Revision
   *  (RID) and configured DeviceID (DID) to the Device. The state is left on receiving a
   *  DDL_Mode(INACTIVE), a DDL_Mode(PREOPERATE) indication (compatibility check
   *  passed) or a DDL_Write(DEVICEIDENT) request (new compatibility requested).
   *
   */
  DSM_STATE_IDENT_STARTUP_4 = 4,
  
  /** \brief System manager identification check state
   *
   *  In DSM_IdentCheck the SM waits for new initialization of communication and
   *  identification parameters. The state is left on receiving a DDL_Mode(INACTIVE)
   *  indication or a DDL_Read(DP 02h) request.
   *  Within this state the Device application shall check the RID and DID parameters from
   *  the SM and set these data to the supported values. Therefore the following sequence
   *  of services shall be executed between Device application and SM.
   *  - DSM_GetDeviceCom(configured RID, parameter list)
   *  - DSM_GetDeviceIdent(configured DID, parameter list)
   *  - Device application checks and provides compatibility function and parameters
   *  - DSM_SetDeviceCom(new supported RID, new parameter list)
   *  - DSM_SetDeviceIdent(new supported DID, parameter list)
   *
   */
  DSM_STATE_IDENT_CHECK_5   = 5,
  
  /** \brief System manager compatibility startup state
   *
   *  In DSM_CompatStartup the communication and identification data are reread and
   *  verified by the Master SM. The state is left on receiving a DDL_Mode(INACTIVE) or a
   *  DDL_Mode(PREOPERATE) indication.
   *
   */
  DSM_STATE_COMP_STARTUP_6  = 6,
  
  /** \brief System manager preoperate state
   *
   *  During DSM_Preoperate the the SerialNumber can be read and verified by the Master
   *  SM, as well as data storage and Device parameterization may be executed. The state
   *  is left on receiving a DDL_Mode(INACTIVE), a DDL_Mode(STARTUP) or a
   *  DDL_Mode(OPERATE) indication.
   *
   */
  DSM_STATE_PREOPERATE_7   = 7,
  
  /** \brief System manager operate state
   *
   *  During DSM_Operate the cyclic process data exchange and acyclic On-Request data
   *  transfer are active. The state is left on receiving a DDL_Mode(INACTIVE) or a
   *  DDL_Mode(STARTUP) indication.
   *
   */
  DSM_STATE_OPERATE_8      = 8
  
} DSM_StateT;

/**
 *  \brief System manager cycle time bases
 *
 *  the time base for the calculation of the MinCycleTime and MasterCycleTime
 *  See: IO-Link spec v1.1.1 Oct 2011, B.1.4, p. 216
 *  
 */
typedef enum
{
  /** \brief 0,1 ms */
  DSM_TIME_BASE_01         = 0x00,
  /** \brief 0,4 ms */
  DSM_TIME_BASE_04         = 0x40,
  /** \brief 1,6 ms */
  DSM_TIME_BASE_16         = 0x80,
  /** \brief Reserved */
  DSM_TIME_BASE_RESERVED   = 0xC0
  
} DSM_CycTimeBaseT;

/**
 *  \brief System manager modes
 *
 *  System manager modes
 *  See: IO-Link spec v1.1.1 Oct 2011, 9.3.2.7, p. 144
 *  
 */
typedef enum
{ 
  DSM_MODE_IDLE          = 0,
  DSM_MODE_SIO           = 1,
  DSM_MODE_ESTABCOM      = 2,     
  DSM_MODE_COM1          = 3,
  DSM_MODE_COM2          = 4,
  DSM_MODE_COM3          = 5,
  DSM_MODE_STARTUP       = 6,
  DSM_MODE_IDENT_STARTUP = 7,
  DSM_MODE_IDENT_CHANGE  = 8,
  DSM_MODE_PREOPERATE    = 9,
  DSM_MODE_OPERATE       = 10
  
} DSM_ModeT;

/**
 *  \brief System manager OD lengths
 *
 *  System manager possible OD lengths of M-sequence
 *  See: IO-Link spec v1.1.1 Oct 2011, A.2.6, Table A.7-10 p. 200-201
 *  
 */
typedef enum
{ 
  DSM_MSEQ_OD_LENGTH_1  = 1,
  DSM_MSEQ_OD_LENGTH_2  = 2,
  DSM_MSEQ_OD_LENGTH_8  = 8,
  DSM_MSEQ_OD_LENGTH_32 = 32
  
} DSM_MseqODLengT;

/**
 * \brief System manager device communication configuration type
 *
 * See: IO-Link spec v1.1.1 Oct 2011, 9.3.2.2, p. 139, ParameterList
 *
 */
typedef struct
{
  /**
   * \brief the SIO mode supported by the Device
   *
   * can be:
   *  DPL_MODE_INACTIVE,
   *  DPL_MODE_DO,
   *  DPL_MODE_DI
   *
   */
  DPL_TargetModeT supportedSIOMode;

  /**
   * \brief the transmission rates supported by the Device
   *
   * can be:
   *  #DPL_MODE_COM1
   *  #DPL_MODE_COM2
   *  #DPL_MODE_COM3
   *
   */
  DPL_TargetModeT supportedBaudrate;

  /**
   * \brief the minimum cycle time supported by the Device
   *
   *  Direct page 1 parameter address 1
   *  cycle time = multiplier * timeBase + const
   *  See: IO-Link spec v1.1.1 Oct 2011, B.1.3, p. 205
   *
   */
  UInteger8T masterCycleTime;

  /**
   * \brief the minimum cycle time supported by the Device
   *
   *  Direct page 1 parameter address 2
   *  min cycle time = multiplier * timeBase + const
   *  See: IO-Link spec v1.1.1 Oct 2011, B.1.4, p. 205
   *
   */
  UInteger8T minCycleTime;

  /** \brief the time base for the calculation of the MinCycleTime */
  DSM_CycTimeBaseT      minCycleTimeBase;

  /** \brief a 6-bit multiplier for the calculation of the MinCycleTime
   *
   *  can be from 0 to 63
   *
   */
  UInteger8T minCycleTimeMultiplier;

  /**
   * \brief the M-sequence capabilities supported by the Device
   *
   *  Direct page 1 parameter address 3
   *  - ISDU support
   *  - OPERATE M-sequence types
   *  - PREOPERATE M-sequence types
   *
   *  See: IO-Link spec v1.1.1 Oct 2011, B.1.5, p. 206
   *
   */
  UInteger8T msequenceCapability;

  /**
   *  \brief the M-sequence capability preoperate
   *
   *  See: IO-Link spec v1.1.1 Oct 2011, A.2.6, p. 200, Table A.8
   */
  UInteger8T msequenceCapabilityPreoperate;

  /**
   *  \brief the M-sequence capability operate
   *
   *  See: IO-Link spec v1.1.1 Oct 2011, A.2.6, p. 201, Table A.9-10
   */
  UInteger8T msequenceCapabilityOperate;

  BooleanT   isISDUSupported;
  
  /**
   *  \brief the M-sequence On-request data length preoperate
   *  
   *  Used only for IO-Link RevID > 1.1. Can be only 1, 2, 8, 32 (see DSM_MseqODLengT).
   *  See: IO-Link spec v1.1.1 Oct 2011, A.2.6, Table A.7-10 p. 200-201
   */
  DSM_MseqODLengT msequenceODLengthPreoper;

  /**
   *  \brief the M-sequence On-request data length operate
   *  
   *  Can be only 1, 2 for IO-Link RevID 1.0, 
   *  and also 8, 32 IO-Link RevID > 1.1 (see DSM_MseqODLengT).
   *  See: IO-Link spec v1.1.1 Oct 2011, A.2.6, Table A.7-10 p. 200-201
   */
  DSM_MseqODLengT msequenceODLengthOper;

  /**
   * \brief the M-sequence type preoperate
   *
   *  can be:
   *  #DDL_MEH_MESSAGE_TYPE_0
   *  #DDL_MEH_MESSAGE_TYPE_1
   *
   */
  EnumT      msequenceTypePreoper;

  /**
   * \brief the M-sequence type operate
   *
   *  can be:
   *  #DDL_MEH_MESSAGE_TYPE_0
   *  #DDL_MEH_MESSAGE_TYPE_1
   *  #DDL_MEH_MESSAGE_TYPE_2
   *
   */
  EnumT      msequenceTypeOper;

  /**
   * \brief the protocol revision RID supported by the Device
   *
   *  Direct page 1 parameter address 4.
   *  It can be only 0x10 or 0x11 for current IO-Link implementation.
   *  See: IO-Link spec v1.1.1 Oct 2011, B.1.6, p. 206
   *
   */
  UInteger8T revisionID;

  /**
   * \brief packed length of process data input to be sent to the Master
   *
   *  Direct page 1 parameter address 5
   *  See: IO-Link spec v1.1.1 Oct 2011, B.1.7, p. 217-218
   *
   */
  UInteger8T processDataIn;

  BooleanT   isBitLengthPDIn;
  /**
   * \brief the length of process data input to be sent to the Master
   *
   *  Set by Device Application. 
   *  Can be from 0 to 16 bits or from 3 to 32 bytes (depending on isBitLengthPDIn).
   *  See: IO-Link spec v1.1.1 Oct 2011, B.1.7, p. 217-218
   */
  UInteger8T lengthPDIn;

  /** \brief DO NOT SET FROM DEVICE APPLICATION !!! */
  UInteger8T lengthPDInBytes;

  /**
   * \brief packed length of process data output to be sent to the Master
   *
   *  Direct page 1 parameter address 6
   *  See: IO-Link spec v1.1.1 Oct 2011, B.1.7-8, p. 217-218
   *
   */
  UInteger8T processDataOut;

  BooleanT   isBitLengthPDOut;
  /**
   * \brief the length of process data output to be sent to the Master
   *
   *  Set by Device Application. 
   *  Can be from 0 to 16 bits or from 3 to 32 bytes (depending on isBitLengthPDOut).
   *  See: IO-Link spec v1.1.1 Oct 2011, B.1.7-8, p. 217-218
   */
  UInteger8T lengthPDOut;

  /** \brief DO NOT SET FROM DEVICE APPLICATION !!! */
  UInteger8T lengthPDOutBytes;

#ifdef DPL_BACK_COMPATIBLE_RevID_10
  BooleanT   isInterleave;
#endif
  
  BooleanT   isOK;

  /** \brief System manager device mode changed flag */
  BooleanT   isDeviceModeChanged;

  /** \brief Device mode */
  DSM_ModeT  deviceMode;

} DSM_DeviceComT;

/**
 * \brief System manager device identification data type
 *
 * See: IO-Link spec v1.1.1 Oct 2011, 9.3.2.4, p. 141, ParameterList; 
 *      B.1.1, Table B.1, p 214; B.1.9-11, p. 218
 *
 */
typedef struct
{
  /** \brief Direct page 1 parameter address 7 */
  UInteger8T vendorID_HI;
  /** \brief Direct page 1 parameter address 8 */
  UInteger8T vendorID_LO;

  /** \brief Direct page 1 parameter address 9 */
  UInteger8T deviceID_HI;
  /** \brief Direct page 1 parameter address 10 */
  UInteger8T deviceID_MID;
  /** \brief Direct page 1 parameter address 11 */
  UInteger8T deviceID_LO;

  /** \brief Direct page 1 parameter address 12 */
  UInteger8T functionID_HI;
  /** \brief Direct page 1 parameter address 13 */
  UInteger8T functionID_LO;

  BooleanT isOK;

} DSM_DeviceIdentT;

/**
 *  \defgroup DDL_Event_Handler_Event_instances
 *
 *  DL event handler event instances
 *  of DDL_EH_EventT and DDL_Event_req()
 *  See: IO-Link spec v1.1.1 Oct 2011, 7.2.1.15, p. 64, A.6.4, Table A.17, p. 211
 *  \{
 */

#define DDL_EH_EVENT_INSTANCE_UNKNOWN 0
//#define DDL_EH_EVENT_INSTANCE_PL      1
//#define DDL_EH_EVENT_INSTANCE_DL      2
//#define DDL_EH_EVENT_INSTANCE_AL      3
#define DDL_EH_EVENT_INSTANCE_APPL    4

/** \} */ // end of DDL_Event_Handler_Event_instances

/**
 *  \defgroup DDL_Event_Handler_Event_sources
 *
 *  DL event handler event sources
 *  of DDL_EH_EventT and DDL_Event_req()
 *  See: IO-Link spec v1.1.1 Oct 2011, 7.2.1.15, p. 64, A.6.4, Table A.18, p. 212
 *  \{
 */

#define DDL_EH_EVENT_SOURCE_REMOTE 0
#define DDL_EH_EVENT_SOURCE_LOCAL  8

/** \} */ // end of DDL_Event_Handler_Event_sources

/**
 *  \defgroup DDL_Event_Handler_Event_types
 *
 *  DL event handler event types
 *  of DDL_EH_EventT and DDL_Event_req()
 *  See: IO-Link spec v1.1.1 Oct 2011, 7.2.1.15, p. 64, A.6.4, Table A.19, p. 212
 *  \{
 */

#define DDL_EH_EVENT_TYPE_RESERVED        0
#define DDL_EH_EVENT_TYPE_NOTIFICATION 0x10
#define DDL_EH_EVENT_TYPE_WARNING      0x20
#define DDL_EH_EVENT_TYPE_ERROR        0x30

/** \} */ // end of DDL_Event_Handler_Event_types

/**
 *  \defgroup DDL_Event_Handler_Event_modes
 *
 *  DL event handler event modes
 *  of DDL_EH_EventT and DDL_Event_req()
 *  See: IO-Link spec v1.1.1 Oct 2011, 7.2.1.15, p. 64, A.6.4, Table A.20, p. 212
 *  \{
 */

#define DDL_EH_EVENT_MODE_RESERVED        0
#define DDL_EH_EVENT_MODE_SINGLE_SHOT  0x40
#define DDL_EH_EVENT_MODE_DISAPPEARS   0x80
#define DDL_EH_EVENT_MODE_APPEARS      0xC0

/** \} */ // end of DDL_Event_Handler_Event_modes

/**
 *  \brief DL Event handler event type
 *
 *  See: IO-Link spec v1.1.1 Oct 2011, 7.2.1.15, p. 64, A.6.4-5, p. 211
 *
 */
typedef struct
{
  /**
   * \brief Event instance
   *
   * can be:
   * #DDL_EH_EVENT_INSTANCE_UNKNOWN
   * #DDL_EH_EVENT_INSTANCE_APPL
   *
   */
  EnumT     instance;
  /**
   * \brief Event type
   *
   * can be:
   * #DDL_EH_EVENT_TYPE_RESERVED
   * #DDL_EH_EVENT_TYPE_NOTIFICATION
   * #DDL_EH_EVENT_TYPE_WARNING
   * #DDL_EH_EVENT_TYPE_ERROR
   *
   */
  EnumT     type;
  /**
   * \brief Event mode
   *
   * can be:
   * #DDL_EH_EVENT_MODE_RESERVED
   * #DDL_EH_EVENT_MODE_SINGLE_SHOT
   * #DDL_EH_EVENT_MODE_DISAPPEARS
   * #DDL_EH_EVENT_MODE_APPEARS
   *
   */
  EnumT     mode;
  /**
   * \brief Event source
   *
   * can be:
   * #DDL_EH_EVENT_SOURCE_REMOTE
   * #DDL_EH_EVENT_SOURCE_LOCAL
   *
   */
  EnumT     source;
  /** \brief Event code
   * See: IO-Link spec v1.1.1 Oct 2011, A.6.5, p. 212, Annex D, p. 235
   */
  UInteger16T code;
} DDL_EH_EventT;

/**
 *  \brief IO-Link device protocol initialization
 *
 *  This function initializes io-link device protocol.
 *  It should be called before starting using io-link protocol in device application.
 *
 */
void DEV_IOL_Init(void);

/**
 *  \brief IO-Link device protocol main cycle
 *
 *  This function process main io-link device protocol tasks
 *  outside all interrupts.
 *  It should be called in a main cycle
 *  of device application firmware.
 *
 */
void DEV_IOL_MainCycle(void);

/**
 *  \brief SM set device communication configuration
 *
 *  This function is used to configure the communication properties supported
 *  by the Device in the System Management.
 *  It should be called from Device application
 *  after DSM_SetDeviceMode(DSM_MODE_IDLE)
 *  and before DSM_SetDeviceIdent(), DSM_SetDeviceMode(DSM_MODE_SIO).
 *
 *  The pointer to DSM_DeviceComT can be obtained by DSM_GetDeviceCom().
 *  The following fields of this DSM_DeviceComT should be defined before call:
 *
 *  - supportedSIOMode
 *  - supportedBaudrate
 *  - minCycleTimeBase   
 *  - minCycleTimeMultiplier 
 *  - msequenceODLengthPreoper
 *  - msequenceODLengthOper
 *  - isISDUSupported
 *  - revisionID
 *  - isBitLengthPDIn
 *  - lengthPDIn
 *  - isBitLengthPDOut
 *  - lengthPDOut
 *
 *  See these fields description in DSM_DeviceComT definition.
 *
 *  See: IO-Link spec v1.1.1 Oct 2011, 9.3.2.2, p.139
 *
 *  \return result of operation, can be:
 *      #SERVICE_RESULT_NO_ERROR
 *      #SERVICE_RESULT_STATE_CONFLICT - some error
 *
 */
ResultT DSM_SetDeviceCom (void);

/**
 *  \brief SM set device identification data
 *
 *  This function is used to configure the Device identification data in the
 *  System Management.
 *  It should be called from Device application
 *  after DSM_SetDeviceMode(DSM_MODE_IDLE), DSM_GetDeviceCom()
 *  and before DSM_SetDeviceMode(DSM_MODE_SIO).
 *
 *  The pointer to DSM_DeviceIdentT can be obtained by DSM_GetDeviceIdent().
 *  The following fields of this DSM_DeviceIdentT should be defined before call:
 *
 *  - vendorID
 *  - deviceID
 *  - functionID
 *
 *  See these fields description in DSM_DeviceIdentT definition.
 *
 *  See: IO-Link spec v1.1.1 Oct 2011, 9.3.2.4, p.141
 *
 *  \return result of operation, can be:
 *      #SERVICE_RESULT_NO_ERROR
 *      #SERVICE_RESULT_STATE_CONFLICT - some error
 *
 */
ResultT DSM_SetDeviceIdent (void);

/**
 *  \brief SM get actual device communication configuration
 *
 *  This function returns pointer to DSM_DeviceComT.
 *
 *  See: IO-Link spec v1.1.1 Oct 2011, 9.3.2.3, p.140
 *
 */
DSM_DeviceComT * DSM_GetDeviceCom (void);

/**
 *  \brief SM get actual device identification data
 *
 *  This function returns pointer to DSM_DeviceIdentT.
 *
 *  See: IO-Link spec v1.1.1 Oct 2011, 9.3.2.5, p.142
 *
 */
DSM_DeviceIdentT * DSM_GetDeviceIdent (void);

/**
 *  \brief SM set mode
 *
 *  This function is used to set the Device
 *  into a defined operational state during initialization.
 *  It should be called from Device application.
 *  See: IO-Link spec v1.1.1 Oct 2011, 9.3.2.6, p.143
 *
 *  \param mode system manager mode, can be
 *        #DSM_MODE_IDLE
 *        #DSM_MODE_SIO
 *
 *  \return result of operation, can be:
 *      #SERVICE_RESULT_NO_ERROR
 *      #SERVICE_RESULT_STATE_CONFLICT - some error
 *
 */
ResultT DSM_SetDeviceMode (EnumT mode);

/**
 *  \brief SM indicate changes of communication states
 *
 *  This function is used by System manager
 *  to indicate changes of communication states to the
 *  Device application.
 *
 *  See: IO-Link spec v1.1.1 Oct 2011, 9.3.2.7, p.144
 */
extern void DSM_DeviceMode (DSM_ModeT mode);

/**
 *
 * \brief SM indicate switching to RevID 1.0
 *
 *  This function is used by System manager
 *  to indicate switching to RevID 1.0 to the
 *  Device application, because of connection with old IO-Link master 1.0
 *
 *  It should be processed quickly !!!
 *  because it is called from DPL_Transfer_ind()
 *  (mainly by interrupt)
 */
extern void DSM_SwitchToRevID10_ind(void);

/**
 *  \brief Set process data input validity
 *
 *  This function should be called from device application
 *  to set current process data input validity flag.
 *  See: IO-Link spec v1.1.1 Oct 2011, 8.2.2.12, p.111
 *  
 *  \param pdInInvalid this flag defines 
 *                     whether process data input is invalid or not
 *  
 */
void DAL_Control_req (BooleanT controlCode);

/**
 *  \brief Get process data output request
 *
 *  This function should be called from device application
 *  to obtain current process data output values.
 *  See: IO-Link spec v1.1.1 Oct 2011, 8.2.2.8, p.109
 *  
 */
UInteger8T * DAL_GetOutput_req (void);

/**
 *  \brief Set process data input request
 *
 *  This function should be called from device application
 *  to set current process data input values.
 *  See: IO-Link spec v1.1.1 Oct 2011, 8.2.2.6, p.108
 *  
 */
UInteger8T * DAL_SetInput_req (void);

/**
 *  \brief On-request data read response
 *
 *  This function should be called from device application
 *  to answer master read request.
 *  See: IO-Link spec v1.1.1 Oct 2011, 8.2.2.1, p.104
 *  
 *  \param length data length of response
 *
 */
void DAL_Read_rsp (UInteger8T length);

/**
 *  \brief AL indicate PD Out validity
 *
 *  This function is used by Application layer
 *  to indicate PD Out validity to Device application.
 *  It should be defined in Device application.
 *  See: IO-Link spec v1.1.1 Oct 2011, 8.2.2.12, p. 111
 *  see comments in dev_al.h
 *
 *  It should be processed quickly !!!
 *  because it is called from DPL_Transfer_ind()
 *  (mainly by interrupt)
 *
 *  controlCode:
 *  #TRUE  - pd out valid
 *  #FALSE - pd out invalid
 */
extern void DAL_Control_ind (BooleanT controlCode);

/**
 *  \brief AL indicate the reception of updated valid output data from master
 *
 *  This function is used by Application layer
 *  to indicate the reception of updated valid output data from master.
 *
 *  See: IO-Link spec v1.1.1 Oct 2011, 8.2.2.9, p.109
 *
 *  It should be processed quickly !!!
 *  because it is called from DPL_Transfer_ind()
 *  (mainly by interrupt)
 */
extern void DAL_NewOutput_ind (void);

/**
 *  \brief AL indicate the end of a process data cycle
 *
 *  This function is used by Application layer
 *  to indicate the end of a process data cycle. 
 *  The Device application can use this service 
 *  to transmit new input data to the application layer via DAL_SetInput_req().
 *
 *  See: IO-Link spec v1.1.1 Oct 2011, 8.2.2.7, p.108
 *
 *  It should be processed quickly !!!
 *  because it is called from DPL_Transfer_ind()
 *  (mainly by interrupt)
 */
extern void DAL_PDCycle_ind (void);

/**
 *  \brief AL indicate OD read
 *
 *  This function is used by Application layer
 *  to indicate OD read to Device application.
 *  It should be defined in Device application.
 *
 *  See: IO-Link spec v1.1.1 Oct 2011, 8.2.2.1, p. 104
 */
extern void DAL_Read_ind (UInteger16T index, UInteger8T subindex, 
                          UInteger8T * data);

/**
 *  \brief On-request data read error response
 *
 *  This function should be called from device application,
 *  if an error has occurred while processing master read request.
 *  See: IO-Link spec v1.1.1 Oct 2011, 8.2.2.1, p.104; Annex C, p.230
 *  
 *  \param errorCode               error code
 *  \param addErrorCode additional error code
 *  
 */
void DAL_Read_err (UInteger8T errorCode, UInteger8T addErrorCode);

/**
 *  \brief On-request data write response
 *
 *  This function should be called from device application,
 *  when master service write request is complete.
 *  See: IO-Link spec v1.1.1 Oct 2011, 8.2.2.2, p.105
 *  
 */
void DAL_Write_rsp (void);

/**
 *  \brief AL indicate OD write
 *
 *  This function is used by Application layer
 *  to indicate OD write to Device application.
 *
 *  See: IO-Link spec v1.1.1 Oct 2011, 8.2.2.2, p. 105
 */
extern void DAL_Write_ind (UInteger16T index, UInteger8T subindex,
                           UInteger8T  dataLength, UInteger8T * data);

/**
 *  \brief On-request data write error response
 *
 *  This function should be called from device application,
 *  if an error has occurred while processing master write request.
 *  See: IO-Link spec v1.1.1 Oct 2011, 8.2.2.2, p.105; Annex C, p.230
 *  
 *  \param errorCode               error code
 *  \param addErrorCode additional error code
 *  
 */
void DAL_Write_err (UInteger8T errorCode, UInteger8T addErrorCode);

/**
 *  \brief AL indicate the aborting of current DAL_Read or DAL_Write
 *
 *  This function is used by Application layer
 *  to indicate the aborting of current
 *  DAL_Read or DAL_Write services by Master.
 *  It abandons the response to DAL_Read or DAL_Write services.
 *
 *  See: IO-Link spec v1.1.1 Oct 2011, 8.2.2.3, p. 106
 *
 *  It should be processed quickly !!!
 *  because it is called from DPL_Transfer_ind()
 *  (mainly by interrupt)
 */
extern void DAL_Abort_ind (void);

/**
 *  \brief Event request
 *
 *  This function should be called from device application,
 *  when several events have occurred in device application
 *  and should be delivered to master.
 *  DAL_Event_cnf() will be called, when they all are delivered. 
 *  See: IO-Link spec v1.1.1 Oct 2011, 8.2.2.11, p.110
 *  
 *  \param eventCount size of events list
 *  \param events     events list
 *  
 */
void DAL_Event_req (UInteger8T eventCount, DDL_EH_EventT * events);

/**
 *  \brief AL confirm the reception of sent events by master
 *
 *  This function is used by Application layer
 *  to confirm the reception of sent events by master.
 *  If events have been sent by DAL_Event_req(),
 *  next events can be sent only after reception of this confirmation.
 *
 *  See: IO-Link spec v1.1.1 Oct 2011, 8.2.2.11, p.110
 */
extern void DAL_Event_cnf (void);

/**
 *  \brief DL Mode Handler Timer callback
 *
 *  This function checks mode handler timeouts.
 */
void DDL_MH_TimerCallback (DPL_DelayTypeT delayType);

/**
 *  \brief DL Message Handler Timer callback
 *
 *  This function checks message handler timeouts.
 */
void DDL_MEH_TimerCallback (DPL_DelayTypeT delayType);

/**
 *  \brief An exceptional operation within the message handler (COMMLOST)
 *
 *  The service MHInfo signals an exceptional
 *  operation within the message handler (COMMLOST).
 *  Actuators shall observe this information
 *  and take corresponding actions.
 *
 *  See: IO-Link spec v1.1.1 Oct 2011, 7.2.2.6,  p. 69, 7.3.3.5, p. 86
 */
extern void DDL_MEH_MHInfo_Commloss (void);

/**
 *  \defgroup Device_application
 *
 *  Device application
 *  See: IO-Link spec v1.1.1 Oct 2011, 10, p. 151
 *  \{
 */

/**
 *  \brief Device application initialization
 *
 *  This function should be called only once
 *  while device initializing
 *  before start using io-link and
 *  before device main forever loop !!!
 *
 */
extern void DA_Init (void);

/**
 *  \brief Init device SM comm and ident
 *
 *  Init of stack system manager
 *  communication and identification parameters
 *  before start of IO-Link.
 *
 */
extern void DA_InitDSM (void);

/**
 *  \brief Device application main cycle
 *
 *  This function should be called
 *  in device main forever loop
 *  while using io-link and after DA_Init()
 *
 */
extern void DA_MainCycle (void);

/** \} */ // end of Device_application

/** 
 *  \defgroup Physical_Layer
 *
 *  Physical layer of the specific hardware
 *  See: IO-Link spec v1.1.1 Oct 2011, 5, p. 39
 *  \{
 */

/**
 *  \brief PL init
 *  
 *  This function initializes Device Physical layer:
 *  IO-Link UART communication, timers and etc.
 *  It is called from DEV_IOL_Init().
 *
 */
extern void DPL_Init(void);

/**
 *  \fn void DPL_Wakeup_ind(void)
 *  \brief PL wake-up identification
 *
 *  This function should be called 
 *  by the device specific hardware physical layer
 *  to report data link layer of device,
 *  that a wake-up request is received from the master.
 *  See: IO-Link spec v1.1.1 Oct 2011, 5.2.2.2, p.41
 */
void DPL_Wakeup_ind (void);

/**
 *  \brief PL start the specific hardware timer
 *
 *  This function starts the specific hardware timer
 *  in milliseconds or bit times, depending on communication baudrate.
 *  It should be defined for the specific platform.
 *  If the timer has been already started, it should be stopped at first.
 *  When the timer, started by this function, is elapsed,
 *  DPL_TimerCallback() should be called.
 *
 *      WARNING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      TFBD Timer (DPL_TIME_TFBD_DELAY) should NOT DEPEND on
 *      DPL_MASTER_MESSAGE_BYTE_RECEIVE_TIMEOUT, 
 *      DPL_DEVICE_RESPONSE_DELAY,
 *      DPL_MAX_CYCLE_TIME timers !!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      It can be, for example, another physical or software timer. 
 *      
 *  \param delayType specifies delay to start corresponding timer
 *          
 */
extern void DPL_StartTimer (DPL_DelayTypeT delayType);

/**
 *  \brief PL stop the specific hardware timer
 *
 *  This function stops the specific hardware timer,
 *  started by DPL_StartTimer();
 *
 *  \param delayType specifies delay to stop corresponding timer
 *          
 */
extern void DPL_StopTimer (DPL_DelayTypeT delayType);

/**
 *  \brief PL mode switching request
 *
 *  This function switches IO-Link physical layer mode
 *  of the specific hardware of device or master.
 *  It should be called from the device or master system management.
 *  See: IO-Link spec v1.1.1 Oct 2011, 5.2.2.1, p.41
 *
 *  The Device has no inactive state. 
 *  By default at power on or cable reconnection, 
 *  the Device shall operate in the SIO mode, as a digital input. 
 *  The Device shall always be able to detect a wake-up current pulse (wake-up request). 
 *  The service DPL_WakeUp_ind reports successful detection of the wake-up request 
 *  (usually a microcontroller interrupt), 
 *  which is required for the Device to switch to the SDCI mode.
 *
 *  \param targetMode specifies the requested operation mode
 * 
 */
extern void DPL_Mode_req (DPL_TargetModeT targetMode);

/**
 *  \fn void DPL_Transfer_ind(UInteger8T data, DPL_TransferStatusT status)
 *  \brief PL byte transfer identification
 *
 *  This function should be called 
 *  by the device specific hardware physical layer
 *  to report data link layer, 
 *  that a data byte is received by io-link physical layer.
 *  See: IO-Link spec v1.1.1 Oct 2011, 5.2.2.3, p.42
 *  
 *  \param data   is a data byte to be received
 *  \param status UART error
 *
 */
void DPL_Transfer_ind (UInteger8T data, DPL_TransferStatusT status);

/**
 *  \fn void DPL_TransferBuf_ind(UInteger8T * data, UInteger8T dataLength)
 *  \brief PL byte buffer transfer identification
 *
 *  This function should be called 
 *  by the device specific hardware physical layer
 *  to report data link layer, 
 *  that a data buffer is received by io-link physical layer.
 *  This function can be used instead of DPL_Transfer_ind(),
 *  but master message length should be checked by PL in this case.
 *  See: IO-Link spec v1.1.1 Oct 2011, 5.2.2.3, p.42
 *  
 *  \param data       is a data buffer to be received
 *  \param dataLength data buffer length
 *
 */
void DPL_TransferBuf_ind(UInteger8T * data, UInteger8T dataLength);

/**
 *  \brief PL byte buffer transfer request
 *
 *  This function sends data bytes from buffer by IO-Link physical layer.
 *  It is called by data link layer.
 *  It should be defined for the specific hardware.
 *  See: IO-Link spec v1.1.1 Oct 2011, 5.2.2.3, p.42
 *
 *  \param data   is byte buffer to send
 *  \param length is a size of data buffer
 *
 */
extern void DPL_TransferBuffer_req (UInteger8T length, UInteger8T * buffer);

/** \} */ // end of Physical_Layer

#ifdef __cplusplus
}
#endif

#endif