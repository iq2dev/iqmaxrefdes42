| © Copyright 2015, IQ² Development GmbH, Neckartenzlingen
| `http://iq2-development.de <http://iq2-development.de>`_
| All rights reserved
|

.. image:: http://bytebucket.org/iq2dev/iqmaxrefdes42/raw/8b3d14bd57063fe7915c830c0fcf295db5e1404a/docs/static/image007.png

Maxim's Mojave (MAXREFDES42#) reference design features an
`IO-Link® <http://www.io-link.com>`_ based resistance to digital
temperature sensor. The IO-Link communication protocol enables
quick sensor configuration and reduces cabling while featuring
a robust, medium speed, communication protocol with power, enabling
higher powered sensors than a 4-20mA loop.

The purpose of this project is by giving the user a strong basis to start
develop production-ready IO-Link® application based on Maxim Integrated IO-Link
device transceiver (`MAX14821 <http://www.maximintegrated.com/datasheet/index.mvp/id/7416>`_),
a Renesas ultra-low-power, 16-bit microcontroller (`RL78 <http://am.renesas.com/products/mpumcu/rl78/rl78g1x/rl78g1a/>`_)
and `iqStack® IO-Link Device Stack <http://www.iq2-development.de/iqstack-device-und-master>`_.

User Manual is `available online <http://iqmaxrefdes42.readthedocs.org/en/latest/>`_.

|

.. image:: https://api.bintray.com/packages/iq2dev/iqmaxrefdes42/distribution/images/download.png
    :target: https://bintray.com/iq2dev/iqmaxrefdes42/distribution/_latestVersion
